package me.relevante.web.controller;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.ModelAndView;

@RestController
public class ViewController {

    @RequestMapping("/")
    public ModelAndView index() {
        return new ModelAndView("/index.html");
    }

    @RequestMapping({"/signin", "/signup", "/login", "/admin/**", "/twitterCallback", "/linkedinCallback", "/pocketCallback","/createList", "/myUniverse", "/recentActivity", "/myContacts", "/watchlist/**", "/account", "/filter/**", "/blacklist", "/search/**", "/list/**", "/contentHUB",
                     "/appFail", "/onboardingFilter", "/onboardingWatchlist/**"})
    public ModelAndView app() {
        return new ModelAndView("forward:/");
    }

}