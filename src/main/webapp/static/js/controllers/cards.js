angular.module('cards', [])

.directive('cardHeader', function(){ 
	return {
		restrict: 'E',
		templateUrl: '/static/templates/cards-elements/card_header.html',
	}; 
})

.directive('cardKeywords', function(){ 
	return {
		restrict: 'E',
		templateUrl: '/static/templates/cards-elements/card_keywords.html',
	}; 
})

.directive('cardTagsWatchlists', function(){ 
	return {
		restrict: 'E',
		templateUrl: '/static/templates/cards-elements/card_tags_watchlists.html',
	}; 
})

.directive('cardUserSignals', function(){ 
	return {
		restrict: 'E',
		templateUrl: '/static/templates/cards-elements/card_user_signals.html',
	}; 
})

.directive('cardPersonalInfo', function(){ 
	return {
		restrict: 'E',
		templateUrl: '/static/templates/cards-elements/card_personal_info.html',
	}; 
})

.directive('cardShares', function(){ 
	return {
		restrict: 'E',
		templateUrl: '/static/templates/cards-elements/card_shares.html',
	}; 
})

.directive('cardRemoveFromCurrentWatchlist', function(){ 
	return {
		restrict: 'E',
		templateUrl: '/static/templates/cards-elements/card_remove_from_current_watchlist.html',
	}; 
})

.directive('cardEngageBar', function(){ 
	return {
		restrict: 'E',
		templateUrl: '/static/templates/cards-elements/card_engage_bar.html',
	}; 
})

.directive('cardCornerExpand', function(){ 
	return {
		restrict: 'E',
		templateUrl: '/static/templates/cards-elements/card_corner_expand.html',
	}; 
})

.directive('cardProfilesTabs', function(){ 
	return {
		restrict: 'E',
		templateUrl: '/static/templates/cards-elements/card_profiles_tabs.html',
	}; 
})

.directive('cardProfileRelevante', function(){ 
	return {
		restrict: 'E',
		templateUrl: '/static/templates/cards-elements/card_profile_relevante.html',
	}; 
})

.directive('cardProfileLinkedin', function(){ 
	return {
		restrict: 'E',
		templateUrl: '/static/templates/cards-elements/card_profile_linkedin.html',
	}; 
})

.directive('cardProfileTwitter', function(){ 
	return {
		restrict: 'E',
		templateUrl: '/static/templates/cards-elements/card_profile_twitter.html',
	}; 
})

.directive('cardHover', function(){ 
	return {
		restrict: 'E',
		templateUrl: '/static/templates/cards-elements/card_hover.html',
	}; 
})

.directive('cardHoverSmall', function(){ 
	return {
		restrict: 'E',
		templateUrl: '/static/templates/cards-elements/card_hover_small.html',
	}; 
})

.directive('cardExpanded', function(){ 
	return {
		restrict: 'E',
		templateUrl: '/static/templates/cards-elements/card_expanded.html',
	}; 
})

.directive('cardExpandedShares', function(){ 
	return {
		restrict: 'E',
		templateUrl: '/static/templates/cards-elements/card_expanded_shares.html',
	}; 
})

// Post paginator
.directive('postPaginator', function(){ 
	return {
		restrict: 'A',
		link: function (scope, element, attrs) {
			setTimeout(function() {
				element.quickPager({pagerLocation:"before",pageSize:"1"});
			});
        }
	}; 
})

// Keywords horizontal scroll on mouse over
.directive('hScroll', function(){ 
	return {
		restrict: 'A',
		link: function (scope, element, attrs) {
			setTimeout(function() {
				$(element).scrollingCarousel(scope.$eval(attrs.hScroll));
			});
        }
	}; 
})

// Posts text vertical scroll on mouse over
.directive('vScroll', function(){ 
	return {
		restrict: 'A',
		link: function (scope, element, attrs) {
			setTimeout(function() {
				if( $(element).height() > 92){
					$(element).addClass('vscroll');
					$(element).scrollingCarousel(scope.$eval(attrs.vScroll));
				}
				
				scope.$watch(
					function () { return element.height(); },
					function (newValue, oldValue) {
						if (newValue !== oldValue) {
							if( $(element).height() > 92){
								$(element).addClass('vscroll');
								$(element).scrollingCarousel(scope.$eval(attrs.vScroll));
							}
						}
					}
				);
			});
        }
	}; 
})

//Manipulating DOM when ng-repeat has finished
.controller('cards', ['$scope', '$http', '$templateCache', '$element', '$uibModal', '$timeout', '$window', '$state', '$location',
function ($scope, $http, $templateCache, $element, $uibModal, $timeout, $window, $state, $location) {
	var parentScope = $scope.$parent.$parent;


	// Watch new selected watchlist to remove just added message
	$scope.$watch('selWatchlist',function(value) {
		$scope.removeJustAddedWatchlist = true;
	});
	//Before render
	$scope.cardFunctions = function() {
	}

	$scope.cardRender = function() {
		//After render
		var resultsArray;

		if ($scope.customSearchEnabled == true) {
			resultsArray = $scope.customSearchResults;
		} else {
			resultsArray = $scope.gridOptions.data;
		}

		// Count visible results
		$scope.resultsLength = $('ul.contacts-list').find('.item-wrap:visible').length;

		var timer=false;
		$scope.$watch(function () {
		  if(timer){
		      $timeout.cancel(timer)
		  }  
		  timer= $timeout(function(){
		      $scope.resultsLength = $('ul.contacts-list').find('.item-wrap:visible').length;
		   },300)
		});

		var i = 0;
		var aside;

		// Show tags & watchlists
        $('.arrow-down').on('click', function() {
            $(this).hide();
            $(this).next('p.arrow-up').css('display', 'inline-table');
            $(this).parent().next('.assigned-to').show().css('opacity', '1');
        });

        $('.tag-list').on('click', function() {
            $(this).parent().find('.assigned-to').show().css('opacity', '1');
            $(this).parent().find('.arrow-down').hide();
            $(this).parent().find('.arrow-up').show();
        });

        // Hide tags & watchlists
        $('p.arrow-up').on('click', function() {
            $(this).hide();
            $(this).prev('.arrow-down').show().css('display', 'inline-table');
            $(this).parent().next('.assigned-to').css('opacity', 0);
        });


	        // Switch profile
	        $scope.switchVisibleProfile = function(userNum, network) {
	        	return resultsArray[userNum].networkProfiles.twitter;

	        }

	        // Save e-mail
	        $scope.addEmailToResult = function (userId, userNum, email) {
	        	var result = resultsArray.filter(function(obj) {
				  return obj.id == userId;
				})[0];

	        	$("#"+ userId + " .email-edit").find('input').val('');

	        	result.addEmail = false;
	        	if (result.email == null) {
	        		result.email = [];
	        	}
	        	result.email.unshift(email);
	        	result.selectedEmail = email;
	        }

	        $scope.addEmailToExpandedResult = function (userId, userNum, email) {
	        	var result = $scope.expandedResult;
	        	var resultCard = resultsArray.filter(function(obj) {
				  return obj.id == userId;
				})[0];

	        	$('.card-expanded .email-edit').find('input').val('');
	        	result.addEmail = false;
	        	if (result.email == null) {
	        		result.email = [];
	        		resultCard.email = [];
	        	}
	        	result.email.unshift(email);
	        	resultCard.email.unshift(email);
	        	result.selectedEmail = email;
	        	resultCard.selectedEmail = email;
	        }

	        $scope.deleteEmailToExpandedResult = function (userId, userNum, email) {
	        	var result = $scope.expandedResult;
	        	var resultCard = resultsArray.filter(function(obj) {
				  return obj.id == userId;
				})[0];

	        	if (result.email.length > 0) {
		        	result.email.splice(email, 1);
		        	resultCard.email.splice(email, 1);
	        		result.selectedEmail = result.email[0];
	        		resultCard.selectedEmail = result.email[0];
	        	}
	        }

	        // Assign tag
	        $scope.saveTag = function (userNum, userId, tag, network) {
	        	var result = resultsArray.filter(function(obj) {
				  return obj.id == userId;
				})[0];

				if($("#"+ userId + " .addTag").val().length > 0) {
					var alreadyAssignedTag;
					angular.forEach(result.tags, function (value, i) {
						if(tag == value) {
							alreadyAssignedTag = true;
							$("#"+ userId + " .addTag").val('');
						}
					});

					if (!alreadyAssignedTag) {
						$scope.triggerLoadingFullScreen();
				        $http({
							url: $scope.apiBaseUrl + "/profiles/" + userId + "/tags",
				            method: "POST",
							contentType: "application/json; charset=utf-8",
							data: tag
						}).then(function () {
			                mixpanel.track("Saves tag", {"Tag": tag});
							$("#"+ userId + " .addTag").val('');
							if($scope.expandedResult != undefined) {
								$scope.expandedResult.tags.push(tag);
							}
							result.tags.push(tag);
							var existingTag = false;
							angular.forEach($scope.savedTags, function(value, i){

				            	if (value.note === tag){
				            		existingTag = true;
				            		value.num = value.num+1;
				            	}

				            });
			            	if (existingTag == false) {
			            		$scope.savedTags.push({"note" : tag, "num" : 1})
			            	}
			            	$scope.closeLoadingFullScreen();
			            });
					}
		    	}
	        };

	        // Delete tag
			$scope.deleteTag = function (userNum, userId, tag, tagIndex)
			{
				var result = resultsArray.filter(function(obj) {
				  return obj.id == userId;
				})[0];

				$scope.triggerLoadingFullScreen();
			    $http({
					url: $scope.apiBaseUrl + "/profiles/" + userId + "/tags/" + tag,
			        method: "DELETE",
			        data: ""
			    }).then( function () {
			        mixpanel.track("Deletes tag", {"Tag": tag});
			        if($scope.expandedResult != undefined) {
		            	$scope.expandedResult.tags.splice(tagIndex, 1);
		        	}
		            result.tags.splice(tagIndex,1);

		            angular.forEach($scope.savedTags, function(value, i){
		            	if (value.note === tag){
		            		value.num = value.num-1;

		            		if (value.num == 0) {
		            			$scope.savedTags.splice(i, 1);
		            		}
		            	}
		            });
		            $scope.closeLoadingFullScreen();
			    });
			}

			// Get current watchlist from select > option
			$scope.getCurrentWatchlist = function(watchlist) {
				$scope.currentWatchlist = watchlist;
			};

			// Assign watchlist
			$scope.addToList = function (userNum, userId, network, list, expanded, customSearch) {
				$scope.triggerLoadingFullScreen();

				angular.forEach($scope.savedLists, function (value, i) {
					var listId;

					if (value.name === list) {
						listId = value.id;

						$http({
					        url: $scope.apiBaseUrl + "/lists/" + listId + "/profiles/" + userId,
					        method: "PUT",
					        data: ""
					    })
						.then(function successCallback(response) {

				            mixpanel.track("Adds result to list", {"List": list});
				            Intercom('update', {"results added to watchlis": true});

				            // Onboarding
				      //       function startIntro(){
						    // var intro = introJs();
						    //   intro.setOptions({
						    //     'showStepNumbers': 'false',
						    //     'showBullets': false,
						    //     steps: [
						    //       {
						    //         element: document.querySelector('#toolbar'),
						    //         position: 'bottom-left-alignment',
						    //         intro: $scope.translation.OB_BUILD_GO_TO_ENGAGE_TITLE + $scope.translation.OB_BUILD_GO_TO_ENGAGE_EXPLANATION
						    //       }
						    //     ]
						    //   })

						        // .setOption('nextLabel', $scope.translation.OB_NEXT)
						        // .setOption('prevLabel', $scope.translation.OB_BACK)
						        // .setOption('skipLabel', $scope.translation.OB_SKIP)
						        // .setOption('doneLabel', $scope.translation.OB_DONE).start()

						        // .onexit(function() {
						        //     mixpanel.track("User closes go-to-engage onboarding");
						        // });
						    //}

					        // if(!localStorage.getItem("buildGoToEngageOnboardingViewed")) {
					        //     angular.forEach($scope.savedWatchlists, function(value, i){
					        //         if (value.watchlistId == $scope.watchlistId) {
					        //             var peopleInCurrentList = value.num;

					        //             if (peopleInCurrentList == 0) {
								     //        mixpanel.track("User views go-to-engage onboarding");
								     //        localStorage.setItem("buildGoToEngageOnboardingViewed", true);
								     //        introJs().exit();
								     //        startIntro();
					        //             }
					        //         }
					        //     });
					        // }

					        var result = resultsArray.filter(function(obj) {
							  return obj.id == userId;
							})[0];

				            if(expanded == true ) {
				            	$scope.expandedResult.watchlists.push(list);
				            }

				            result.watchlists.push(list);

			                // if(customSearch == true
			                // 	&& $location.path().indexOf('/filter') === -1) {
			                // 	$state.reload();
			                // }

				            // Update watchlist list value.num
		                	$scope.getLists();

		                	$scope.closeLoadingFullScreen();
		                	
				        }, function errorCallback(response) {
					    	if (response.status === 409) {
				    			$scope.$parent.$parent.feedbackPeopleListLimit = true;
					    	}
				    	});
					}
				});
			}

			// Remove from list
			$scope.removeFromList = function (userNum, userId, list, listIndex, expanded, currentList) {

				angular.forEach($scope.savedLists, function (value, i) {
					var listId;

					if (value.name === list) {
						listId = value.id;
						$scope.triggerLoadingFullScreen();
						$http({
							url: $scope.apiBaseUrl + "/lists/" + listId + "/profiles/" + userId,
					        method: "DELETE",
					        data: ""
					    })
						.then(function () {
				            mixpanel.track("Removes result from list", {"List": list});

				            var result = resultsArray.filter(function(obj) {
							  return obj.id == userId;
							})[0];

				            if(expanded == true) {
				            	$scope.expandedResult.watchlists.splice(listIndex, 1);
				            }

			                result.watchlists.splice(listIndex, 1);

				            // Update watchlist list value.num
		                	$scope.getLists();

		                	// Remove from array if currentList coincidence
		                	if(list === currentList && $location.path().indexOf('list') != -1) {
		                		if(expanded) {
		                			$scope.closeExpand();
		                		}

		                		resultsArray = resultsArray.filter(function(el) {
				    				return el.id != userId;
								});
								$scope.fadeOutResult(userId);
								$scope.gridOptions.data = resultsArray;
		                	}

		                	$scope.closeLoadingFullScreen();
				        });
				    }
				});
			}

			// Set selected list to all results
			$scope.setSelectedListToAllResults = function(list) {
				angular.forEach(resultsArray, function (value, i) {
					value.selectedList = list;
				});
			}

			$scope.removeFromCurrentWatchlist = function (userId, network, watchlist) {

				angular.forEach($scope.savedWatchlists, function (value, i) {
					var watchlistId;

					if (value.name === watchlist) {
						watchlistId = value.watchlistId;

						$http({
					        url: $scope.apiBaseUrl + "/watchlists/" + watchlistId + "/profiles/" + userId,
					        method: "DELETE",
					        data: ""
					    })
						.then(function () {
				            mixpanel.track("Removes result from watchlist", {"Watchlist": watchlist});

				            // Update watchlist list value.num
		                	angular.forEach($scope.savedWatchlists, function(value,i){
		                		if (value.name == watchlist) {
		                			value.num = value.num -1;
		                		}
		                	});
				        });
				    }
				});
			}

			// Remove from current list
			$scope.removeFromCurrentList = function (userNum, userId, list) {
				var result = resultsArray.filter(function(obj) {
				  return obj.id == userId;
				})[0];

				angular.forEach($scope.savedLists, function (value, i) {
					var listId;
					var savedList;

					if (value.name === list) {
						savedList = value;
						listId = value.id;

						$http({
					        url: $scope.apiBaseUrl + "/lists/" + listId + "/profiles/" + userId,
					        method: "DELETE",
					        data: ""
					    })
						.then(function () {
				            mixpanel.track("Removes result from list", {"List": list});

				            // Update watchlist list value.num
				            savedList.num = savedList.num -1;

				            // Remove result from results list
	                		resultsArray = resultsArray.filter(function(el) {
			    				return el.id != userId;
							});
							$scope.gridOptions.data = resultsArray;
				        });
				    }
				});
			}

			// Add to blacklist
			$scope.addToBlacklist = function (userNum, network, userId) {
				$http({
					url: $scope.apiBaseUrl + "/blacklist/" + userId,
			        method: "PUT",
			        data: ""
			    }).then( function () {

		            mixpanel.track("Adds result to blacklist");

					// Remove result from results list
            		resultsArray = resultsArray.filter(function(el) {
	    				return el.id != userId;
					});
					$scope.gridOptions.data = resultsArray;

		            // Update watchlist list value.num
		            $scope.getLists();
			    });
			}

			// Relist from blacklist
			$scope.relistFromBlacklist = function (userNum, network, userId) {
				$http({
					url: $scope.apiBaseUrl + "/blacklist/" + userId,
			        method: "DELETE",
			        data: ""
			    }).then(function () {

		            mixpanel.track("Relists result from blacklist");

		            // Remove result from results list
            		resultsArray = resultsArray.filter(function(el) {
	    				return el.id != userId;
					});
					$scope.gridOptions.data = resultsArray;

		            // Update watchlist list value.num
		            $scope.getLists();
			    });
			}

			// Fade out result
			$scope.fadeOutResult = function (id) {
				$('ul.contacts-list').find('#' + id).addClass('hiddenResult').fadeOut('fast');
			}

			// Check if already in watchlist in card's select > option
			$scope.alreadyInCardWatchlist = function (id, watchlist) {
				if($scope.savedLists != undefined) {
					if($scope.savedLists.length > 0) {

						if (!$('#' + id).hasClass('hiddenResult')) {
							var result = resultsArray.filter(function(obj) {
							  return obj.id == id;
							})[0];

							return result.watchlists.some(function(value, key) {
								return value === watchlist; // returns true
							});
						}
					}
				}
			}

			// Check if already in expanded watchlist
			$scope.alreadyInExpandedWatchlist = function (id, expandedWatchlist) {
				if($scope.savedLists.length > 0) {
					if($scope.expandedResult != undefined) {
						return $scope.expandedResult.watchlists.some(function(value, key) {
							return value === expandedWatchlist; // returns true
						});
					}
				}
			}

			// Reply layer
			$scope.showReplyLayer = function (id, postNum, expandedCard)
			{
				$scope.hideReplyLayer(id);
				$scope.focusReply = !$scope.focusReply;
				if (expandedCard == false) {
					$('#' + id).find('.comment-wrap').fadeToggle();
				} else if (expandedCard == true) {
					$('#' + id).find('.post.'+ postNum + ' .comment-wrap').fadeToggle();
				}
			}

			// Message layer
			$scope.showMessageLayer = function (id)
			{
				$scope.focusMessage = !$scope.focusMessage;
				$('#' + id).find('.contact-wrap').fadeToggle();
			}

			// Hide any textarea layer
			$scope.hideTextareaLayer = function (id)
			{
				$scope.focusReply = false;
				$scope.focusMessage = false;
				$('#' + id).find('.msg-textarea').fadeOut();
			}

			// Hide reply layer
			$scope.hideReplyLayer = function (id)
			{
				$scope.focusReply = false;
				$scope.focusMessage = false;
				$('#' + id).find('.comment-wrap').fadeOut();
			}

			// Enrich profile
			$scope.enrichProfile = function(userNum, userId) {
				var userData = [];
				userData.push(userId);

				$http({
			        url: $scope.apiBaseUrl + "/requests/enrichment",
			        method: "POST",
			        data: JSON.stringify(userData)
			    }).then(function (response) {
			    	mixpanel.track("Gets email", {"User id": userId});

			    	// Update expanded card
			    	if($scope.expandedResult != undefined) {
		            	$scope.expandedResult.enrichment = response.data[0];
		        	}
		        	
			    	// Update card
			    	var result = resultsArray.filter(function(obj) {
					  return obj.id == userId;
					})[0];
			    	result.enrichment = response.data[0];
			    });
			}

			// Send email
			$scope.sendEmail = function(email) {
				mixpanel.track("Sends email", {"email": email});
				window.location.href = "mailto:"+ email;
			}

		    if (!$templateCache.get ('subscribeToGetEmailTemplate.html')) {
		        $templateCache.put (
		          'subscribeToGetEmailTemplate.html', 
		          '<p class="subscribe-now-tooltip">This feature is not available during your trial period. It will be enabled when you get your first subscription</p><p class="subscribe-now-tooltip"><button ng-click="openSubscribeNowModal()">{{translation.SUBSCRIBE_NOW}}</button></p>'
		        );
		    }

			// LinkedIn Like
			$scope.linkedinLike = function (userNum, postNum, postId, network)
			{
				var postData = [];
				postData.push({"network": "linkedin", "postId": postId})

			    $http({
			        url: $scope.apiBaseUrl + "/requests/level1",
			        method: "POST",
			        data: JSON.stringify(postData)
			    }).then(function (response) {
			    	mixpanel.track("Clicks Like on LinkedIn post", {"Post": postId});
			    	Intercom('update', {"engage action done": true});
	        		angular.forEach(resultsArray, function(value, i){
		        		var user = resultsArray[i];
		        		var lastPost;
		        		if( user.networkProfiles.linkedin != undefined) {
		        			lastPost = user.networkProfiles.linkedin.lastPost;
		        		}
		        		if (lastPost != null && lastPost.id == postId) {
				        	lastPost.like = response.data[0];
				        	user.level1Done = true;
		        		}
		        	});
			    });
			}

			// LinkedIn Like Expanded
			$scope.linkedinLikeExpanded = function (userId, postNum, postId, network)
			{
				var postData = [];
				postData.push({"network": "linkedin", "postId": postId})

			    $http({
			        url: $scope.apiBaseUrl + "/requests/level1",
			        method: "POST",
			        data: JSON.stringify(postData)
			    }).then(function (response) {
			    	mixpanel.track("Clicks Like on LinkedIn post", {"Post": postId});
			    	Intercom('update', {"engage action done": true});
	        		var user = $scope.expandedResult;
		        	var post = user.shares.filter(function(obj) {
					  return obj.id == postId;
					})[0];
				    post.like = response.data[0];
				    user.level1Done = true;
				    
				    // updating regular card
	        		var result = resultsArray.filter(function(obj) {
					  return obj.id == userId;
					})[0];
				    result.level1Done = true;
				    if (post.id == result.networkProfiles.linkedin.lastPost.id) {
				    	result.networkProfiles.linkedin.lastPost.like = response.data[0];
				    }
			    });
			}

			// LinkedIn Comment
			$scope.submitLinkedinComment = function (userNum, postId, comment, postNum)
			{
				var postData = [];
				postData.push({"network": "linkedin", "postId": postId, "commentText": comment})

				if(comment != undefined && comment.length) {
				    $http({
				        url: $scope.apiBaseUrl + "/requests/level2",
				        method: "POST",
				        data: JSON.stringify(postData)
				    }).then(function (response) {
			        	mixpanel.track("Submits LinkedIn comment", {"Comment": comment});
			        	Intercom('update', {"engage action done": true});
			        	$('#' + userNum).find('.comment-wrap').fadeOut();
			        	$('#' + userNum).find('.comment-wrap textarea').val('');
			        	angular.forEach(resultsArray, function(value, i){
			        		var user = resultsArray[i];
			        		var lastPost;
			        		if( user.networkProfiles.linkedin != undefined) {
			        			lastPost = user.networkProfiles.linkedin.lastPost;
			        		}
			        		if (lastPost != null && lastPost.id == postId) {
					        	lastPost.comment = response.data[0];
					        	user.level2Done = true;
			        		}
			        	});
				    });
				}
			}

			// LinkedIn Comment Expanded
			$scope.submitLinkedinCommentExpanded = function (userId, postId, comment, postNum)
			{
				var postData = [];
				postData.push({"network": "linkedin", "postId": postId, "commentText": comment})

				if(comment != undefined && comment.length) {
				    $http({
				        url: $scope.apiBaseUrl + "/requests/level2",
				        method: "POST",
				        data: JSON.stringify(postData)
				    }).then(function (response) {
			        	mixpanel.track("Submits LinkedIn comment", {"Comment": comment});
			        	Intercom('update', {"engage action done": true});
			        	$('#' + userId).find('.comment-wrap').fadeOut();
			        	$('#' + userId).find('.comment-wrap textarea').val('');
			        	var user = $scope.expandedResult;
		        		var post = post = user.shares.filter(function(obj) {
						  return obj.id == postId;
						})[0];
				        post.comment = response.data[0];
				        user.level2Done = true;

				        // updating regular card
		        		var result = resultsArray.filter(function(obj) {
						  return obj.id == userId;
						})[0];
					    result.level2Done = true;
					    if (post.id == result.networkProfiles.linkedin.lastPost.id) {
					    	result.networkProfiles.linkedin.lastPost.comment = response.data[0];
					    }
				    });
				}
			}

			// LinkedIn Connect
			$scope.linkedinConnect = function (userId, linkedinId)
			{
				var userData = [];
				userData.push({"network": "linkedin", "profileId": linkedinId})
				
				$http({
			        url: $scope.apiBaseUrl + "/requests/level3",
			        method: "POST",
			        data: JSON.stringify(userData)
			    }).then(function (response) {
		        	mixpanel.track("Submits LinkedIn connect");
		        	Intercom('update', {"engage action done": true});

	        		var result = resultsArray.filter(function(obj) {
					  return obj.id == userId;
					})[0];
	        		result.networkProfiles.linkedin.connectRequest = response.data[0];
	        		result.level3Done = true;
			    });
			}

			// LinkedIn Connect Expanded
			$scope.linkedinConnectExpanded = function (userId, linkedinId)
			{
				var userData = [];
				userData.push({"network": "linkedin", "userId": linkedinId})
				
				$http({
			        url: $scope.apiBaseUrl + "/requests/level3",
			        method: "POST",
			        data: JSON.stringify(userData)
			    }).then(function (response) {
		        	mixpanel.track("Submits LinkedIn connect");
		        	Intercom('update', {"engage action done": true});
	        		var user = $scope.expandedResult;
					user.networkProfiles.linkedin.connectRequest = response.data[0];
					user.level3Done = true;

					// updating regular card
					var result = resultsArray.filter(function(obj) {
					  return obj.id == userId;
					})[0];
				    result.level3Done = true;
				    result.networkProfiles.linkedin.connectRequest = response.data[0];
			    });
			}

			// LinkedIn Message
			$scope.submitLinkedinMessage = function (userId, linkedinId, subject, message)
			{
				var userData = [];
				userData.push({"network": "linkedin", "profileId": userId, "subject": "", "message": message})

				if(message != undefined && message.length) {
				    $http({
				        url: $scope.apiBaseUrl + "/requests/level4",
				        method: "POST",
				        data: JSON.stringify(userData)
				    }).then(function (response) {
			        	mixpanel.track("Submits LinkedIn message");
			        	Intercom('update', {"engage action done": true});
			        	$('#' + userId).find('.contact-wrap').fadeOut();
						$('#' + userId).find('.contact-wrap textarea').val('');
						var result = resultsArray.filter(function(obj) {
						  return obj.id == userId;
						})[0];
						result.networkProfiles.linkedin.message = response.data[0];
						result.level4Done = true;
				    });
				}
			}

			// LinkedIn InMail Message Expanded
			$scope.submitLinkedinMessageExpanded = function (userId, linkedinId, subject, message)
			{
				var userData = [];
				userData.push({"network": "linkedin", "userId": linkedinId, "subject": "", "message": message})

				if(message != undefined && message.length) {
				    $http({
				        url: $scope.apiBaseUrl + "/requests/level4",
				        method: "POST",
				        data: JSON.stringify(userData)
				    }).then(function (response) {
			        	mixpanel.track("Submits LinkedIn message");
			        	Intercom('update', {"engage action done": true});
			        	$('#' + userId).find('.contact-wrap').fadeOut();
						$('#' + userId).find('.contact-wrap textarea').val('');
						var user = $scope.expandedResult;
						user.networkProfiles.linkedin.message = response.data[0];
						user.level4Done = true;

						// updating regular card
						var result = resultsArray.filter(function(obj) {
						  return obj.id == userId;
						})[0];
					    result.level4Done = true;
					    result.networkProfiles.linkedin.message = response.data[0];
				    });
				}
			}

			// Twitter RT
			$scope.twitterRt = function (userNum, tweetNum, tweetId)
			{
				var tweetData = {"tweetId": tweetId};

			    $http({
					url: $scope.apiBaseUrl + "/requests/twitter/retweet",
					method: "POST",
			        data: JSON.stringify(tweetData)
			    }).then(function (response) {
		            mixpanel.track("Clicks Retweet on tweet", {"Tweet": tweetId});
		            Intercom('update', {"engage action done": true});
		            angular.forEach(resultsArray, function(value, i){
		        		var user = resultsArray[i];

		        		if (value.shares.length > 0) {
		        			angular.forEach(value.shares, function(value, i){
		        				if (value.id == tweetId) {
				        			value.retweet = response.data[0];
				        			user.tapDone = true;
				        		}
				        	});
		        		}
		        	});
				});
			}

			// Twitter RT Expanded
			$scope.twitterRtExpanded = function (tweetNum, tweetId)
			{
				var tweetData = {"tweetId": tweetId};

			    $http({
					url: $scope.apiBaseUrl + "/requests/twitter/retweet",
					method: "POST",
			        data: JSON.stringify(tweetData)
			    }).then(function (response) {
		            mixpanel.track("Clicks Retweet on tweet", {"Tweet": tweetId});
		            Intercom('update', {"engage action done": true});
		        	var user = $scope.expandedResult;
		        	var post = user.shares[tweetNum];
				    post.retweet = response.data;
				    user.level1Done = true;
				});
			}

			// Twitter Fav
			$scope.twitterLike = function (userNum, tweetNum, tweetId)
			{
				var postData = [];
				postData.push({"network": "twitter", "postId": tweetId})

				$http({
					url: $scope.apiBaseUrl + "/requests/level1",
					method: "POST",
					data: JSON.stringify(postData)
				}).then(function (response) {
			        mixpanel.track("Clicks Fav on tweet", {"Tweet": tweetId});
			        Intercom('update', {"engage action done": true});
					angular.forEach(resultsArray, function(value, i){
		        		var user = resultsArray[i];
		        		var lastPost;
		        		if( user.networkProfiles.twitter != undefined) {
		        			lastPost = user.networkProfiles.twitter.lastPost;
		        		}
		        		if (lastPost != null && lastPost.id == tweetId) {
				        	lastPost.fav = response.data[0];
				        	user.level1Done = true;
		        		}
		        	});
				});
			}

			// Twitter Fav Expanded
			$scope.twitterLikeExpanded = function (userId, tweetNum, tweetId)
			{
				var postData = [];
				postData.push({"network": "twitter", "postId": tweetId})

				$http({
					url: $scope.apiBaseUrl + "/requests/level1",
					method: "POST",
					data: JSON.stringify(postData)
				}).then(function (response) {
			        mixpanel.track("Clicks Fav on tweet", {"Tweet": tweetId});
			        Intercom('update', {"engage action done": true});
					var user = $scope.expandedResult;
		        	var post = user.shares.filter(function(obj) {
					  return obj.id == tweetId;
					})[0];
				    post.fav = response.data[0];
				    user.level1Done = true;

				    // updating regular card
	        		var result = resultsArray.filter(function(obj) {
					  return obj.id == userId;
					})[0];
				    result.level1Done = true;
				    if (post.id == result.networkProfiles.twitter.lastPost.id) {
				    	result.networkProfiles.twitter.lastPost.fav = response.data[0];
				    }
				});
			}

			// Twitter Reply
			$scope.submitTwitterReply = function (userId, tweetId, reply, tweetNum)
			{
				var postData = [];
				postData.push({"network": "twitter", "postId": tweetId, "commentText": reply})

				if(reply != undefined && reply.length) {
					$http({
						url: $scope.apiBaseUrl + "/requests/level2",
						method: "POST",
						data: JSON.stringify(postData)
				    }).then(function (response) {
			        	mixpanel.track("Submits Twitter reply");
			        	Intercom('update', {"engage action done": true});
			        	$('#' + userId).find('.comment-wrap').fadeOut();
			        	$('#' + userId).find('.comment-wrap textarea').val('');
			        	angular.forEach(resultsArray, function(value, i){
			        		var user = resultsArray[i];
			        		var lastPost;
			        		if( user.networkProfiles.twitter != undefined) {
			        			lastPost = user.networkProfiles.twitter.lastPost;
			        		}
			        		if (lastPost != null && lastPost.id == tweetId) {
					        	lastPost.reply = response.data[0];
					        	user.level2Done = true;
			        		}
			        	});
				    });
				}
			}

			$scope.submitTwitterReplyExpanded = function (userId, tweetId, reply, tweetNum)
			{
				var postData = [];
				postData.push({"network": "twitter", "postId": tweetId, "commentText": reply})

				if(reply != undefined && reply.length) {
					$http({
						url: $scope.apiBaseUrl + "/requests/level2",
						method: "POST",
						data: JSON.stringify(postData)
				    }).then(function (response) {
			        	mixpanel.track("Submits Twitter reply");
			        	Intercom('update', {"engage action done": true});
			        	$('#' + userId).find('.comment-wrap').fadeOut();
			        	$('#' + userId).find('.comment-wrap textarea').val('');
		        		var user = $scope.expandedResult;
		        		var post = post = post = user.shares.filter(function(obj) {
						  return obj.id == postId;
						})[0];
				        post.reply = response.data[0];
				        user.level2Done = true;

				        // updating regular card
					    var result = resultsArray.filter(function(obj) {
						  return obj.id == userId;
						})[0];
					    result.level2Done = true;
					    if (post.id == result.networkProfiles.twitter.lastPost.id) {
					    	result.networkProfiles.twitter.lastPost.reply = response.data[0];
					    }
				    });
				}
			}

			// Twitter Follow
			$scope.twitterFollow = function (userId, twitterId)
			{
				var userData = [];
				userData.push({"network": "twitter", "profileId": twitterId})

				$http({
					url: $scope.apiBaseUrl + "/requests/level3",
					method: "POST",
					data: JSON.stringify(userData)
				}).then(function (response) {
			    	mixpanel.track("Clicks Twitter follow");
			    	Intercom('update', {"engage action done": true});
	        		
	        		// updating regular card
		    		var result = resultsArray.filter(function(obj) {
					  return obj.id == userId;
					})[0];
		        	result.networkProfiles.twitter.follow = response.data[0];
	        		result.level3Done = true;
				});
			}

			// Twitter Follow Expanded
			$scope.twitterFollowExpanded = function (userId, twitterrId)
			{
				var userData = [];
				userData.push({"network": "twitter", "userId": twitterId})

				$http({
					url: $scope.apiBaseUrl + "/requests/level3",
					method: "POST",
					data: JSON.stringify(userData)
				}).then(function (response) {
			    	mixpanel.track("Clicks Twitter follow");
			    	Intercom('update', {"engage action done": true});
		    		var user = $scope.expandedResult;
		        	user.networkProfiles.twitter.follow = response.data[0];
	        		user.level3Done = true;

	        		// updating regular card
					var result = resultsArray.filter(function(obj) {
					  return obj.id == userId;
					})[0];
				    result.level3Done = true;
				    result.networkProfiles.twitter.follow = response.data[0];
				});
			}

			// Twitter Direct Message
			$scope.submitTwitterDM = function (userId, twitterId, message)
			{
				var userData = [];
				userData.push({"network": "twitter", "profileId": twitterId, "subject": "", "message": message})

				if(message != undefined && message.length) {
					$http({
						url: $scope.apiBaseUrl + "/requests/level4",
						method: "POST",
						data: JSON.stringify(userData)
					}).then(function (response) {
			        	mixpanel.track("Submits Twitter Direct Message");
			        	Intercom('update', {"engage action done": true});
			        	$('#' + userId).find('.contact-wrap').fadeOut();
						$('#' + userId).find('.contact-wrap textarea').val('');
						var result = resultsArray.filter(function(obj) {
						  return obj.id == userId;
						})[0];
						result.networkProfiles.twitter.directMessage = response.data[0];
						result.level4Done = true;
				    });
				}
			}

			// Twitter Direct Message Expanded
			$scope.submitTwitterDMExpanded = function (userId, twitterId, message)
			{
				var userData = [];
				userData.push({"network": "twitter", "userId": twitterId, "subject": "", "message": message})

				if(message != undefined && message.length) {
					$http({
						url: $scope.apiBaseUrl + "/requests/level4",
						method: "POST",
						data: JSON.stringify(userData)
					}).then(function (response) {
			        	mixpanel.track("Submits Twitter Direct Message");
			        	Intercom('update', {"engage action done": true});
			        	$('#' + userId).find('.contact-wrap').fadeOut();
						$('#' + userId).find('.contact-wrap textarea').val('');
						var user = $scope.expandedResult;
						user.networkProfiles.twitter.directMessage = response.data[0];
						user.level4Done = true;

						// updating regular card
						var result = resultsArray.filter(function(obj) {
						  return obj.id == userId;
						})[0];
					    result.level4Done = true;
					    result.networkProfiles.twitter.directMessage = response.data[0];
				    });
				}
			}

			// Show expanded card
			$scope.expand = function (userNum, userId, size) {
				$scope.triggerLoadingFullScreen();

				$http({
					url: $scope.apiBaseUrl + "/profiles/" + userId + "?type=expanded",
					method: "GET",
				}).then(function (response) {
					var data = response.data;

		        	if(data.networkProfiles.linkedin == null){
		        		data.activeProfile = 'twitter';
		        	} else if (data.networkProfiles.twitter == null){
		        		data.activeProfile = 'linkedin';
		        	} else if (data.networkProfiles.linkedin != null
		        		&& data.networkProfiles.twitter != null) {
		        		data.activeProfile = 'relevante';
		        	}

		        	// Order keywords
		            if(data.keywords.length > 1) {
		              data.keywords = data.keywords.sort(function (a, b) {
		                  return a.toLowerCase().localeCompare(b.toLowerCase());
		                });
		            }

		            // Order lists
		            if(data.watchlists.length > 1) {
		              data.watchlists = data.watchlists.sort(function (a, b) {
		                  return a.toLowerCase().localeCompare(b.toLowerCase());
		                });
		            }

		            // Order tags
		            if(data.tags.length > 1) {
		              data.tags = data.tags.sort(function (a, b) {
		                  return a.toLowerCase().localeCompare(b.toLowerCase());
		                });
		            }

		        	$scope.expandedResult = data;
					$scope.expandedResultIndex = userNum;

					$scope.closeLoadingFullScreen();

				    var modalInstance = $uibModal.open({
				      animation: false,
				      templateUrl: '/static/templates/cards-elements/card_expanded.html',
				      size: size,
				      scope: $scope,
				      windowClass: 'expandedCardModal'
				    });

					// Close expanded card
					$scope.closeExpand = function () {
					    modalInstance.close();
					};
			    });
			};

			// Show personal metrics
			$scope.showPersonalMetrics = function() {
				$('#personal-metrics').slideToggle();
			}

			// Show metrics details
			$scope.showMetricsDetails = function(itemNum) {
				$('.metrics-details.' + itemNum).slideToggle('fast');
			}

			// Expand all metrics details
			$scope.expandAllMetricsDetails = function() {
				$('.metrics-details').slideDown('fast');
			}

			// Close all metrics details
			$scope.closeAllMetricsDetails = function() {
				$('.metrics-details').slideUp('fast');
			}

			// Show signals
			$scope.showSignals = function (userNum)
			{
				mixpanel.track("Opens signals modal");
				$('#' + userNum).find(".signals-modal").addClass('target');
			}

			// Hide signals
			$('.signals-modal').mouseup(function (e)
	        {
	            var container = $('.signals-modal ul');

	            if (!container.is(e.target) // if the target of the click isn't the container...
	                && container.has(e.target).length === 0) // ... nor a descendant of the container
	            {
	                $('.signals-modal').removeClass('target');
	            }
	        });

	        // Track LinkedIn connect
	        $scope.trackLinkedInConnect = function() {
	        	mixpanel.track("Clicks LinkedIn connect");
	        }

	        // Track LinkedIn message
	        $scope.trackLinkedInMessage = function() {
	        	mixpanel.track("Clicks LinkedIn message");
	        }

	        // Track Twitter reply
	        $scope.trackTwitterReply = function() {
	        	mixpanel.track("Clicks Twitter reply");
	        }

	        // Track Twitter reply
	        $scope.trackTwitterFollow = function() {
	        	mixpanel.track("Clicks Twitter follow");
	        }

	        // Track Twitter direct message
	        $scope.trackTwitterDM = function() {
	        	mixpanel.track("Clicks Twitter direct message");
	        }
	}
}])

.controller('filterCtrl', function($scope, $timeout){

	// Location filter
	$scope.isExcludedByFilterLocation = applySearchFilterLocation();

	$scope.$watch(
	    "filters.location",
	    function( newLocation, oldLocation ) {
	        if ( newLocation === oldLocation ) {
	            return;
	        }
	        applySearchFilterLocation();
	    }
	);

	function applySearchFilterLocation() {
	    var filter = $scope.filters.location.toLowerCase();
	    var location = $scope.result.location.toLowerCase();
	    var isSubstring = ( location.indexOf( filter ) !== -1 );

	    return $scope.isExcludedByFilterLocation = ! isSubstring;
	}

	// Position filter
	$scope.isExcludedByFilterPosition = applySearchFilterPosition();

	$scope.$watch(
	    "filters.position",
	    function( newPosition, oldPosition ) {
	        if ( newPosition === oldPosition ) {
	            return;
	        }
	        applySearchFilterPosition();
	    }
	);

	function applySearchFilterPosition() {
	    var filter = $scope.filters.position.toLowerCase();
	    var position;
	    if ($scope.result.type == 'linkedin') {
	    	position = $scope.result.headline.toLowerCase();
	    } else if ($scope.result.type == 'twitter') {
	    	position = $scope.result.userBio.toLowerCase();
	    }
	    var isSubstring = ( position.indexOf( filter ) !== -1 );

	    return $scope.isExcludedByFilterPosition = ! isSubstring;
	}

    // Company filter
	$scope.isExcludedByFilterCompany = applySearchFilterCompany();

	$scope.$watch(
	    "filters.company",
	    function( newCompany, oldCompany ) {
	        if ( newCompany === oldCompany ) {
	            return;
	        }
	        applySearchFilterCompany();
	    }
	);

	function applySearchFilterCompany() {
	    var filter = $scope.filters.company.toLowerCase();
	    var company;
	    if ($scope.result.type == 'linkedin' && $scope.result.companyName != null) {
	    	company = $scope.result.companyName.toLowerCase();
	    } else if ($scope.result.type == 'linkedin' && $scope.result.companyName == null) {
	    	company = "";
	    }else if ($scope.result.type == 'twitter') {
	    	company = $scope.result.userBio.toLowerCase();
	    }
	    var isSubstring = ( company.indexOf( filter ) !== -1 );

	    return $scope.isExcludedByFilterCompany = ! isSubstring;
	}

	var timeoutPromise;
    var delayInMs = 500;
    $scope.$watch("resultsLength", function(query) {
     $timeout.cancel(timeoutPromise);  //does nothing, if timeout already done
     timeoutPromise = $timeout(function(){   //Set timeout
         if ($scope.resultsLength < 3) {
	    	$scope.loadMore();
	    }
     },delayInMs);
    });
})

.directive(
    "bnLineItem",
    function() {
        function link( $scope, element, attributes ) {

        }
        return({
            link: link,
            restrict: "A"
        });
    }
)