angular.module('build', [])

.controller('build',['$scope', '$rootScope', '$http', '$stateParams',
function ($scope, $rootScope, $http, $stateParams) {
    $scope.loadEnabled = false;
    $scope.busy = false;
    $scope.cardsView = true;
    $scope.linkedinEnabled = true;
    $scope.twitterEnabled = true;
    $scope.nameWatchlist = '';

    var searchId = $stateParams.id;
    var searchType = $stateParams.searchType;

    $scope.searchId = searchId;


    var filterData;
    $scope.getFilterData = function() {
        $scope.$parent.editWatchlistTerms = [];
        $http.get($scope.apiBaseUrl + '/searches/' + searchId).success(function(data) {
            filterData = data;
            if(data.negativeTerms != undefined) {
                data.negativeTerms = data.negativeTerms.join(', ')
            }
            $scope.editAdvancedSearchFilter = data;
            $scope.watchlistId = data.watchlistId;
            $scope.nameSearch = data.name;
            $scope.editNameSearch = data.name;
            $scope.sourceTerms = data.searchTerms;
            angular.forEach($scope.sourceTerms, function(value, i){
                $scope.$parent.editWatchlistTerms.push(value);
            });
            $scope.currentBuildList = data;
            $scope.joinedTerms = data.searchTerms.join(', ')
            $scope.loadMore();
        });
    }

    $scope.getFilterData();

    function toObject(arr) {
        var rv = {};
        for (var i = 0; i < arr.length; ++i)
          if (arr[i] !== undefined) rv[i] = arr[i];
        return rv;
      }

    $scope.getWatchlistData = function(listId) {
      $http.get($scope.apiBaseUrl + '/lists/' + listId).success(function(string_data) {
          var rules = string_data.automationConfiguration;
          if (rules != null) {
            if (rules.stage2 != undefined) {
              rules.stage2.reply = toObject(rules.stage2.reply);
            } else {
              rules.stage2.reply = [{}];
            }

            if (rules.stage4 != undefined) {
              rules.stage4.message = toObject(rules.stage4.message);
            } else {
              rules.stage4.message = [{}];
            }
          }

          $scope.currentList = string_data;
      });
    }

    $scope.getWatchlistData($scope.searchId)


    $scope.start = 0; // 10 results per page
      var controller = this;
      controller.contactList = [];
      var termsArray = [];

      $scope.contactList = [];

    $scope.loadMore = function() {
        $scope.loadMoreTrigger = false;
        if($scope.start > 0) {
            $scope.searchingMoreResults = true;
        } else {
            $scope.busy = true;
        }

        termsArray.push($scope.joinedTerms)

      chrome.runtime.sendMessage($rootScope.chromeExtensionId, {
        "type": "search-from-web",
        "terms": termsArray,
        "start": $scope.start
      },
        function(response) {
            console.log(response)

            // Sort response by common contacts
            response.sort(function(a, b){
               var A=parseInt(a.common_contacts, 10), B=parseInt(b.common_contacts, 10);
               if (A > B) //sort string descending
                return -1;
               if (A < B)
                return 1;
               return 0; //default return value (no sorting)
            });

            console.log(response)


            angular.forEach(response, function(value, i){
                value.name = value.full_name;
                value.id = value.public_identifier;

                controller.contactList.push(value)
            });

            $scope.start = $scope.start + 10;

            $scope.busy = false;
            $scope.searchingMoreResults = false;


            if (response.length == 10) {
                $scope.loadMoreTrigger = true;
            } else {
                $scope.loadMoreTrigger = false;
                $scope.allResultsLoaded = true;
                $scope.resultsLength = $('ul.contacts-list').find('.item-wrap:visible').length;
            }

            $scope.contactList = controller.contactList;
            $scope.$apply();
        });

    }

        $scope.selectedBuildResults = []
    
    // Add result to list
    $scope.pushBuildResultToList = function(result) {
        $scope.triggerLoadingFullScreen();
        $scope.selectedBuildResults.push(result.id)

        var profile = {
            "given_name": null,
            "family_name": null,
            "locality": result.location,
            "headline": result.occupation,
            "summary": null,
            "industry": result.industry,
            "full_name": result.full_name,
            "public_identifier": result.public_identifier
        }

        $http({
            url: $scope.apiBaseUrl + "/extract/profiles/linkedin",
            method: 'POST',
            data: { profile: profile, list_id: $scope.searchId }
        }).then(function successCallback(response) {
            result.validated = true;
            $scope.closeLoadingFullScreen();
          })
    }

    // Remove result from list
    $scope.removeBuildResultTFromList = function(result) {
        angular.forEach($scope.selectedBuildResults, function (value, i) {
            if (value.id == result.id) {
                $scope.selectedBuildResults.splice(i, 1);
            }
        });
    }

    // Engage list
    $scope.addBuildResultsToList = function() {
        var results = $scope.selectedBuildResults;
        var watchlist = $scope.currentBuildList.name;
        var bulkData = [];
        var selectedItems =results;
        var successResultsNum = 0;
        var alreadyResultsNum = 0;

        if (watchlist != undefined) {
            var allResultsAssigned = true;
            angular.forEach(selectedItems, function (value, i) {
                var userId = value.id;
                var pushWatchlist = true;

                angular.forEach(value.watchlists, function(value, i) {
                    if (value == watchlist) {
                        pushWatchlist = false;
                        alreadyResultsNum = alreadyResultsNum + 1;
                        return;
                    }
                });
                if (pushWatchlist == true) {
                    bulkData.push(userId);
                    successResultsNum = successResultsNum + 1
                    allResultsAssigned = false;
                }
            });

            if (!allResultsAssigned) {
                $scope.triggerLoadingFullScreen();

                var listId = searchId;

                $http({
                    url: $scope.apiBaseUrl + "/extract/profiles/linkedin",
                    method: 'PUT',
                    data: JSON.stringify(bulkData)
                }).then(function successCallback(response) {
                    console.log(response)
                    mixpanel.track("Bulk assigned list " + watchlist + " to " + selectedItems.length + " persons.");

                    $scope.getLists();

                    // Close Modal
                    $scope.closeLoadingFullScreen();

                    $scope.goToList(listId, watchlist);
                    
                }, function errorCallback(response) {
                    if (response.status === 409) {
                        $scope.feedbackPeopleListLimit = true;
                    }
                });
            } else if (allResultsAssigned == true) {
                angular.forEach($scope.savedLists, function (value, i) {
                    if (value.name === watchlist) {
                        listId = value.id;
                    }
                });
                $scope.closeCustomSearch();
                $scope.goToList(listId, watchlist);
            }

            $scope.feedbackBulkWatchlist = true;
            $scope.alreadyResultsNum = alreadyResultsNum;
            $scope.successResultsNum = successResultsNum;
            setTimeout(function(){
                $scope.feedbackBulkWatchlist = false;
            }, 3000);
        }
    }
    
}]);