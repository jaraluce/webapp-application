angular.module('quicksearch', [])

.controller('quicksearch',['$scope', '$http', '$stateParams',
function ($scope, $http, $stateParams) {
    $scope.busy = true;
    $scope.linkedinEnabled = true;
    $scope.twitterEnabled = true;
    $scope.linkedinExtendedEnabled = false;
    $scope.twitterExtendedEnabled = false;

    function replaceAll( text, busca, reemplaza ){
      while (text.toString().indexOf(busca) != -1)
          text = text.toString().replace(busca,reemplaza);
      return text;
    }

    var searchTerms = replaceAll($stateParams.searchTerms, '%20', ' ');
    $scope.searchInputTerms = searchTerms;

    getSearch(searchTerms);    

    function getSearch(search_input) {
        mixpanel.track("Quick search", {"Search terms": search_input});

		var controller = this;

		var controller = this;
        controller.contactList = [];

        //Get recent activity       
        var contactList = [];
        $scope.contactList = [];

        var loadNum = 6;
        var start = 0;
        var end = loadNum;

        $scope.loadMore = function() {
            $scope.loadMoreTrigger = false;
            $scope.loadingResults = true;
            if ($scope.infiniteScrollDisabled) return;
            $scope.infiniteScrollDisabled = true;

            $http({
                url: $scope.apiBaseUrl + '/users',
                method: 'GET',
                params: {startingIndex: start, total: loadNum, "searchTerms" : search_input}
            }).then(function(response) {
                $scope.response = response.data;

                if ($scope.response.length > 0) {

                    angular.forEach($scope.response, function(value, i){
                        controller.contactList.push(response.data[i]);
                    })

                    $scope.busy = false;
                    $scope.loadingResults = false;

                    $scope.contactList = controller.contactList;
                    start = start + loadNum;
                    end = end + loadNum;

                } else if ($scope.response.length == 0) {
                    mixpanel.track("No results shown for this search");
                    $scope.busy = false;
                    $scope.infiniteScrollDisabled = true;
                    $scope.allResultsLoaded = true;
                }

                $scope.loadingResults = false;

            }).finally(function () {
                if ($scope.response.length == loadNum) {
                    $scope.infiniteScrollDisabled = false;
                    $scope.loadMoreTrigger = true;
                } else if ($scope.response.length < loadNum) {
                    $scope.busy = false;
                    $scope.infiniteScrollDisabled = true;
                    $scope.allResultsLoaded = true;
                    $scope.resultsLength = $('ul.contacts-list').find('.item-wrap:visible').length;
                }
            });
        }
	}
}]);