angular.module('filter', [])

.controller('filter',['$scope', '$http', '$stateParams',
function ($scope, $http, $stateParams) {
    $scope.loadEnabled = false;
    $scope.busy = false;
    wdtLoading.start({
        category: 'build_' + $scope.selectedLanguage,
        speed: 2000
    });
    $scope.cardsView = true;
    $scope.linkedinEnabled = true;
    $scope.twitterEnabled = true;
    $scope.nameWatchlist = '';
    $scope.searchTypeAlerts = false;

    var filterId = $stateParams.id;
    var searchType = $stateParams.searchType;

    $scope.filterId = filterId;


    var filterData;
    $scope.getFilterData = function() {
        $scope.$parent.editWatchlistTerms = [];
        $http.get($scope.apiBaseUrl + '/searches/' + filterId).success(function(data) {
            filterData = data;
            if(data.negativeTerms != undefined) {
                data.negativeTerms = data.negativeTerms.join(', ')
            }
            $scope.editAdvancedSearchFilter = data;
            $scope.watchlistId = data.watchlistId;
            $scope.nameFilter = data.name;
            $scope.editNameFilter = data.name;
            $scope.sourceTerms = data.searchTerms;
            angular.forEach($scope.sourceTerms, function(value, i){
                $scope.$parent.editWatchlistTerms.push(value);
            });
            $scope.currentBuildList = data;
            $scope.doFilter(filterId, $scope.nameFilter);
            if (data.location != null) {
                var metadata = {
                  location: data.location
                };
                Intercom('trackEvent', 'Location field used', metadata);
            }
        });
    }

    $scope.getFilterData();

    function toObject(arr) {
        var rv = {};
        for (var i = 0; i < arr.length; ++i)
          if (arr[i] !== undefined) rv[i] = arr[i];
        return rv;
      }

    $scope.getWatchlistData = function(listId) {
      $http.get($scope.apiBaseUrl + '/lists/' + listId).success(function(string_data) {
          var rules = string_data.automationConfiguration;
          if (rules != null) {
            if (rules.stage2 != undefined) {
              rules.stage2.reply = toObject(rules.stage2.reply);
            } else {
              rules.stage2.reply = [{}];
            }

            if (rules.stage4 != undefined) {
              rules.stage4.message = toObject(rules.stage4.message);
            } else {
              rules.stage4.message = [{}];
            }
          }

          $scope.currentList = string_data;
      });
    }

    $scope.getWatchlistData($scope.filterId)

    $scope.infiniteScrollDisabled = true;

    $scope.networksList = ['linkedin', 'twitter'];
    $scope.networkPref = ['linkedin', 'twitter'];

     // Network preferences
        $scope.setNetworkPref = function (network) {
          if($scope.networkPref.indexOf(network) == -1) {
            if (network == 'relevante') {
              $scope.networkPref = [];
              $scope.networkPref.push(network);
            } else if (network != 'relevante') {
              if ($scope.networkPref.indexOf('relevante') != -1) {
                $scope.networkPref = [];
              }
              $scope.networkPref.push(network);
            }
          } else if ($scope.networkPref.indexOf(network) != -1) {
            if (network != 'relevante'
              && $scope.networkPref.length > 1) {
              angular.forEach($scope.networkPref, function (value, i) {
                if(value == network) {
                  $scope.networkPref.splice(i, 1);
                }
              });
            }
          }

          angular.forEach($scope.gridOptions.data, function(value, i) {
            var user = value;
              if ($scope.networkPref.length == 1) {
                if($scope.networkPref.indexOf('linkedin') != -1 && user.networkProfiles.linkedin == null
                || $scope.networkPref.indexOf('twitter') != -1 && user.networkProfiles.twitter == null) {
                  user.disabledProfile = true;
                } else if ($scope.networkPref.indexOf('linkedin') != -1 && user.networkProfiles.linkedin != null
                || $scope.networkPref.indexOf('twitter') != -1 && user.networkProfiles.twitter != null) {
                  user.activeProfile = $scope.networkPref[0];
                  user.disabledProfile = false;
                }
              } else {
                angular.forEach($scope.networkPref, function(value, i) {
                  if (user.networkProfiles.twitter != undefined) {
                    user.activeProfile = 'twitter';
                  } else if(user.networkProfiles.linkedin != undefined) {
                    user.activeProfile = 'linkedin';
                  }
                  user.disabledProfile = false;
                });
              }
          });
        }
    
    $scope.doFilter = function(filterId, name) {
        $scope.infiniteScrollDisabled = false;
        var controller = this;
        controller.contactList = [];
        $scope.contactList = [];

        var loadNum = 12;
        var start = 0;
        var end = loadNum;
        $scope.startingResultIndex = start;

        // Loading list view with full response
        $scope.infiniteScrollDisabled = false;

        var searchUrl;
        var searchMethod;
        if (searchType == "universe") {
          searchUrl = $scope.apiBaseUrl + '/searches/' + filterId + "/" + searchType + "?type=signalBased";
          searchMethod = "POST"
        } else if (searchType == "discover") {
          searchUrl = $scope.apiBaseUrl + '/searches/' + filterId + "/profiles?type=signalBased";
          searchMethod = "GET"
        } else if (searchType == "alerts") {
          $scope.searchTypeAlerts = true;
          searchUrl = $scope.apiBaseUrl + '/searches/' + filterId + "/profiles/alerts";
          searchMethod = "GET"
        }
        
        $http({
            url: searchUrl,
            method: searchMethod,
            params: {total: 1000}
        }).then(function(response) {
            mixpanel.track(response.data.length + " results shown");
            if (response.data.length > 0) {
                var metadata = {
                  'results shown': response.data.length
                };
                Intercom('trackEvent', 'Search produced results', metadata);
            }
            
            var resultIndex;
            angular.forEach(response.data, function(value, i){
                resultIndex = i;

                // Order keywords
                if(value.keywords.length > 1) {
                  value.keywords = value.keywords.sort(function (a, b) {
                      return a.toLowerCase().localeCompare(b.toLowerCase());
                    });
                }

                // Order lists
                if(value.watchlists.length > 1) {
                  value.watchlists = value.watchlists.sort(function (a, b) {
                      return a.toLowerCase().localeCompare(b.toLowerCase());
                    });
                }

                // Order tags
                if(value.tags.length > 1) {
                  value.tags = value.tags.sort(function (a, b) {
                      return a.toLowerCase().localeCompare(b.toLowerCase());
                    });
                }
            
                // Setting current engage level
                if(value.level1Done) {
                  value.currentEngageLevel = '2. REPLY';
                } else if(value.level2Done) {
                  value.currentEngageLevel = '3. CONNECT';
                } else if(value.level3Done) {
                  value.currentEngageLevel = '4. MESSAGE';
                } else if(value.level4Done) {
                  value.currentEngageLevel = 'SALES READY';
                } else if (!value.level1Done
                  && !value.level2Done
                  && !value.level3Done
                  && !value.level4Done) {
                  value.currentEngageLevel = '1. LIKE';
                }

                // Setting network profile
                value.networks = [];
                if(value.networkProfiles.linkedin != undefined) {
                    value.networks.push('linkedin');
                  if (value.networkProfiles.linkedin.groups.length > 0) {
                    value.groupsName = [];
                    var groupsName = value.groupsName;
                    angular.forEach(value.networkProfiles.linkedin.groups, function(value, i){
                      groupsName.push(value);
                    });  
                  }
                }
                if(value.networkProfiles.twitter != undefined) {
                    value.networks.push('twitter');
                }

                angular.forEach(value.watchlists, function(value, i){
                    if(value == $scope.currentList.name) {
                        response.data.splice(resultIndex, 1)
                    }
                });
            });
            $scope.gridOptions.data = response.data;
            $scope.originalResponse = response.data
            $scope.loadEnabled = true;
            $scope.loadMore();

            if (response.data.length < 1000 && searchType == 'discover') {
                $scope.searchingMoreResults = true;
                $http({
                    url: $scope.apiBaseUrl + '/searches/' + filterId + "/" + searchType + "?type=raw",
                    method: 'POST',
                    params: {total: 1000}
                }).then(function(response) {
                    $scope.searchingMoreResults = false;
                    mixpanel.track(response.data.length + " results shown at raw search");
                    if (response.data.length > 0) {
                        var metadata = {
                          'results shown': response.data.length
                        };
                        Intercom('trackEvent', 'Raw search produced results', metadata);
                    }
                    
                    var resultIndex;
                    angular.forEach(response.data, function(value, i){
                        var id = value.id;
                        resultIndex = i;

                        var repeated = false;

                        angular.forEach($scope.originalResponse, function(value, i){
                            if(value.id == id) {
                                repeated = true;
                            }
                        });

                        if (!repeated) {
                            // Setting current engage level
                            if(value.level1Done) {
                              value.currentEngageLevel = '2. REPLY';
                            } else if(value.level2Done) {
                              value.currentEngageLevel = '3. CONNECT';
                            } else if(value.level3Done) {
                              value.currentEngageLevel = '4. MESSAGE';
                            } else if(value.level4Done) {
                              value.currentEngageLevel = 'SALES READY';
                            } else if (!value.level1Done
                              && !value.level2Done
                              && !value.level3Done
                              && !value.level4Done) {
                              value.currentEngageLevel = '1. LIKE';
                            }

                            // Setting network profile
                            value.networks = [];
                            if(value.networkProfiles.linkedin != undefined) {
                                value.networks.push('linkedin');
                              if (value.networkProfiles.linkedin.groups.length > 0) {
                                value.groupsName = [];
                                var groupsName = value.groupsName;
                                angular.forEach(value.networkProfiles.linkedin.groups, function(value, i){
                                  groupsName.push(value);
                                });  
                              }
                            }
                            if(value.networkProfiles.twitter != undefined) {
                                value.networks.push('twitter');
                            }

                            var alreadyInWatchlist = false;
                            angular.forEach(value.watchlists, function(value, i){
                                if(value == $scope.currentList.name) {
                                    alreadyInWatchlist = true;
                                }
                            });

                            if (!alreadyInWatchlist) {
                                $scope.gridOptions.data.push(response.data[i]);
                            }
                        }
                    });
                    $scope.loadEnabled = true;
                    $scope.loadMore();
                });
            }
        });
        

        $scope.loadMore = function() {
            if($scope.gridOptions.data != undefined && $scope.loadEnabled == true) {
                angular.forEach($scope.gridOptions.data, function(value, i){

                    if( i >= start && i < end) {
                        if (value.watchlists.length > 0) {
                            value.activeProfile = 'relevante';
                        } if (value.networkProfiles.twitter != null) {
                            value.activeProfile = 'twitter';
                        } else if (value.networkProfiles.linkedin != null) {
                            value.activeProfile = 'linkedin';
                        }
                        value.justAddedToWatchlist = false;
                        controller.contactList.push($scope.gridOptions.data[i]);
                    }
                })

                $scope.hideNoResults = false;

                if($scope.gridOptions.data.length == 0) {
                    var metadata = {
                      terms: filterData.searchTerms.join(", "),
                      target: filterData.searchGoal,
                      location: filterData.location,
                      industry: filterData.industry,
                      company: filterData.company,
                      name: filterData.name
                    };
                    Intercom('trackEvent', 'No results shown at build mode', metadata);
                  mixpanel.track("No results shown for this filter");
                  wdtLoading.done();
                  $scope.infiniteScrollDisabled = true;
                  $scope.allResultsLoaded = true;
                  $scope.resultsLength = $('ul.contacts-list').find('.item-wrap:visible').length;
                } else if ($scope.gridOptions.data.length - end > 0) {
                  wdtLoading.done();
                  $scope.loadingResults = false;
                  $scope.infiniteScrollDisabled = false;
                  $scope.loadMoreTrigger = true;
                } else if ($scope.gridOptions.data.length - end < 1) {
                  $scope.loadMoreTrigger = false;
                  wdtLoading.done();
                  $scope.infiniteScrollDisabled = true;
                  $scope.allResultsLoaded = true;
                  $scope.resultsLength = $('ul.contacts-list').find('.item-wrap:visible').length;
                }

                if($scope.gridOptions.data.length < loadNum) {
                    start = start + $scope.gridOptions.data.length;
                    end = end + $scope.gridOptions.data.length;
                } else {
                    start = start + loadNum;
                    end = end + loadNum;
                }

                $scope.contactList = controller.contactList;
            }
        }
	}

    // Checking if terms have changed
    $scope.checkFilterTermsChanges = function(terms) {

        Array.prototype.compare = function(testArr) {
            if (this.length != testArr.length) return false;
            for (var i = 0; i < testArr.length; i++) {
                if (this[i].compare) { //To test values in nested arrays
                    if (!this[i].compare(testArr[i])) return false;
                }
                else if (this[i] !== testArr[i]) return false;
            }
            return true;
        }

        var array1 = terms;
        var array2 = $scope.sourceTerms;

        if(array1.compare(array2)) {
            $scope.runFilter = false;
        } else {
            $scope.runFilter = true;
        }
    }

    // Update watchlist terms
    $scope.updateAddWatchlistTerm = function(term, watchlistName, advancedSearch) {
        $scope.checkFilterTermsChanges($scope.$parent.editWatchlistTerms)

        if($('.edit-watchlist input.terms').val().length > 0) {
            $('.edit-watchlist input.terms').val('');
            $scope.$parent.editWatchlistTerms.push(term);

        } else if ($('.edit-watchlist input').val().length == 0) {
            if($scope.runFilter == true) {
                $scope.editSearch($scope.nameFilter, watchlistName, $scope.$parent.editWatchlistTerms, advancedSearch);
            }
        }
    }

    // Delete terms from watchlist edit
    $scope.updateDeleteWatchlistTerm = function(term) {
        $scope.$parent.editWatchlistTerms.splice(term, 1);
    }

    // Edit watchlist
    $scope.editSearch = function(sourceWatchlist, watchlist, terms, advancedSearch) {
        $scope.triggerLoadingFullScreen();

        //$scope.checkFilterTermsChanges(terms);

        if(terms.length > 0) {
            angular.forEach($scope.savedWatchlists, function (value, i) {
                var watchlistId;

                if (value.name === sourceWatchlist) {
                    watchlistId = value.id;

                    $scope.runFilter = true;

                    var location;
                    var industry;
                    var company;
                    var goal = advancedSearch.searchGoal;
                    var searchCondition = advancedSearch.searchCondition;
                    var negativeTerms = []

                    if(advancedSearch.negativeTerms != undefined
                        && advancedSearch.negativeTerms != "") {
                        negativeTerms = advancedSearch.negativeTerms.split(', ');
                    }

                    if(advancedSearch != undefined) {
                        if(advancedSearch.location != '') {
                            location = advancedSearch.location;
                            localStorage.setItem("defaultLocation", location);
                        }
                        if(advancedSearch.industry != '') {
                            industry = advancedSearch.industry;
                        }
                        if(advancedSearch.company != '') {
                            company = advancedSearch.company;
                        }
                    }

                    $http({
                        url: $scope.apiBaseUrl + "/searches/" + watchlistId,
                        method: "PUT",
                        data: { name: watchlist, searchTerms: terms, searchCondition: searchCondition, negativeTerms: negativeTerms, location : location, industry : industry, company : company, searchGoal: goal }
                    })
                    .then(function () {

                        mixpanel.track("Updates search", {"Search": watchlist});

                        // Closing edit filter panel
                        $scope.editFilterEnabled = false;

                        // Refresh saved searches
                        $scope.getSavedSearches();

                        $scope.closeLoadingFullScreen();

                        if ($scope.runFilter == true) {
                            $scope.busy = false;
                            wdtLoading.start({
                                category: 'build_' + $scope.selectedLanguage,
                                speed: 2000
                            });
                            $scope.hideNoResults = true;
                            $scope.doFilter(watchlistId, watchlist);
                            $scope.loadMoreTrigger = false;
                            $scope.getFilterData();
                        } else if ($scope.runFilter == false) {
                            $scope.selWatchlist = watchlist;
                            $scope.newSelWatchlist(watchlist);
                        }
                    });
                }
            });
        }
    }

    $scope.selectedBuildResults = []
    
    // Add result to list
    $scope.pushBuildResultToList = function(result) {
        $scope.selectedBuildResults.push(result)
    }

    // Remove result from list
    $scope.removeBuildResultTFromList = function(result) {
        angular.forEach($scope.selectedBuildResults, function (value, i) {
            if (value.id == result.id) {
                $scope.selectedBuildResults.splice(i, 1);
            }
        });
    }

    // Engage list
    $scope.addBuildResultsToList = function() {
        var results = $scope.selectedBuildResults;
        var watchlist = $scope.currentBuildList.name;
        var bulkData = [];
        var selectedItems =results;
        var successResultsNum = 0;
        var alreadyResultsNum = 0;

        if (watchlist != undefined) {
            var allResultsAssigned = true;
            angular.forEach(selectedItems, function (value, i) {
                var userId = value.id;
                var pushWatchlist = true;

                angular.forEach(value.watchlists, function(value, i) {
                    if (value == watchlist) {
                        pushWatchlist = false;
                        alreadyResultsNum = alreadyResultsNum + 1;
                        return;
                    }
                });
                if (pushWatchlist == true) {
                    bulkData.push(userId);
                    successResultsNum = successResultsNum + 1
                    allResultsAssigned = false;
                }
            });

            if (!allResultsAssigned) {
                $scope.triggerLoadingFullScreen();

                var listId = filterId;

                $http({
                    url: $scope.apiBaseUrl + "/lists/" + listId + "/profiles/",
                    method: 'PUT',
                    data: JSON.stringify(bulkData)
                }).then(function successCallback(response) {
                    mixpanel.track("Bulk assigned list " + watchlist + " to " + selectedItems.length + " persons.");

                    angular.forEach(selectedItems, function (value, i) {
                        var watchlistRepeated = false;
                
                        angular.forEach(value.watchlists, function (value, i) {
                            if (value == watchlist) {
                                watchlistRepeated = true;
                            }
                        });

                        if (watchlistRepeated == false) {
                            value.watchlists.push(watchlist);
                        }
                    });

                    $scope.getLists();

                    if ($scope.searchTypeAlerts) {
                      $http.post($scope.apiBaseUrl + '/searches/' + listId + '/alerts/acknowledge').success(function(data) {
                        // Close loading screen and go to list
                        $scope.closeLoadingFullScreen();
                        $scope.goToList(listId, watchlist);
                      });
                    } else {
                      // Close loading screen and go to list
                      $scope.closeLoadingFullScreen();
                      $scope.goToList(listId, watchlist);
                    }
                    
                }, function errorCallback(response) {
                    if (response.status === 409) {
                        $scope.feedbackPeopleListLimit = true;
                    }
                });
            } else if (allResultsAssigned == true) {
                angular.forEach($scope.savedLists, function (value, i) {
                    if (value.name === watchlist) {
                        listId = value.id;
                    }
                });
                $scope.closeCustomSearch();
                $scope.goToList(listId, watchlist);
            }

            $scope.feedbackBulkWatchlist = true;
            $scope.alreadyResultsNum = alreadyResultsNum;
            $scope.successResultsNum = successResultsNum;
            setTimeout(function(){
                $scope.feedbackBulkWatchlist = false;
            }, 3000);
        }
    }

    function startIntro(){
    var intro = introJs();
      intro.setOptions({
        'showStepNumbers': 'false',
        'showBullets': false,
        steps: [
          {
            element: document.querySelector('#item0'),
            position: 'right',
            intro: $scope.translation.OB_WELCOME + $scope.userName + '!</h1>' + $scope.translation.OB_BUILD_ADD_TO_WATCHLIST
          }
        ]
      })

        .setOption('nextLabel', $scope.translation.OB_NEXT)
        .setOption('prevLabel', $scope.translation.OB_BACK)
        .setOption('skipLabel', $scope.translation.OB_SKIP)
        .setOption('doneLabel', $scope.translation.OB_DONE).start()

        .onexit(function() {
            mixpanel.track("User closes add-results-to-watchlist onboarding");
        });
    }

    // Onboarding
    $scope.startOnboardingBuild = function() {

        if(!localStorage.getItem("buildAddToListOnboardingViewed")
            && $scope.gridOptions.data != undefined 
            && $scope.gridOptions.data.length > 0
            && $scope.startingResultIndex == 0
            && $scope.savedWatchlists != undefined
            && $scope.savedWatchlists.length == 1) {
            mixpanel.track("User views add-results-to-watchlist onboarding");
            Intercom('trackEvent', 'Runs onboarding');
            localStorage.setItem("buildAddToListOnboardingViewed", true);
            startIntro();
        }
    }
}]);