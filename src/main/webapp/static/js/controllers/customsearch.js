angular.module('custom-search', [])

.controller('custom-search',['$scope', '$http', '$location',
function ($scope, $http, $location) {

  $scope.loadEnabled = false;
  $scope.linkedinEnabled = true;
  $scope.twitterEnabled = true;
  $scope.infiniteScrollDisabled = false;

  console.log($scope.gridOptions.data)
  $scope.customSearchResults = $scope.gridOptions.data;

  $scope.selResult = function($item, $model, $label) {
    $scope.customSearchList = [];
    $scope.loadMoreTrigger = false;
    $scope.allResultsLoaded = false;

    var sourceIndex;
    angular.forEach($scope.customSearchResults, function(value, i){
      if (value.id == $item.id) {
        sourceIndex = i;
      }
    });
    $item.activeProfile = 'relevante';
    $item.sourceIndex = sourceIndex;
    $scope.customSearchList.push($item);
  }

  var loadNum;
  var start;
  var end;

  var customSearchList = [];
  $scope.contactList = [];

  $scope.getCustomSearchResults = function(model, context) {
  	$scope.getMatchingResults(model);
    $scope.customSearchSelResults = [];
    customSearchList = [];
    $scope.contactList = [];
    $scope.allResultsLoaded = false;

    if (model != undefined && model.length > 2) {
      var normalizedModel = model.toLowerCase();

      loadNum = 6;
      start = 0;
      end = loadNum;

      var sourceIndex;
      angular.forEach($scope.customSearchResults, function(value, i){
        var user = $scope.customSearchResults[i];
        sourceIndex = i;

        if (context == 'any') {
        	angular.forEach($scope.coincidencesList, function (value, i) {
        		if (value.id == user.id) {
        			value.activeProfile = 'relevante';
            		value.sourceIndex = sourceIndex;
        			$scope.customSearchSelResults.push(value);
        		}
        	});
        }
        
        function checkCoincidences(user){

          var repeated = false;
          angular.forEach($scope.customSearchSelResults, function(value, i) {
            if (value.id == user.id) {
              repeated = true;
            }
          });

          if (!repeated) {
            value.activeProfile = 'relevante';
            value.sourceIndex = sourceIndex;
            $scope.customSearchSelResults.push(user);
          }
        }

        // Search matching tags
        function customSearchByTag() {
          var matchingTags = [];
          angular.forEach(value.tags, function(value, i) {
            if (value.toLowerCase() == model.toLowerCase()) {
              matchingTags.push(value);
            } 
          });
          if(matchingTags.length > 0) {
            checkCoincidences(user);
          }
        }

        // Search matching keywords
        function customSearchByKeyword() {
          var matchingKeywords = [];
          angular.forEach(value.keywords, function(value, i) {
            if (value.toLowerCase() == model.toLowerCase()) {
              matchingKeywords.push(value);
            } 
          });
          if(matchingKeywords.length > 0) {
            checkCoincidences(user);
          }
        }

        if (context == 'tag') {
          customSearchByTag();
        }

        if (context == 'keyword') {
          customSearchByKeyword();
        }
      });

      $scope.loadEnabled = true;
      $scope.loadMoreCustomSearch();
    }
  }

  $scope.loadMoreCustomSearch = function() {
      if($scope.customSearchSelResults != undefined && $scope.loadEnabled == true) {
          angular.forEach($scope.customSearchSelResults, function(value, i){
              if( i >= start && i < end) {
                  value.activeProfile = 'relevante';
                  value.justAddedToWatchlist = false;
                  customSearchList.push($scope.customSearchSelResults[i]);
              }
          })

          $scope.hideNoResults = true;

          if($scope.customSearchSelResults.length == 0) {
            $scope.busy = false;
            $scope.infiniteScrollDisabled = true;
            $scope.allResultsLoadedCustomSearch = true;
            $scope.resultsLengthCustomSearch = $scope.customSearchSelResults.length;
          } else if ($scope.customSearchSelResults.length - end > 0) {
            $scope.busy = false;
            $scope.loadingResults = false;
            $scope.infiniteScrollDisabled = false;
            $scope.loadMoreTrigger = true;
          } else if ($scope.customSearchSelResults.length - end < 1) {
            $scope.loadMoreTrigger = false;
            $scope.busy = false;
            $scope.infiniteScrollDisabled = true;
            $scope.allResultsLoadedCustomSearch = true;
            $scope.resultsLengthCustomSearch = $scope.customSearchSelResults.length;
          }

          start = start + loadNum;
          end = end + loadNum;

          $scope.customSearchList = customSearchList;
      }
  }

	$scope.getMatchingResults = function($viewValue) {
		var searchTerms = [];
		searchTerms = $viewValue.split(", ");
		$scope.matchingResults = [];
		$scope.searchByTagEnable = false;
		$scope.searchByKeywordEnable = false;

		var termIndex;
		

		// Search by tag function
		$scope.searchByTag = [];
		function addSearchByTag(tag) {
	  	var repeated = false;
	  	var tagIndex;
      if($scope.searchByTag.length > 0) {
		  	angular.forEach($scope.searchByTag, function(value, i) {
		    	if(value.tag.toLowerCase() == tag) {
		      		value.count = value.count + 1;
		      		repeated = true;
		      		if (searchTerms.length == 1) {
		      			$scope.searchByTagEnable = true;
		      		}
		    	}
        });
		  }
      if (!repeated) {
          $scope.searchByTag.push({'matchingKey': 'searchByTag', 'tag' : tag, 'count' : 1});
      }
    }

		// Search by keyword function
		$scope.searchByKeyword = [];
		function addSearchByKeyword(keyword) {
			var repeated = false;
			var keywordIndex;
      if ($scope.searchByKeyword.length > 0) {
  			angular.forEach($scope.searchByKeyword, function(value, i) {
  				if(value.keyword.toLowerCase() == keyword) {
            value.count = value.count + 1;
            repeated = true;
				  	if (searchTerms.length == 1) {
				  		$scope.searchByKeywordEnable = true;
				  	}
  				}
        });
      }
			if (!repeated) {
				$scope.searchByKeyword.push({'matchingKey': 'searchByKeyword', 'keyword' : keyword, 'count' : 1});
			}
		}

        function checkCoincidences(user, matchingKey, matchingVal){
          	var repeated = false;
          	var userIndex
          	angular.forEach($scope.matchingResults, function(value, i) {
            	if (value.id == user.id) {
            		userIndex = i;
              		repeated = true;
            	}
          	});

         	if (!repeated) {
         		if ($scope.coincidenceFound) {
    				user.match = [];
    				user.match.push({'matchingKey': matchingKey, 'matchingVal' : matchingVal});
					$scope.matchingResults.push(user);
				}
          	} else if (repeated) {
            	var matchRepeated = false;
            	angular.forEach(user.match, function(value,i){
            		if (value.matchingVal == matchingVal) {
                		matchRepeated = true;
              		}
            	});

            	if (!$scope.coincidenceFound) {
            		$scope.matchingResults.splice(userIndex, 1);
            	}

            	if (!matchRepeated) {
              		user.match.push({'matchingKey': matchingKey, 'matchingVal' : matchingVal});
            	}
          	}
        }

        $scope.coincidencesList = [];
        $scope.deleteList = [];
		angular.forEach(searchTerms, function(value, i) {
			termIndex = i;
			// Search coincidences
			if (termIndex == 0 && value.length > 2
				|| termIndex > 0) {
				$scope.resultsList;
				
		        var matchingKeywords = [];
			   	if (termIndex == 0) {
			   		$scope.resultsList = $scope.customSearchResults;
			   	} else if (termIndex > 0) {
			   		$scope.resultsList = $scope.coincidencesList;
			   		$scope.searchByTagEnable = false;
					$scope.searchByKeywordEnable = false;
			   	}
			   	var user;
			   	var userId;
			   	var userIndex;
			   	var matchFound;
	           angular.forEach($scope.resultsList, function(value,i) {
					matchFound = false;
			   		user = value;
			   		userIndex = i;
			   		userId = value.id;

					if (user.name.toLowerCase().indexOf(searchTerms[termIndex].toLowerCase()) != -1) {
						matchFound = true;
						var repeated = false;
			          	angular.forEach($scope.coincidencesList, function(value, i) {
			            	if (value.id == user.id) {
			              		repeated = true;
			            	}
			          	});
						if (termIndex == 0 && !repeated) {
							user.match = [];
    						user.match.push({'matchingKey': 'name', 'matchingVal' : user.name});
							$scope.coincidencesList.push(user)
						} else if (termIndex == 0 && repeated) {
							user.match.push({'matchingKey': 'name', 'matchingVal' : user.name});
						} else if (termIndex > 0) {
							var keyRepeated = false;
							angular.forEach(user.match, function(value, i) {
								if (value.matchingKey == 'name') {
									keyRepeated = true;
								}
							});
							if (!keyRepeated) {
								user.match.push({'matchingKey': 'name', 'matchingVal' : user.name});
							}
						}
						//$scope.coincidencesList.push({'user' : user, 'matchingKey' : 'name', 'matchingVal' : user.name});
						//checkCoincidences(user, 'name', user.name);
					}
					if (user.headline.toLowerCase().indexOf(searchTerms[termIndex].toLowerCase()) != -1) {
						matchFound = true;
						var repeated = false;
			          	angular.forEach($scope.coincidencesList, function(value, i) {
			            	if (value.id == user.id) {
			              		repeated = true;
			            	}
			          	});
						if (termIndex == 0 && !repeated) {
							user.match = [];
    						user.match.push({'matchingKey': 'headline', 'matchingVal' : user.headline});
							$scope.coincidencesList.push(user)
						} else if (termIndex == 0 && repeated) {
							user.match.push({'matchingKey': 'headline', 'matchingVal' : user.headline});
						} else if (termIndex > 0) {
							var keyRepeated = false;
							angular.forEach(user.match, function(value, i) {
								if (value.matchingKey == 'headline') {
									keyRepeated = true;
								}
							});
							if (!keyRepeated) {
								user.match.push({'matchingKey': 'headline', 'matchingVal' : user.headline});
							}
						}
						//$scope.coincidencesList.push({'user' : user, 'matchingKey' : 'headline', 'matchingVal' : user.headline});
						//checkCoincidences(user, 'headline', user.headline);
					} 
					if (user.location.toLowerCase().indexOf(searchTerms[termIndex].toLowerCase()) != -1) {
						matchFound = true;
						var repeated = false;
			          	angular.forEach($scope.coincidencesList, function(value, i) {
			            	if (value.id == user.id) {
			              		repeated = true;
			            	}
			          	});
						if (termIndex == 0 && !repeated) {
							user.match = [];
    						user.match.push({'matchingKey': 'location', 'matchingVal' : user.location});
							$scope.coincidencesList.push(user)
						} else if (termIndex == 0 && repeated) {
							user.match.push({'matchingKey': 'location', 'matchingVal' : user.location});
						} else if (termIndex > 0) {
							var keyRepeated = false;
							angular.forEach(user.match, function(value, i) {
								if (value.matchingKey == 'location') {
									keyRepeated = true;
								}
							});
							if (!keyRepeated) {
								user.match.push({'matchingKey': 'location', 'matchingVal' : user.location});
							}
						}
						//$scope.coincidencesList.push({'user' : user, 'matchingKey' : 'location', 'matchingVal' : user.location});
						//checkCoincidences(user, 'location', user.location);
						
					}

					// Search matching tags
					var matchingTags = [];
          angular.forEach(user.tags, function(value, i) {
						if (value.toLowerCase().indexOf(searchTerms[termIndex].toLowerCase()) != -1) {
							matchingTags.push(value);

					  		addSearchByTag(value.toLowerCase());
						} 
					});
					if (matchingTags.length > 0) {
						matchFound = true;
						var repeated = false;
			          	angular.forEach($scope.coincidencesList, function(value, i) {
			            	if (value.id == user.id) {
			              		repeated = true;
			            	}
			          	});
			          	if (termIndex == 0 && !repeated) {
							user.match = [];
    						user.match.push({'matchingKey': 'tag', 'matchingVal' : matchingTags.join(", ")});
							$scope.coincidencesList.push(user)
						} else if (termIndex == 0 && repeated) {
    						user.match.push({'matchingKey': 'tag', 'matchingVal' : matchingTags.join(", ")});
						} else if (termIndex > 0) {
							var alreadyHasTags = false;
							angular.forEach(user.match, function(value, i) {
								if (value.matchingKey == 'tag') {
									alreadyHasTags = true;
									var tagsList = value.matchingVal.split(', ');
									tagsList.push(matchingTags);
									value.matchingVal = tagsList.join(', ');
								}
							});
							if (!alreadyHasTags) {
								user.match.push({'matchingKey': 'tag', 'matchingVal' : matchingTags.join(", ")});
							}
						}
					}

					// Search matching keywords
					var matchingKeywords = [];
          angular.forEach(user.keywords, function(value, i) {
						if (value.toLowerCase().indexOf(searchTerms[termIndex].toLowerCase()) != -1) {
							matchingKeywords.push(value);
					  	addSearchByKeyword(value.toLowerCase());
						} 
					});
					if (matchingKeywords.length > 0) {
						matchFound = true;
						var repeated = false;
			          	angular.forEach($scope.coincidencesList, function(value, i) {
			            	if (value.id == user.id) {
			              		repeated = true;
			            	}
			          	});
			          	if (termIndex == 0 && !repeated) {
							user.match = [];
    						user.match.push({'matchingKey': 'keyword', 'matchingVal' : matchingKeywords.join(", ")});
							$scope.coincidencesList.push(user)
						} else if (termIndex == 0 && repeated) {
    						user.match.push({'matchingKey': 'keyword', 'matchingVal' : matchingKeywords.join(", ")});
						} else if (termIndex > 0) {
							var alreadyHasKeywords = false;
							angular.forEach(user.match, function(value, i) {
								if (value.matchingKey == 'keyword') {
									alreadyHasKeywords = true;
									var keywordsList = value.matchingVal.split(', ');
									keywordsList.push(matchingKeywords);
									value.matchingVal = keywordsList.join(', ');
								}
							});
							if (!alreadyHasKeywords) {
								user.match.push({'matchingKey': 'keyword', 'matchingVal' : matchingKeywords.join(", ")});
							}
						}
					}

					if (!matchFound && searchTerms.length > 1) {
						$scope.deleteList.push(userId);
					}
				});
		        if ($scope.deleteList.length > 0) {
		        	angular.forEach($scope.deleteList, function(value, i) {
		        		var deleteId = value;
		        		angular.forEach($scope.coincidencesList, function(value, i) {
		        			if(deleteId == value.id) {
								$scope.coincidencesList.splice(i, 1);
							}
						});
		        	});
		    	}

	        }
      	});

		$scope.coincidencesListLength = $scope.coincidencesList.length;
		var coincidences = $scope.coincidencesList;
		if (coincidences.length > 12) {
			coincidences = coincidences.slice(0,12);
		}
      	
      	return coincidences;
    }

    // Bulk add to list
    $scope.bulkSearchList = function(watchlist, run) {
      var results;

      if ($scope.customSearchSelResults != undefined) {
        results = $scope.customSearchSelResults;
      } else {
        results = $scope.customSearchList;
      }
    	var bulkData = [];
    	var selectedItems =results;
    	var successResultsNum = 0;
    	var alreadyResultsNum = 0;

    	if (watchlist != undefined) {
    		var allResultsAssigned = true;
			angular.forEach(selectedItems, function (value, i) {
				var userId = value.id;
				var pushWatchlist = true;

				angular.forEach(value.watchlists, function(value, i) {
					if (value == watchlist) {
						pushWatchlist = false;
						alreadyResultsNum = alreadyResultsNum + 1;
						return;
					}
				});
				if (pushWatchlist == true) {
					bulkData.push(userId);
					successResultsNum = successResultsNum + 1
					allResultsAssigned = false;
				}
			});

			if (!allResultsAssigned) {
				$scope.triggerLoadingFullScreen();

				angular.forEach($scope.savedLists, function (value, i) {
					var listId;

					if (value.name === watchlist) {
						watchlistIndex = i;
						listId = value.id;

						$http({
					        url: $scope.apiBaseUrl + "/lists/" + listId + "/profiles/",
					        method: 'PUT',
					        data: JSON.stringify(bulkData)
					    }).then(function successCallback(response) {
					    	mixpanel.track("Bulk assigned list " + watchlist + " to " + selectedItems.length + " persons.");

							angular.forEach(selectedItems, function (value, i) {
								var watchlistRepeated = false;
						
								angular.forEach(value.watchlists, function (value, i) {
									if (value == watchlist) {
										watchlistRepeated = true;
									}
					    		});

					    		if (watchlistRepeated == false) {
					    			value.watchlists.push(watchlist);
					    		}
					    	});

					    	$scope.savedLists[watchlistIndex].num = $scope.savedLists[watchlistIndex].num + successResultsNum;

					    	// Close Modal
							$scope.closeLoadingFullScreen();

					    	if(run == true) {
								$scope.closeCustomSearch();
					    		$scope.goToList(listId, watchlist);
					    	}
					    	
					    }, function errorCallback(response) {
					    	if (response.status === 409) {
				    			$scope.feedbackPeopleListLimit = true;
					    	}
				    	});
					}
				});
			} else if (allResultsAssigned == true && run == true) {
				angular.forEach($scope.savedLists, function (value, i) {
					if (value.name === watchlist) {
						listId = value.id;
					}
				});
				$scope.closeCustomSearch();
				$scope.goToList(listId, watchlist);
			}

			$scope.feedbackBulkWatchlist = true;
			$scope.alreadyResultsNum = alreadyResultsNum;
			$scope.successResultsNum = successResultsNum;
			setTimeout(function(){
				$scope.feedbackBulkWatchlist = false;
			}, 3000);
		}
    }

    $scope.visitList = function(list) {
    	angular.forEach($scope.savedLists, function (value, i) {
			if (value.name === list) {
				listId = value.id;
			}
		});
		$scope.closeCustomSearch();
		$scope.goToList(listId, list);
    }
}]);