angular.module('onboardingFilter', ['cards'])

.controller('onboardingFilter',['$scope', '$http', '$location',
function ($scope, $http, $location) {
    $scope.busy = true;
    $scope.linkedinEnabled = true;
    $scope.twitterEnabled = true;
    $scope.linkedinExtendedEnabled = true;
    $scope.twitterExtendedEnabled = true;

    $scope.nameFilter = 'Social selling';

    var controller = this;
    controller.contactList = [];

    $http({
        url: $scope.apiBaseUrl + '/filters/demoUsers',
        method: 'GET',
        params: {startingIndex: 0, total: 12}
    }).then(function(response) {
        $scope.response = response.data;

        if ($scope.response.length > 0) {

            angular.forEach($scope.response, function(value, i){
                controller.contactList.push(response.data.contactList[i]);
                if (value.type == 'linkedin' && value.isContact == true) {
                    value.networkFilter = 'linkedin';
                }
                if (value.type == 'twitter' && value.isContact == true) {
                    value.networkFilter = 'twitter';
                }
                if (value.type == 'linkedin' && value.isContact == false) {
                    value.networkFilter = 'linkedinExtended';
                }
                if (value.type == 'twitter' && value.isContact == false) {
                    value.networkFilter = 'twitterExtended';
                }

                if (value.type == 'linkedin') {
                    var signals
                    angular.forEach(value.shares, function(value, i){
                        if (value.prospects.length > 0 || value.usersLike.length > 0) {
                            signals = true
                        }
                    });
                }

                if (signals == true) {
                    value.signals = true;
                }
            })

            $scope.busy = false;
            $scope.loadingResults = false;

            $scope.contactList = controller.contactList;
        } else if ($scope.response.length == 0) {
            $scope.busy = false;
            $scope.infiniteScrollDisabled = true;
            $scope.allResultsLoaded = true;
        }

        $scope.loadingResults = false;

    }).finally(function () {
        if ($scope.response.length == loadNum) {
            $scope.infiniteScrollDisabled = false;
            $scope.loadMoreTrigger = true;
        } else if ($scope.response.length < loadNum) {
            $scope.busy = false;
            $scope.infiniteScrollDisabled = true;
            $scope.allResultsLoaded = true;
        }
    });

    // Onboarding
    $scope.cardFilterRender = function() {
        startIntro();
    }

    function startIntro(){
    var intro = introJs();
      intro.setOptions({
        'tooltipPosition': 'auto',
        'showStepNumbers': 'false',
        'showBullets': false,
        steps: [
          {
            element: '#item0',
            intro: $scope.translation.OB_ITEM0,
            position: 'right'
          },
          {
            element: '#signal0',
            intro: $scope.translation.OB_SIGNAL0,
            position: 'top'
          },
          {
            element: '#step-engage',
            intro: $scope.translation.OB_STEP_ENGAGE,
            position: 'left'
          }
        ]
      });

        intro.onchange(function(targetElement) {
            switch (targetElement.id) 
                {
                case "item0":
                    $('.discover').removeClass('hover');
                break;
                case "step-engage":
                    $('.signals-modal').hide();
                    $('.engage').addClass('hover');
                break;
                }
        }).onbeforechange(function(targetElement) {
            switch (targetElement.id) 
                {
                case "signal0":
                    $('#item0').find(".signals-modal").addClass('target');
                    setTimeout(function(){
                        $('#item0 #signal0 li.prospectItem:first-child').find('.actionModal.watchlist').trigger('click');
                    },1000);
                    setTimeout(function(){
                        $('#item0 #signal0 li.prospectItem:first-child').find('.actionModal.watchlist').trigger('click');
                    },2000);
                    setTimeout(function(){
                        $('#item0 #signal0 li.prospectItem:first-child').find('.actionModal.watchlist').trigger('click');
                    },3000);
                break;
                }
        });

        intro.setOption('nextLabel', $scope.translation.OB_NEXT)
        .setOption('prevLabel', $scope.translation.OB_BACK)
        .setOption('skipLabel', $scope.translation.OB_SKIP)
        .setOption('doneLabel', $scope.translation.OB_GO_TO_WATCHLIST).start().oncomplete(function() {
            $location.path("/onboardingWatchlist/level1");
        });

        intro.onexit(function() {
            mixpanel.track("User closes onboarding");
            $scope.selWatchlist = $scope.initialWatchlist;
            $scope.newSelWatchlist($scope.initialWatchlist);
            $scope.deleteWatchlist('Onboarding watchlist');
            window.location.href = "/";
        });
    }
}]);