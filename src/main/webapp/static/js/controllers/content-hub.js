angular.module('content-hub', [])

.controller('content-hub',['$scope', '$http', '$location',
function ($scope, $http, $location) {

	// Get Pocket tags
	$scope.$watch('pocketAccount',function(value) {
		if ($scope.pocketAccount) {
			$http({
		        url: $scope.apiBaseUrl + '/pocket/tags',
		        method: 'GET'
		    }).then(function(data) {

		      	if (data.data.length > 0) {
		        	$scope.pocketTags = data.data;
		    	} else if (data.data.length == 0) {
		    		$scope.pocketTags = [];
		    		$scope.pocketTags.push('*')
		    	}
		    });
		}
	});

   	$scope.$watch('wordpressData',function(value) {
		if ($scope.wordpressHubId != undefined) {

			// Get WordPress categories
			$http({
				url: $scope.apiBaseUrl + '/wordpress/hubs/' + $scope.wordpressHubId + '/categories',
				method: 'GET'
			}).then(function(response) {
				var data = response.data
				angular.forEach(data, function (value, i) {
		    		if (value.language == 'en') {
		    			value.language = 'gb';
		    		}
		    	});
				$scope.hubCategories = data;
			});

			// Get saved workflows
			$http({
				url: $scope.apiBaseUrl + '/wordpress/hubs/' + $scope.wordpressHubId + '/mappings',
				method: 'GET'
			}).then(function(data) {
				$scope.savedHubWorkflows = data.data;
			});

		}
	});


	$scope.setPocketActive= function(item, num) {
		if (item === $scope.selected) {
	        $scope.pocketSelected = null;
	    } else {
	        $scope.pocketSelected = item;
	        $scope.pocketTagIndex = num;
	    }
	};

	$scope.isPocketActive = function(item) {
		return $scope.pocketSelected === item;
	}

	$scope.setHubActive= function(item) {
		if (item === $scope.selected) {
	        $scope.hubSelected = null;
	    } else {
	        $scope.hubSelected = item.name;
	        $scope.hubLanguage = item.language;
	    }
	};

	$scope.isHubActive = function(item) {
		return $scope.hubSelected === item;
	}

	$scope.pocketTagUsed = function(item) {
		var used = false
		angular.forEach($scope.savedHubWorkflows, function (value, i) {
			if (value.pocket == item) {
				used = true;
			}
		});
		return used;
	}

	$scope.allPocketTagsUsed = function() {
		if ($scope.pocketTags != undefined && $scope.savedHubWorkflows != undefined) {

			var allUsed = false;

			if ($scope.pocketTags.length == $scope.savedHubWorkflows.length) {
				allUsed = true;
			}

			return allUsed;

		}
	}

	$scope.hoverWorkflowIn = function(){
        this.hoverWorkflow = true;
    };

    $scope.hoverWorkflowOut = function(){
        this.hoverWorkflow = false;
    };

	$scope.saveHubWorkflow = function(pocketTag, pocketTagIndex, hubCategory, language) {
		if (pocketTag != undefined && hubCategory != undefined) {
			$http({
		        url: $scope.apiBaseUrl + '/wordpress/hubs/' + $scope.wordpressHubId + '/mappings',
		        method: 'POST',
				data: { "pocket" : pocketTag, "match" : hubCategory, "language" : language }
		    }).then(function(data) {
                $scope.savedHubWorkflows.push(data.data);
				$scope.pocketSelected = null;
				$scope.hubSelected = null;
		    });
		}
	}

	$scope.deleteHubWorkflow = function(workflowIndex, workflowId) {
		$http({
	        url: $scope.apiBaseUrl + '/wordpress/hubs/' + $scope.wordpressHubId + '/mappings/' + workflowId,
	        method: 'DELETE'
	    }).then(function() {
	        $scope.savedHubWorkflows.splice(workflowIndex, 1);
	    });
	}
}]);