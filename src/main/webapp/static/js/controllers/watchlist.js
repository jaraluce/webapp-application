angular.module('watchlist', [])

.controller('watchlist',['$scope', '$http', '$location', '$stateParams', '$window', '$uibModal',
function ($scope, $http, $location, $stateParams, $window, $uibModal) {
    $scope.hideContextList();
    $scope.loadEnabled = false;
    $scope.busy = true;
    $scope.cardsView = true;
    $scope.linkedinEnabled = true;
    $scope.twitterEnabled = true;
    $scope.emptyWatchlist = false;
    $scope.engageMode = true;
    $scope.engageActionsEnabled = true;

    $scope.contentLibraryEnabled = false;

    var watchlistId = $stateParams.id;

    function toObject(arr) {
    var rv = {};
    for (var i = 0; i < arr.length; ++i)
      if (arr[i] !== undefined) rv[i] = arr[i];
    return rv;
  }

    $scope.watchlistId = watchlistId;

    $scope.getWatchlistData = function() {
      $http.get($scope.apiBaseUrl + '/lists/' + watchlistId).success(function(string_data) {
          var rules = string_data.automationConfiguration;
          if (rules != null) {
            if (rules.stage2 != undefined) {
              rules.stage2.reply = toObject(rules.stage2.reply);
            } else {
              rules.stage2.reply = [{}];
            }

            if (rules.stage4 != undefined) {
              rules.stage4.message = toObject(rules.stage4.message);
            } else {
              rules.stage4.message = [{}];
            }
          }

          $scope.listId = string_data.id;
          $scope.currentList = string_data;
          $scope.listName = string_data.name;
          $scope.nameWatchlist = string_data.name;
          $scope.selWatchlist = string_data.name;
          $scope.newSelWatchlist(string_data.name);
          $scope.contact_watchlist(watchlistId, $scope.nameWatchlist);
          localStorage.setItem("buildList", string_data.name);

          // if (!string_data.automation) {
          //   $scope.showAutomationModal();
          // }
      });
    }

    var filterData;
    $scope.getFilterData = function() {
        $scope.$parent.editWatchlistTerms = [];
        $http.get($scope.apiBaseUrl + '/searches/' + watchlistId).success(function(data) {
            filterData = data;
            if(data.negativeTerms != undefined) {
                data.negativeTerms = data.negativeTerms.join(', ')
            }
            $scope.editAdvancedSearchFilter = data;
            $scope.watchlistId = data.watchlistId;
            $scope.nameFilter = data.name;
            $scope.editNameFilter = data.name;
            $scope.sourceTerms = data.searchTerms;
            angular.forEach($scope.sourceTerms, function(value, i){
                $scope.$parent.editWatchlistTerms.push(value);
            });
        });
    }

    $scope.getFilterData();


    $scope.setActiveTab = function(path) {
      if (path == 'engage' || path == undefined) {
        $location.path("/list/" +  watchlistId + "/engage");
        $scope.activeTab = 'engage';
      } else if (path == 'build') {
        $location.path("/list/" +  watchlistId + "/build");
        $scope.activeTab = 'build';
      }
    }

    $scope.getActiveTab = function() {
      if ($location.path().indexOf('/engage') != -1) {
        $scope.activeTab = 'engage';
      } else if ($location.path().indexOf('/build') != -1) {
        $scope.activeTab = 'build';
      }
    }

    $scope.getWatchlistData();

    $scope.infiniteScrollDisabled = true;

    $scope.contact_watchlist = function(watchlist) {
        $scope.infiniteScrollDisabled = false;
        var controller = this;
        controller.contactList = [];
        $scope.contactList = [];

        var loadNum = 12;
        var start = 0;
        var end = loadNum;
        $scope.startingResultIndex = start;

         // Loading list view with full response
        $scope.infiniteScrollDisabled = false;
        
        $http({
            url: $scope.apiBaseUrl + '/lists/' + watchlistId + "/profiles",
            method: 'GET',
            params: {total: 1000}
        }).then(function(response) {
            mixpanel.track(response.data.length + " results shown");

            // Show automation fake
            // if(response.data.length == 0) {
            //   $scope.showAutomationModal();
            // }

            data = response.data.reverse();
            var orderedResponse = [];
            var network;
            // if (localStorage.getItem("networkPref") != undefined) {
            //   var networkPrefList = JSON.parse(localStorage.getItem("networkPref"));
            //   $scope.networksList = networkPrefList;
            //   $scope.networkPref = networkPrefList[0];
            //   network = $scope.networkPref;
            // } else {
            //     $scope.networksList = ['linkedin', 'twitter', 'relevante'];
            //     $scope.networkPref = 'relevante';
            //     network = 'relevante';
            // }
            $scope.networksList = ['linkedin', 'twitter', 'relevante'];
            $scope.networkPref = ['linkedin', 'twitter'];
            angular.forEach(response.data, function(value, i){

                // Order keywords
                if(value.keywords.length > 1) {
                  value.keywords = value.keywords.sort(function (a, b) {
                      return a.toLowerCase().localeCompare(b.toLowerCase());
                    });
                }

                // Order lists
                if(value.watchlists.length > 1) {
                  value.watchlists = value.watchlists.sort(function (a, b) {
                      return a.toLowerCase().localeCompare(b.toLowerCase());
                    });
                }

                // Order tags
                if(value.tags.length > 1) {
                  value.tags = value.tags.sort(function (a, b) {
                      return a.toLowerCase().localeCompare(b.toLowerCase());
                    });
                }

                // Setting current engage level
                if(value.level1Done) {
                  value.currentEngageLevel = '2. REPLY';
                } else if(value.level2Done) {
                  value.currentEngageLevel = '3. CONNECT';
                } else if(value.level3Done) {
                  value.currentEngageLevel = '4. MESSAGE';
                } else if(value.level4Done) {
                  value.currentEngageLevel = 'SALES READY';
                } else if (!value.level1Done
                  && !value.level2Done
                  && !value.level3Done
                  && !value.level4Done) {
                  value.currentEngageLevel = '1. LIKE';
                }

                // Setting network profile and last share
                value.networks = [];
                value.shares = [];
                if (value.networkProfiles.linkedin != undefined) {
                  value.networks.push('linkedin');
                  value.activeProfile = 'linkedin';
                  if (value.networkProfiles.linkedin.groups.length > 0) {
                    value.groupsName = [];
                    var groupsName = value.groupsName;
                    angular.forEach(value.networkProfiles.linkedin.groups, function(value, i){
                      groupsName.push(value);
                    });  
                  } if (value.networkProfiles.linkedin.lastPost != null) {
                    value.networkProfiles.linkedin.profileTab = 'activity';
                    value.networkProfiles.linkedin.lastPost.network = 'linkedin';
                    value.shares.push(value.networkProfiles.linkedin.lastPost);
                    orderedResponse.unshift(value);
                  } else {
                    value.networkProfiles.linkedin.profileTab = 'info';
                    if(value.networkProfiles.twitter != undefined) {
                      if (value.networkProfiles.twitter.lastPost != null) {
                        value.activeProfile = 'twitter';
                        orderedResponse.unshift(value);
                      }
                    } else {
                      orderedResponse.push(value);
                    }
                  }
                } else if (value.networkProfiles.twitter != undefined) {
                  value.networks.push('twitter');
                  value.activeProfile = 'twitter';
                  if (value.networkProfiles.twitter.lastPost != null) {
                    value.networkProfiles.twitter.lastPost.network = 'twitter';
                    value.shares.push(value.networkProfiles.twitter.lastPost);
                    orderedResponse.unshift(value);
                  } else {
                    orderedResponse.push(value);
                  }
                }

                // Setting Twitter profile tab
                if(value.networkProfiles.twitter != undefined) {
                  if (value.networkProfiles.twitter.lastPost != null) {
                    value.networkProfiles.twitter.profileTab = 'activity';
                  } else {
                    value.networkProfiles.twitter.profileTab = 'info';
                  }
                }
            });
            $scope.gridOptions.data = orderedResponse;
            //$scope.gridOptions.data = fakeUser;
            $scope.loadEnabled = true;
            $scope.loadMore();
        });

        $scope.loadMore = function() {
            if($scope.gridOptions.data != undefined && $scope.loadEnabled == true) {
                angular.forEach($scope.gridOptions.data, function(value, i){
                    if( i >= start && i < end) {
                        value.justAddedToWatchlist = false;
                        controller.contactList.push($scope.gridOptions.data[i]);
                    }
                })

                if($scope.gridOptions.data.length == 0) {
                  mixpanel.track("Watchlist is empty");
                  $scope.busy = false;
                  $scope.infiniteScrollDisabled = true;
                  $scope.allResultsLoaded = true;
                  $scope.emptyWatchlist = true;
                } else if ($scope.gridOptions.data.length - end > 0) {
                  $scope.busy = false;
                  $scope.loadingResults = false;
                  $scope.infiniteScrollDisabled = false;
                  $scope.loadMoreTrigger = true;
                } else if ($scope.gridOptions.data.length - end < 1) {
                  $scope.loadMoreTrigger = false;
                  $scope.busy = false;
                  $scope.infiniteScrollDisabled = true;
                  $scope.allResultsLoaded = true;
                  $scope.resultsLength = $('ul.contacts-list').find('.item-wrap:visible').length;
                }

                start = start + loadNum;
                end = end + loadNum;

                $scope.contactList = controller.contactList;
                $scope.contactListRetrieved = true;
            }
        }

        // Fake user
        // $scope.gridOptions.data = fakeUser;
        // $scope.loadEnabled = true;
        // $scope.loadMore();
        // angular.forEach($scope.gridOptions.data, function(value, i){
        //     if( i >= start && i < end) {
        //         if (localStorage.getItem("networkPref") != undefined) {
        //             var networkPrefList = JSON.parse(localStorage.getItem("networkPref"));
        //             $scope.networksList = networkPrefList;
        //             $scope.networkPref = networkPrefList[0];
        //             var network = $scope.networkPref;
        //             angular.forEach($scope.gridOptions.data, function(value, i) {
        //               if(network == 'linkedin' && value.networkProfiles.linkedin == null
        //                 || network == 'twitter' && value.networkProfiles.twitter == null) {
        //                 value.activeProfile = 'relevante';
        //               } else {
        //                 value.activeProfile = network;
        //               }
        //             });
        //           } else {
        //             $scope.networksList = ['relevante', 'linkedin', 'twitter'];
        //             $scope.networkPref = 'relevante';
        //             value.activeProfile = 'relevante';
        //         }
        //         value.justAddedToWatchlist = false;
        //         controller.contactList.push($scope.gridOptions.data[i]);
        //     }
        // })

        // Automation modal
  $scope.showAutomationModal = function(listId) {
    $scope.automationListId = listId;
      var modalInstance = $uibModal.open({
        animation: false,
        templateUrl: '/static/templates/layout-elements/automation_modal.html',
        scope: $scope,
        windowClass: 'automationModal'
      });

      // Hide policy modal
      $scope.hideAutomationModal = function() {
        modalInstance.close();
      }
    }

    // Add automation fields
    $scope.addAutomationFields = function(stage) {
      stage.push({});
    }

    // Automation submit
    $scope.submitAutomation = function(rules, custom, list) {
      $scope.triggerLoadingFullScreen();

      var listId;

      function returnValues(obj) {
          var arr = [];
          for (var key in obj)
             arr.push(obj[key]);
          return arr;
      }

      rules.custom = custom;

      if (custom) {
        if (rules.stage1.force == undefined) {
          rules.stage1.force = false;
        }

        if (rules.stage2 != undefined) {
          rules.stage2.reply = returnValues(rules.stage2.reply);

          if (rules.stage2.force == undefined) {
            rules.stage2.force = false;
          }
        } else {
          rules.stage2 = {};
          rules.stage2.force = false;
        }

        if (rules.stage4 != undefined) {
          rules.stage4.message = returnValues(rules.stage4.message);
        } else {
          rules.stage4 = {};
        }

        if (rules.duration == undefined) {
          rules.stage1.force = true;
          rules.stage2.force = true;
        }
        listId = $scope.automationListId;
      }

      // Default settings
      if (!custom) {
        rules = {};
        rules.custom = false;
        rules.duration = 1;
        rules.stage1 = {};
        rules.stage2 = {};
        rules.stage3 = {};
        rules.stage4 = {};
        rules.stage1.likesNumber = 2;
        rules.stage1.force = false;
        rules.stage2.force = false;
        rules.stage2.reply = undefined;
        rules.stage3.preferredNetwork = 'all';
        rules.stage4.message = undefined;
        listId = list;
      }


    $http({
          url: $scope.apiBaseUrl + '/lists/' + listId + '/automation',
          method: 'PUT',
          data: {'duration' : rules.duration,
              'custom' : custom,
              'stage1' : rules.stage1,
              'stage2' : rules.stage2,
              'stage3' : rules.stage3,
              'stage4' : rules.stage4,
        }
      }).then(function successCallback(response) {
        $scope.closeLoadingFullScreen();
        $scope.currentList.automationConfiguration = rules;
      });
    }

    // Delete automation
    $scope.deleteAutomation = function() {
      var listId = $scope.currentList.id;

      $http({
            url: $scope.apiBaseUrl + '/lists/' + listId + '/automation',
            method: 'delete',
        }).then(function successCallback(response) {
          $scope.currentList.automationConfiguration = null;
        });
    }

    // $scope.showAutomationModal();

        // Content library
        $scope.contentLibraryModal = function(size) {
            var modalInstance = $uibModal.open({
              animation: false,
              templateUrl: '/static/templates/layout-elements/content_library_modal.html',
              size: size,
              scope: $scope,
              windowClass: 'content-library-modal'
            });

            // Close bulk action touch
            $scope.cancelContentLibrary = function() {
              modalInstance.close();
            }
        }

        // Network preferences
        $scope.setNetworkPref = function (network) {
          if($scope.networkPref.indexOf(network) == -1) {
            if (network == 'relevante') {
              $scope.networkPref = [];
              $scope.networkPref.push(network);
            } else if (network != 'relevante') {
              if ($scope.networkPref.indexOf('relevante') != -1) {
                $scope.networkPref = [];
              }
              $scope.networkPref.push(network);
            }
          } else if ($scope.networkPref.indexOf(network) != -1) {
            if (network != 'relevante'
              && $scope.networkPref.length > 1) {
              angular.forEach($scope.networkPref, function (value, i) {
                if(value == network) {
                  $scope.networkPref.splice(i, 1);
                }
              });
            }
          }

          angular.forEach($scope.gridOptions.data, function(value, i) {
            var user = value;
            if ($scope.networkPref.indexOf('relevante') != -1) {
              user.activeProfile = 'relevante';
              user.disabledProfile = false;
            } else {
              if ($scope.networkPref.length == 1) {
                if($scope.networkPref.indexOf('linkedin') != -1 && user.networkProfiles.linkedin == null
                || $scope.networkPref.indexOf('twitter') != -1 && user.networkProfiles.twitter == null) {
                  user.disabledProfile = true;
                } else if ($scope.networkPref.indexOf('linkedin') != -1 && user.networkProfiles.linkedin != null
                || $scope.networkPref.indexOf('twitter') != -1 && user.networkProfiles.twitter != null) {
                  user.activeProfile = $scope.networkPref[0];
                  user.disabledProfile = false;
                }
              } else {
                angular.forEach($scope.networkPref, function(value, i) {
                  if (user.networkProfiles.twitter != undefined) {
                    user.activeProfile = 'twitter';
                  } else if(user.networkProfiles.linkedin != undefined) {
                    user.activeProfile = 'linkedin';
                  }
                  user.disabledProfile = false;
                });
              }
            }
          });
        }

        $scope.onDropCompleteNetworks = function (index, obj, evt) {
            var otherObj = $scope.networksList[index];
            var otherIndex = $scope.networksList.indexOf(obj);
            $scope.networksList[index] = obj;
            $scope.networksList[otherIndex] = otherObj;
            $scope.networkPref = $scope.networksList[0];
            $scope.setNetworkPref(obj);
            localStorage.setItem("networkPref", JSON.stringify($scope.networksList));
        }

        function startIntro(){
      var intro = introJs();
        intro.setOptions({
          'showStepNumbers': 'false',
          'showBullets': false,
          steps: [
            {
              element: document.querySelector('#item0'),
              position: 'right',
              intro: $scope.translation.OB_START_ENGAGING_TITLE + $scope.translation.OB_START_ENGAGING_EXPLANATION
            },
            {
              element: document.querySelector('#item0'),
              position: 'right',
              intro: $scope.translation.OB_DOWNLOAD_GCE_TITLE + $scope.translation.OB_DOWNLOAD_GCE_EXPLANATION
            }
          ]
        })

          .setOption('nextLabel', $scope.translation.OB_NEXT)
          .setOption('prevLabel', $scope.translation.OB_BACK)
          .setOption('skipLabel', $scope.translation.OB_SKIP)
          .setOption('doneLabel', $scope.translation.OB_DOWNLOAD_EXTENSION)

          .oncomplete(function() {
            $scope.goToChromeExtensionUrl();
            mixpanel.track("User goes to Chrome Extension download from onboarding");
            Intercom('trackEvent', 'Completes onboarding');
          }).start()

          .onexit(function() {
            mixpanel.track("Exits onboarding");
          });
      }

    // Onboarding
    $scope.startOnboardingEngage = function() {
        if(!localStorage.getItem("engageOnboardingViewed")
            && $scope.gridOptions.data != undefined 
            && $scope.gridOptions.data.length > 0
            && $scope.startingResultIndex == 0) {
            mixpanel.track("User views engage-actions onboarding");
            localStorage.setItem("engageOnboardingViewed", true);
            $scope.closeGCEAdvice = true;

            var user = $scope.gridOptions.data[0];

            if(user.networkProfiles.linkedin != null){
                user.activeProfile = 'linkedin';
              } else if (user.networkProfiles.twitter != null){
                user.activeProfile = 'twitter';
              }

            startIntro();
        }
    }

    // Checking if terms have changed
    $scope.checkFilterTermsChanges = function(terms) {

        Array.prototype.compare = function(testArr) {
            if (this.length != testArr.length) return false;
            for (var i = 0; i < testArr.length; i++) {
                if (this[i].compare) { //To test values in nested arrays
                    if (!this[i].compare(testArr[i])) return false;
                }
                else if (this[i] !== testArr[i]) return false;
            }
            return true;
        }

        var array1 = terms;
        var array2 = $scope.sourceTerms;

        if(array1.compare(array2)) {
            $scope.runFilter = false;
        } else {
            $scope.runFilter = true;
        }
    }

    // Update watchlist terms
    $scope.updateAddWatchlistTerm = function(term, watchlistName, advancedSearch) {
        $scope.checkFilterTermsChanges($scope.$parent.editWatchlistTerms)

        if($('.edit-watchlist input.terms').val().length > 0) {
            $('.edit-watchlist input.terms').val('');
            $scope.$parent.editWatchlistTerms.push(term);

        } else if ($('.edit-watchlist input').val().length == 0) {
            if($scope.runFilter == true) {
                $scope.editSearch($scope.nameFilter, watchlistName, $scope.$parent.editWatchlistTerms, advancedSearch);
            }
        }
    }

    // Delete terms from watchlist edit
    $scope.updateDeleteWatchlistTerm = function(term) {
        $scope.$parent.editWatchlistTerms.splice(term, 1);
    }

    // Edit watchlist
    $scope.editSearch = function(sourceWatchlist, watchlist, terms, advancedSearch) {
        $scope.triggerLoadingFullScreen();

        //$scope.checkFilterTermsChanges(terms);

        if(terms.length > 0) {
            angular.forEach($scope.savedWatchlists, function (value, i) {
                var watchlistId;

                if (value.name === sourceWatchlist) {
                    watchlistId = value.id;

                    $scope.runFilter = true;

                    var location;
                    var industry;
                    var company;
                    var goal = advancedSearch.searchGoal;
                    var searchCondition = advancedSearch.searchCondition;
                    var negativeTerms = []

                    if(advancedSearch.negativeTerms != undefined
                        && advancedSearch.negativeTerms != "") {
                        negativeTerms = advancedSearch.negativeTerms.split(', ');
                    }

                    if(advancedSearch != undefined) {
                        if(advancedSearch.location != '') {
                            location = advancedSearch.location;
                            localStorage.setItem("defaultLocation", location);
                        }
                        if(advancedSearch.industry != '') {
                            industry = advancedSearch.industry;
                        }
                        if(advancedSearch.company != '') {
                            company = advancedSearch.company;
                        }
                    }

                    $http({
                        url: $scope.apiBaseUrl + "/searches/" + watchlistId,
                        method: "PUT",
                        data: { name: watchlist, searchTerms: terms, searchCondition: searchCondition, negativeTerms: negativeTerms, location : location, industry : industry, company : company, searchGoal: goal }
                    })
                    .then(function () {

                        mixpanel.track("Updates searc", {"Search": watchlist});

                        // Closing edit filter panel
                        $scope.editFilterEnabled = false;

                        // Refresh saved searches
                        $scope.getSavedSearches();

                        $scope.closeLoadingFullScreen();

                        if ($scope.runFilter == true) {
                            $scope.busy = false;
                            wdtLoading.start({
                                category: 'build_' + $scope.selectedLanguage,
                                speed: 2000
                            });
                            $scope.hideNoResults = true;
                            $scope.doFilter(watchlistId, watchlist);
                            $scope.loadMoreTrigger = false;
                            $scope.getFilterData();
                        } else if ($scope.runFilter == false) {
                            $scope.selWatchlist = watchlist;
                            $scope.newSelWatchlist(watchlist);
                        }
                    });
                }
            });
        }
    }

        $scope.watchlistLv0 = function() { mixpanel.track("Opens watchlist level: view all", {"Watchlist": $scope.nameWatchlist}); }
        $scope.watchlistLv1 = function() { mixpanel.track("Opens watchlist level: tap", {"Watchlist": $scope.nameWatchlist}); }
        $scope.watchlistLv2 = function() { mixpanel.track("Opens watchlist level: touch", {"Watchlist": $scope.nameWatchlist}); }
        $scope.watchlistLv3 = function() { mixpanel.track("Opens watchlist level: connect", {"Watchlist": $scope.nameWatchlist}); }
        $scope.watchlistLv4 = function() { mixpanel.track("Opens watchlist level: contact", {"Watchlist": $scope.nameWatchlist}); }
	}
}]);