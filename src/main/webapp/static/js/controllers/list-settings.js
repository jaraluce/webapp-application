angular.module('list-settings', [])

.controller('listSettings',['$scope', '$http', '$location', '$stateParams', '$window', '$uibModal',
function ($scope, $http, $location, $stateParams, $window, $uibModal) {

  var listId = $stateParams.id;
  var currentList;
  
  $scope.getListData = function(listId) {
    

    $http.get($scope.apiBaseUrl + '/lists/' + listId).success(function(string_data) {
      currentList = string_data;

      if(currentList == undefined) {
        $scope.definedList = false;
      } else {
        $scope.currentList = string_data;
        $scope.listNameCopy = angular.copy(string_data.name);

        // Get search
        $http.get($scope.apiBaseUrl + '/searches').success(function(data) {
          var currentSearchFound = false;
          angular.forEach(data, function (value, i) {
            if (listId == value.id) {
              currentSearchFound = true;
              value.negativeTerms = value.negativeTerms.join(', ')
              $scope.currentSearch = value;
              $scope.nameFilter = value.name;

              $scope.editAdvancedSearchFilter = angular.copy(value);
              $scope.editNameFilter = angular.copy(value.name);
              $scope.sourceTerms = angular.copy(value.searchTerms);
              $scope.editWatchlistTerms = [];
              angular.forEach($scope.sourceTerms, function(value, i){
                if($scope.editWatchlistTerms.indexOf(value) == -1) {
                  $scope.editWatchlistTerms.push(value);
                }
              });
            }
          });
          if(!currentSearchFound) {
            $scope.currentSearch = undefined;
          }
        });
	    }
	  });
	}

  if (listId != undefined) {
	 $scope.getListData(listId);
  }

  	// Edit list
    $scope.editList = function(list) {
      if(!$scope.listNameExists
        && list.name && list.name != undefined) {
        $http({
              url: $scope.apiBaseUrl + "/lists/" + list.id,
              method: "PUT",
              data: { name : list.name, monitored : list.monitored }
          })
          .then(function(response) {
              mixpanel.track("Edits list", {"List": list.name});

              $scope.listName = list.name;
              $scope.currentList.name = list.name;
              $scope.getListData(id);
              $scope.getLists();
              
        });
      }
    }

  // Checking if list exists
  $scope.checkListName = function(listName) {
    if (listName) {
      $scope.listNameExists = false;

      angular.forEach($scope.savedLists, function (value, i) {
        if (value.name.toLowerCase() == listName.toLowerCase()) {
          $scope.listNameExists = true;
        }
      });
    }
  }

  // Create list
  $scope.createList = function(listName, closeModal, goToList) {
    if(!$scope.listNameExists
      && listName != undefined
      && listName.length > 0) {
      $scope.triggerLoadingFullScreen();
      $http({
            url: $scope.apiBaseUrl + "/lists/",
            method: "POST",
            data: { name : listName,  monitored : true}
        })
        .then(function(response) {
            if ($scope.savedLists.length == 1) {
              Intercom('trackEvent', 'User creates his first list');
            }
            mixpanel.track("Creates a new list", {"List": listName});
            $('#main-menu .add-list').find('input').val('');
            $scope.listName = '';

            if(closeModal) {
              $scope.hideCreateListModal();
            }

        $scope.setSelectedList(listName);
        $scope.getLists(false, true, listName);

        $http.get($scope.apiBaseUrl + '/lists').success(function(data) {
          $scope.savedLists = data;
          $scope.savedLists = $scope.savedLists.sort($scope.sort_by('name', false, function(a){return a.toLowerCase()}));
          
              if(goToList) {
                $scope.closeLoadingFullScreen();
            $scope.goToList(response.data.id, response.data.name, true);
              } else {
                $scope.closeLoadingFullScreen();
              }
        })

      });
    }
  }


    // SEARCH
    // Checking if terms have changed
    $scope.checkFilterTermsChanges = function(terms) {

        Array.prototype.compare = function(testArr) {
            if (this.length != testArr.length) return false;
            for (var i = 0; i < testArr.length; i++) {
                if (this[i].compare) { //To test values in nested arrays
                    if (!this[i].compare(testArr[i])) return false;
                }
                else if (this[i] !== testArr[i]) return false;
            }
            return true;
        }

        var array1 = terms;
        var array2 = $scope.sourceTerms;

        if(array1.compare(array2)) {
            $scope.runFilter = false;
        } else {
            $scope.runFilter = true;
        }
    }

    // Update watchlist terms
    $scope.updateAddWatchlistTerm = function(term, watchlistName, advancedSearch, bookmarkFromLinkedin) {
        $scope.checkFilterTermsChanges($scope.editWatchlistTerms);

        if($('.edit-watchlist input.terms').val().length > 0) {
            $('.edit-watchlist input.terms').val('');

            var repeatedTerm = false;
            angular.forEach($scope.editWatchlistTerms, function(value, i){
              if (value == term) {
                repeatedTerm = true;
              }
            });

            if (!repeatedTerm) {
              $scope.editWatchlistTerms.push(term);
            }

        } else if ($('.edit-watchlist input').val().length == 0) {
            if($scope.runFilter == true) {
                $scope.editSearch($scope.nameFilter, watchlistName, $scope.editWatchlistTerms, advancedSearch, $scope.currentList.id, bookmarkFromLinkedin, true);
            }
        }
    }

    // Delete terms from watchlist edit
    $scope.updateDeleteWatchlistTerm = function(term) {
        $scope.editWatchlistTerms.splice(term, 1);
    }

    // Add terms to watchlist
  $scope.watchlistTerms = [];

  $scope.addWatchlistTerm = function(term, watchlistName, listId, advancedSearchFilter) {
    if($('.create-context input.create-filter').val().length > 0
      || $('.create-list input.create-filter').val().length > 0) {
      $('.create-context input.create-filter').val('');
      $('.create-list input.create-filter').val('');

      var repeatedTerm = false;
      angular.forEach($scope.watchlistTerms, function(value, i) {
        if (value == term) {
          repeatedTerm = true;
        }
      })

      if (!repeatedTerm) {
        $scope.watchlistTerms.push(term);

        $scope.emptyWatchlistInput = true;
        $scope.newListName = $scope.watchlistTerms[0].substring(0,20);
        $scope.checkNameWatchlist($scope.newListName);
      }

    } else if ($scope.watchlistTerms.length > 0 && $('.create-context input.create-filter').val().length == 0) {
      $scope.saveWatchlist(watchlistName, term, $scope.watchlistTerms, advancedSearchFilter, listId, true, true);
    }
  }

  // Delete terms from watchlist
  $scope.deleteWatchlistTerm = function(term) {
    $scope.watchlistTerms.splice(term, 1);

    if ($scope.watchlistTerms.length == 0) {
      $scope.emptyWatchlistInput = false;
    }
  }

  // Checking if watchlist exists
  $scope.checkNameWatchlist = function(watchlistName) {
    $scope.watchlistNameExists = false;

    angular.forEach($scope.savedWatchlists, function (value, i) {
      if (value.name == watchlistName) {
        $scope.watchlistNameExists = true;
      }
    });
  }

  // Checking watchlist input content
  $scope.checkContextInputLength = function() {
    if ($scope.watchlistTerms.length > 0 && $('.create-context input.create-filter').val().length == 0) {
      $scope.emptyWatchlistInput = true;
    } else {
      $scope.emptyWatchlistInput = false;
    }
  }

  // Save watchlist
  $scope.advancedSearchFilter = {};
    
  $scope.saveWatchlist = function(watchlistName, termsInput, terms, advancedSearchFilter, id, bookmarkFromLinkedin, search) {
    var searchTerms = terms;

    if (searchTerms == 0 && termsInput.length > 0) {
      searchTerms.push(termsInput)
    }

    $scope.saveWatchlistValidatedFields = function() {
      var location;
      var industry;
      var company;
      var goal = advancedSearchFilter.searchGoal;
      var searchCondition = advancedSearchFilter.searchCondition;
      var negativeTerms = []

      if(advancedSearchFilter.negativeTerms != undefined
        && advancedSearchFilter.negativeTerms != "") {
        negativeTerms = advancedSearchFilter.negativeTerms.split(', ');
      }

      if(advancedSearchFilter != undefined) {
        if(advancedSearchFilter.location != '') {
          location = advancedSearchFilter.location;
          localStorage.setItem("defaultLocation", location);
        }
        if(advancedSearchFilter.industry != '') {
          industry = advancedSearchFilter.industry;
        }
        if(advancedSearchFilter.company != '') {
          company = advancedSearchFilter.company;
        }
        if(goal == undefined) {
          goal = 'misc';
        }
      }

          $scope.triggerLoadingFullScreen();

          var linkedinTab = window.open('', '_blank');
            linkedinTab.document.write($scope.translation.LINKEDIN_LOADING);

      $http({
            url: $scope.apiBaseUrl + '/searches',
            method: "POST",
            data: { id: id, name : watchlistName, searchTerms : terms, searchCondition: searchCondition, negativeTerms: negativeTerms, location : location, industry : industry, company : company, searchGoal: goal }
        })
        .then(function successCallback(response) {
          var data = response.data
          mixpanel.track("Creates watchlist", {"Watchlist": data.name, "Terms" : data.searchTerms, "Search condition": searchCondition, "Negative terms": negativeTerms, "Location" : data.location, "Industry" : data.industry, "Company" : data.company});

              $('#new-watchlist-tooltip').slideUp('fast');
              $('.watchlist-list input, #new-watchlist-tooltip input').val('');
              $('.advancedSearch').find("input:not('.advancedLocation')").val('');
              $scope.termsNotDefined = false;
        $scope.newSelWatchlist(watchlistName);
        $scope.watchlistName = null;
        $scope.createListTerms = [];
              $scope.watchlistTerms = [];
              if($scope.advancedSearchFilter.location != undefined) {
          $scope.advancedSearchFilter.location = null;
        }
              if($scope.advancedSearchFilter.industry != undefined) {
          $scope.advancedSearchFilter.industry = null;
        }
        if($scope.advancedSearchFilter.company != undefined) {
          $scope.advancedSearchFilter.company = null;
        }
              $scope.emptyWatchlistInput = false;


              $scope.closeLoadingFullScreen();

              if (bookmarkFromLinkedin == true) {
                  $scope.$parent.checkBookmarkFromLinkedIn = true;

                  $http.get($scope.apiBaseUrl + '/lists/' + id).success(function(string_data) {
                    $scope.$parent.linkedinList = string_data;

                    $http.get($scope.apiBaseUrl + '/searches/' + id).success(function(data) {
                  data.negativeTerms = data.negativeTerms.join(', ')
                  $scope.currentSearch = data;
                  $scope.nameFilter = data.name;

                  $scope.editAdvancedSearchFilter = angular.copy(data);
                  $scope.editNameFilter = angular.copy(data.name);
                  $scope.sourceTerms = angular.copy(data.searchTerms);
                  $scope.editWatchlistTerms = [];
                  angular.forEach($scope.sourceTerms, function(value, i){
                    $scope.editWatchlistTerms.push(value);
                  });
              });
                  });

                  var searchConfig = terms.toString().replace(/,/g , " ");
                  var url = 'https://linkedin.com/search/results/people/?keywords=' + searchConfig + '&origin=CLUSTER_EXPANSION';
                  linkedinTab.location.href = url;
                }

              // if (search) {
              //   $location.path('/list/' + id + '/build');
              // } else {
              //   $location.path('/list/' + id + '/engage');
              // }
              

        }, function errorCallback(response) {
          if (response.status === 409) {
            $scope.feedbackListLimit = true;
          }
        });
    }

    if (watchlistName == undefined
      || watchlistName != undefined
      && !watchlistName.length) {
      $scope.termsNotDefined = true;
    } else {
      if (searchTerms.length > 0) {
        $scope.saveWatchlistValidatedFields();
      }
    }

    // if (advancedSearchFilter != undefined
    //  && advancedSearchFilter.location.length) {
    //  location = advancedSearchFilter.location;
    //  industry = advancedSearchFilter.industry;
    //  company = advancedSearchFilter.company;
    //  localStorage.setItem("defaultLocation", location);
    //  $scope.saveWatchlistValidatedFields();
    // } else if (advancedSearchFilter == undefined
    //  || advancedSearchFilter != undefined
    //  && !advancedSearchFilter.location.length) {
    //  $scope.locationNotDefined = true;
    // }
  }

  // Update watchlist
  // $scope.updateListData = function(watchlist) {
  //  $scope.updateCurrentList = watchlist;      
  // }

  // Backup watchlist data before editin
  $scope.backupWatchlistData = function (watchlist) {
    return watchlist;
  }

  $scope.getWatchlistData = function (watchlist) {
    if(watchlist.negativeTerms != undefined) {
      watchlist.negativeTerms = watchlist.negativeTerms.join(', ')
    }
    return angular.copy(watchlist);
  }

  $scope.watchlistNameUpdateExists = false;
  $scope.checkNameWatchlistUpdate = function (watchlistName, watchlistId) {
    $scope.watchlistNameUpdateExists = false;
    var sourceListName;

    angular.forEach($scope.savedWatchlists, function (value, i) {
      if (value.watchlistId == watchlistId) {
        sourceListName = value.name;
      }
    });

    if (sourceListName != watchlistName) {
      angular.forEach($scope.savedWatchlists, function (value, i) {
        if (value.name == watchlistName) {
          $scope.watchlistNameUpdateExists = true;
        }
      });
    }
  }

    // Edit watchlist
    $scope.editSearch = function(sourceWatchlist, watchlist, newTerms, advancedSearch, id, bookmarkFromLinkedin, search) {
        //$scope.checkFilterTermsChanges(terms);

        if(newTerms.length > 0) {
            $scope.triggerLoadingFullScreen();

            if(bookmarkFromLinkedin) {
              var linkedinTab = window.open('', '_blank');
              linkedinTab.document.write($scope.translation.LINKEDIN_LOADING);
            }
            $scope.runFilter = true;

            var location;
            var industry;
            var company;
            var goal = advancedSearch.searchGoal;
            var searchCondition = advancedSearch.searchCondition;
            var negativeTerms = []

            if(advancedSearch.negativeTerms != undefined
                && advancedSearch.negativeTerms != "") {
                negativeTerms = advancedSearch.negativeTerms.split(', ');
            }

            if(advancedSearch != undefined) {
                if(advancedSearch.location != '') {
                    location = advancedSearch.location;
                    localStorage.setItem("defaultLocation", location);
                }
                if(advancedSearch.industry != '') {
                    industry = advancedSearch.industry;
                }
                if(advancedSearch.company != '') {
                    company = advancedSearch.company;
                }
            }

            $http({
                url: $scope.apiBaseUrl + "/searches/" + id,
                method: "PUT",
                data: { name: watchlist, searchTerms: newTerms, searchCondition: searchCondition, negativeTerms: negativeTerms, location : location, industry : industry, company : company, searchGoal: goal }
            })
            .then(function () {

                mixpanel.track("Updates search", {"Search": watchlist});

                // Closing edit filter panel
                $scope.editFilterEnabled = false;

                // Refresh saved searches
                $scope.getSavedSearches();
                $scope.getListData(id);

                $scope.closeLoadingFullScreen();

                $scope.selWatchlist = watchlist;
                $scope.newSelWatchlist(watchlist);

                if (bookmarkFromLinkedin == true) {
                  $scope.$parent.checkBookmarkFromLinkedIn = true;
                  $scope.$parent.linkedinList = $scope.currentList;

                  $scope.getListData(id);

                  var searchConfig = newTerms.toString().replace(/,/g , " ");
                  var url = 'https://linkedin.com/search/results/people/?keywords=' + searchConfig + '&origin=CLUSTER_EXPANSION';
                  linkedinTab.location.href = url;
                }

                if (!bookmarkFromLinkedin) {
                  if (search) {
                    $location.path('/list/' + id + '/build');
                  } else {
                    $location.path('/list/' + id + '/engage');
                  }
                }
            });
        }
    }

    //AUTOMATION
  // Automation modal
  $scope.showAutomationModal = function(listId) {
    $scope.automationListId = listId;
      var modalInstance = $uibModal.open({
        animation: false,
        templateUrl: '/static/templates/layout-elements/automation_modal.html',
        scope: $scope,
        windowClass: 'automationModal'
      });

      // Hide policy modal
      $scope.hideAutomationModal = function() {
        modalInstance.close();
      }
    }

    // Add automation fields
    $scope.addAutomationFields = function(stage) {
      stage.push({});
    }

    // Get current automation configuration
    $scope.getCurrentAutomation = function(rules, custom) {
      if (rules == null) {
        rules = [];
      } else {
        rules = rules;
      }

      if (rules.duration == undefined
        || !rules.custom) {
        rules.duration = 2;
      }

      function returnValues(obj) {
          var arr = [];
          for (var key in obj)
             arr.push(obj[key]);
          return arr;
      }

      if (custom) {

        if (rules.stage1 == undefined) {
          rules.stage1 = {};
        }


        if (rules.stage1.force == undefined) {
          rules.stage1.force = false;
        }

        if (rules.stage2 != undefined) {
            rules.stage2.reply = returnValues(rules.stage2.reply);

          if (rules.stage2.reply.length == 0) {
            rules.stage2.reply = [];
          }

          if (rules.stage2.force == undefined) {
            rules.stage2.force = false;
          }
        } else {
          rules.stage2 = {};
          rules.stage2.reply = [];
          rules.stage2.force = false;
        }

        if (rules.stage3 == undefined) {
          rules.stage3 = {};
          rules.stage3.message = $scope.translation.DEFAULT_AUTOMATION_STAGE3_VALUE;
        } else if (rules.stage3 != undefined) {
          if (rules.stage3.message == ''
            || rules.stage3.message == null) {
            rules.stage3.message = $scope.translation.DEFAULT_AUTOMATION_STAGE3_VALUE;
          }
        }
        

        if (rules.stage4 != undefined) {
          rules.stage4.message = returnValues(rules.stage4.message);

          if (rules.stage4.message.length == 0) {
            rules.stage4.message = [{}];
            rules.stage4.message[0].text = $scope.translation.DEFAULT_AUTOMATION_STAGE4_VALUE;
          }
        } else {
          rules.stage4 = {};
          rules.stage4.message = [{}];
          rules.stage4.message[0].text = $scope.translation.DEFAULT_AUTOMATION_STAGE4_VALUE;
        }

        if (rules.duration == undefined) {
          rules.stage1.force = true;
          rules.stage2.force = true;
        }

        return rules;
      }

      // Default settings
      if (!custom) {
        rules = {};
        rules.custom = false;
        rules.duration = 1;
        rules.stage1 = {};
        rules.stage2 = {};
        rules.stage3 = {};
        rules.stage4 = {};
        rules.stage1.likesNumber = 2;
        rules.stage1.force = false;
        rules.stage2.force = false;
        rules.stage2.reply = undefined;
        rules.stage3.preferredNetwork = 'all';
        rules.stage4.message = undefined;
        listId = currentList.id;

        $scope.submitAutomation(rules, false, listId);
      }
    }

    // Automation submit
    $scope.submitAutomation = function(rules, custom, listId) {
      $scope.triggerLoadingFullScreen();

      $http({
        url: $scope.apiBaseUrl + '/lists/' + listId + '/automation',
        method: 'PUT',
        data: {'duration' : rules.duration,
            'custom' : custom,
            'stage1' : rules.stage1,
            'stage2' : rules.stage2,
            'stage3' : rules.stage3,
            'stage4' : rules.stage4
          }
      }).then(function successCallback(response) {
        $scope.getListData(listId);
        $scope.closeLoadingFullScreen();
        $scope.currentList.automationConfiguration = rules;
      });
    }

    // Delete automation
    $scope.deleteAutomation = function() {
      var listId = $scope.currentList.id;

      $http({
            url: $scope.apiBaseUrl + '/lists/' + listId + '/automation',
            method: 'delete',
        }).then(function successCallback(response) {
          $scope.currentList.automationConfiguration = null;
        });
    }


  // TEMPLATES
  // Get templates
  $scope.getTemplates = function(setSelTemplate) {
    // Default templates
    $scope.templates = [
      {
        id: null,
        name: $scope.translation.GENERIC_MESSAGE + ' ' + $scope.translation.TEMPLATE_BY_RELEVANTE,
        text: $scope.translation.DEFAULT_AUTOMATION_STAGE3_VALUE,
        type: 'connect'
      },
      {
        id: null,
        name: $scope.translation.INDUSTRY + ' ' + $scope.translation.TEMPLATE_BY_RELEVANTE,
        text: $scope.translation.DEFAULT_AUTOMATION_STAGE3_INDUSTRY,
        type: 'connect'
      },
      {
        id: null,
        name: $scope.translation.JOB_TITLE + ' ' + $scope.translation.TEMPLATE_BY_RELEVANTE,
        text: $scope.translation.DEFAULT_AUTOMATION_STAGE3_JOB_TITLE,
        type: 'connect'
      },
      {
        id: null,
        name: $scope.translation.POST_TITLE + ' ' + $scope.translation.TEMPLATE_BY_RELEVANTE,
        text: $scope.translation.DEFAULT_AUTOMATION_STAGE3_POST_TITLE,
        type: 'connect'
      },
      {
        id: null,
        name: $scope.translation.DEFAULT_AUTOMATION_STAGE4_NAME + ' ' + $scope.translation.TEMPLATE_BY_RELEVANTE,
        text: $scope.translation.DEFAULT_AUTOMATION_STAGE4_VALUE,
        type: 'message'
      },
    ]

    // User's custom templates
    $http.get($scope.apiBaseUrl + '/templates').success(function(data) {
      angular.forEach(data, function (value, i) {
        $scope.templates.push(value)
      });

      // Set selTemplate after saving a new template
      if(setSelTemplate) {
        var array = $scope.templates;
        $scope.selTemplate = array[array.length - 1];
      }
    });
  }

  // Check selected template
  $scope.checkSelTemplate = function(message, type){
    $scope.selTemplate = null;
    angular.forEach($scope.templates, function (value, i) {
      if($scope.currentList!= undefined) {
        if (value.listId == $scope.currentList.id
            || !value.listId
            || value.listId == null
            || value.listId == undefined) {
          if(value.text === message && value.type == type) {
            $scope.selTemplate = value;
          }
        }
      }
    });
  }

  // Save template
  $scope.saveTemplate = function(name, text, parent, type) {
    if (name && text) {
      $scope.templateNameRepeated = false;
      $scope.templateTextRepeated = false;
      angular.forEach($scope.templates, function (value, i) {
        if(name == value.name) {
          $scope.templateNameRepeated = true;
        }
        if(text == value.text) {
          $scope.templateTextRepeated = true;
        }
      });

      if($scope.templateNameRepeated) {
        alert($scope.translation.TEMPLATE_NAME_REPEATED)
      } else if($scope.templateTextRepeated) {
        alert($scope.translation.TEMPLATE_TEXT_REPEATED)
      }

      if(!$scope.templateNameRepeated
        && !$scope.templateTextRepeated) {

        var listId;
        if(parent) {
          listId = $scope.currentList.id;
        } else {
          listId = null
        }

        $http({
          url: $scope.apiBaseUrl + '/templates',
          method: 'POST',
          data: {'name' : name, 'text' : text, 'listId': listId, 'type': type}
        }).then(function successCallback(response) {
            $scope.getTemplates(true);
        });
      }
    } else {
      alert($scope.translation.NAME_TEXT_CAN_NOT_BE_EMPTY);
    }
  };

  // Delete template
  $scope.deleteTemplate = function(template) {
    $http({
      url: $scope.apiBaseUrl + '/templates/' + template.id,
      method: 'DELETE'
    }).then(function successCallback(response) {
        $scope.getTemplates(false);
    });
  }

  // Update template
  $scope.updateTemplate = function(template, name, text, parent, type) {
    if(!name) {
      name = template.name
    }
    if (name && text) {
      $scope.templateNameRepeated = false;
      $scope.templateTextRepeated = false;
      angular.forEach($scope.templates, function (value, i) {
        if(name != template.name && name == value.name) {
          $scope.templateNameRepeated = true;
        }
        if(text != template.text && text == value.text) {
          $scope.templateTextRepeated = true;
        }
      });

      if($scope.templateNameRepeated) {
        alert($scope.translation.TEMPLATE_NAME_REPEATED)
      } else if($scope.templateTextRepeated) {
        alert($scope.translation.TEMPLATE_TEXT_REPEATED)
      }

      if(!$scope.templateNameRepeated
        && !$scope.templateTextRepeated) {

        var listId;
        if(parent) {
          listId = $scope.currentList.id;
        } else {
          listId = null
        }

        $http({
          url: $scope.apiBaseUrl + '/templates/' + template.id,
          method: 'PUT',
          data: {'name' : name, 'text' : text, 'listId': listId, 'type': type}
        }).then(function successCallback(response) {
            $scope.getTemplates(false);
        });
      }
    } else {
      alert($scope.translation.NAME_TEXT_CAN_NOT_BE_EMPTY);
    }
  };

  // UPLOAD CSV
  $scope.fileDataObj = {};
  $scope.matchedCsvTitles = [];
  $scope.getCsvMandatoryFields = function() {
    return [
      {
        name: $scope.translation.LINKEDIN_PROFILE,
        class: 'linkedin',
        icon: 'fa fa-linkedin-square',
        key: 'linkedinUrl',
        required: true
      },
      {
        name: $scope.translation.TWITTER_PROFILE,
        class: 'twitter',
        icon: 'fa fa-twitter',
        key: 'twitterScreenName',
        required: true
      },
      {
        name: $scope.translation.EMAIL,
        class: 'email',
        icon: 'fa fa-envelope',
        key: 'email',
        required: true
      },
      {
        name: $scope.translation.NAME,
        class: 'common-field',
        icon: 'fa fa-user',
        key: 'name',
        required: false
      },
      {
        name: $scope.translation.HEADLINE,
        class: 'common-field',
        icon: 'fa fa-suitcase',
        key: 'headline',
        required: false
      },
      {
        name: $scope.translation.COMPANY,
        class: 'common-field',
        icon: 'fa fa-building',
        key: 'company',
        required: false
      },
      {
        name: $scope.translation.LOCATION,
        class: 'common-field',
        icon: 'fa fa-map-marker',
        key: 'location',
        required: false
      },
      {
        name: $scope.translation.INDUSTRY,
        class: 'common-field',
        icon: 'fa fa-industry',
        key: 'industry',
        required: false
      },
      {
        name: $scope.translation.PHONE,
        class: 'common-field',
        icon: 'fa fa-phone',
        key: 'phoneNumber',
        required: false
      },
      {
        name: $scope.translation.TAGS,
        class: 'common-field',
        icon: 'fa fa-tag',
        key: 'tags',
        required: false
      },

    ];
  }
    
    $scope.uploadFile = function(fileContent) {
      if (fileContent) {
        $scope.uploadedFileContent = fileContent;

        var headersSource = fileContent.split(/\r\n|\n/)
        headersSource[0] = headersSource[0].replace(/['"]+/g, '');
        $scope.uploadedFileData = headersSource[0].split(';').join(',').split(',');
        $scope.uploadedFileDataCopy = angular.copy($scope.uploadedFileData);
      }
    }

    // Set active row
    $scope.setRowTitleActive = function(item, num) {
    if (item === $scope.selected) {
          $scope.rowTitleSelected = null;
      } else {
          $scope.rowTitleSelected = item;
          $scope.rowTitleIndex = num;
      }
  };

  $scope.isRowTitleActive = function(item) {
    return $scope.rowTitleSelected === item;
  }

  $scope.rowTitleUsed = function(item) {
    var used = false
    angular.forEach($scope.csvMatches, function (value, i) {
      if (value.csvRowTitle == item) {
        used = true;
      }
    });
    return used;
  }

  // Set active mandatory field
  $scope.setCsvMandatoryFieldActive = function(item, num) {
    if (item === $scope.selected) {
          $scope.csvMandatoryFieldSelected = null;
      } else {
          $scope.csvMandatoryFieldSelected = item;
      }
  };

  $scope.isCsvMandatoryFieldActive = function(item) {
    return $scope.csvMandatoryFieldSelected === item;
  }

  $scope.csvMandatoryFieldUsed = function(item) {
    var used = false
    angular.forEach($scope.csvMatches, function (value, i) {
      if (value.csvMandatoryField.name == item.name) {
        used = true;
      }
    });
    return used;
  }

  $scope.hoverCsvMatchIn = function(){
        this.hoverCsvMatch = true;
    };

    $scope.hoverCsvMatchOut = function(){
        this.hoverCsvMatch = false;
    };

  // Save csv match
  $scope.csvMatches = [];
  $scope.saveCsvMatch = function(rowTitle, rowTitleIndex, csvMandatoryField) {
    if (rowTitle != undefined && csvMandatoryField != undefined) {
            $scope.uploadedFileDataCopy[rowTitleIndex] = csvMandatoryField.key;
            $scope.csvMatches.push({
              csvRowTitle: rowTitle,
              csvRowTitleIndex: rowTitleIndex,
              csvMandatoryField: csvMandatoryField
            });
            $scope.rowTitleSelected = null;
            $scope.csvMandatoryFieldSelected = null;
    }
  }

  $scope.deleteCsvMatch = function(matchIndex, rowTitle, rowTitleIndex) {
    $scope.uploadedFileDataCopy[rowTitleIndex] = rowTitle;
      $scope.csvMatches.splice(matchIndex, 1);
  }

  $scope.requiredMatch = function() {
    var matched = false;
    $scope.csvMatches.forEach(function(item, i) {
      if (item.csvMandatoryField.required == true) {
        matched = true;
      }
    });
    return matched;
  }

  $scope.submitCsvFile = function() {
    var csv = $scope.uploadedFileContent;
    var newCsvHeader = $scope.uploadedFileDataCopy;
    var profiles = [];

    var lines = csv.split(/\r\n|\n/)
    lines.forEach(function(item, i) {
      item = item.replace(/['"]+/g, '');
      item = item.split(';').join(',').split(',');
      if(i !== 0) {
        var itemProfile = {};
        itemProfile.additionalData = {};
        item.forEach(function(value, i) {
          if (newCsvHeader[i] == "linkedinUrl"
            || newCsvHeader[i] == "twitterScreenName"
            || newCsvHeader[i] == "email"
            || newCsvHeader[i] == "name"
            || newCsvHeader[i] == "headline"
            || newCsvHeader[i] == "company"
            || newCsvHeader[i] == "location"
            || newCsvHeader[i] == "industry"
            || newCsvHeader[i] == "phoneNumber"
            || newCsvHeader[i] == "tags") {

            itemProfile[newCsvHeader[i]] = value;
          } else {
            itemProfile.additionalData[newCsvHeader[i]] = value;
          }
        });
        profiles.push(itemProfile)
      }
    })

    console.log("listId: " + listId)
    console.log("profiles: %O", profiles)
  
    // Submit profiles to API
    $http({
        url: $scope.apiBaseUrl + "/profiles/import",
        method: "POST",
        data: { profiles : profiles, listId : listId }
    })
    .then(function(response) {
       $scope.goToList(listId, $scope.currentList.name, false);
    }, function errorCallback(response) {
        if (response.status === 412) {
          $scope.openEnrichmentBudgetModal();
        }
    });
  }

}]);