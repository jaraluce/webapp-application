angular.module('blacklist', [])

.controller('blacklist',['$scope', '$http',
function ($scope, $http) {
    mixpanel.track("Opens blacklist");
    $scope.loadEnabled = false;
    $scope.blacklist = true;
    $scope.busy = true;
    $scope.cardsView = true;
    $scope.linkedinEnabled = true;
    $scope.twitterEnabled = true;
    $scope.nameWatchlist = ''; 
    $scope.emptyBlacklist = false;
    $scope.blacklist = true;

	var controller = this;
    controller.contactList = [];
	var contactList = [];
	$scope.contactList = [];


    var loadNum = 12;
    var start = 0;
    var end = loadNum;

    // Loading list view with full response
    $scope.infiniteScrollDisabled = false;

    $http({
        url: $scope.apiBaseUrl + '/blacklist',
        method: 'GET',
        params: {total: 1000}
    }).then(function(response) {
        mixpanel.track(response.data.length + " results shown");
        $scope.hideContextList();
        angular.forEach(response.data, function(value, i){

            // Order keywords
            if(value.keywords.length > 1) {
              value.keywords = value.keywords.sort(function (a, b) {
                  return a.toLowerCase().localeCompare(b.toLowerCase());
                });
            }

            // Order lists
            if(value.watchlists.length > 1) {
              value.watchlists = value.watchlists.sort(function (a, b) {
                  return a.toLowerCase().localeCompare(b.toLowerCase());
                });
            }

            // Order tags
            if(value.tags.length > 1) {
              value.tags = value.tags.sort(function (a, b) {
                  return a.toLowerCase().localeCompare(b.toLowerCase());
                });
            }

            // Setting current engage level
            if(value.level1Done) {
              value.currentEngageLevel = '2. REPLY';
            } else if(value.level2Done) {
              value.currentEngageLevel = '3. CONNECT';
            } else if(value.level3Done) {
              value.currentEngageLevel = '4. MESSAGE';
            } else if(value.level4Done) {
              value.currentEngageLevel = 'SALES READY';
            } else if (!value.level1Done
              && !value.level2Done
              && !value.level3Done
              && !value.level4Done) {
              value.currentEngageLevel = '1. LIKE';
            }

            // Setting network profile
            value.networks = [];
            if(value.networkProfiles.linkedin != undefined) {
              value.networks.push('linkedin');
              if (value.networkProfiles.linkedin.groups.length > 0) {
                value.groupsName = [];
                var groupsName = value.groupsName;
                angular.forEach(value.networkProfiles.linkedin.groups, function(value, i){
                  groupsName.push(value);
                });  
              }
            }
            if(value.networkProfiles.twitter != undefined) {
                value.networks.push('twitter');
            }
        });
        $scope.gridOptions.data = response.data;
        $scope.loadEnabled = true;
        $scope.loadMore();
    });

    $scope.loadMore = function() {
        if($scope.gridOptions.data != undefined && $scope.loadEnabled == true) {
            angular.forEach($scope.gridOptions.data, function(value, i){
                if( i >= start && i < end) {
                    value.activeProfile = 'relevante';
                    value.justAddedToWatchlist = false;
                    controller.contactList.push($scope.gridOptions.data[i]);
                }
            })

            if($scope.gridOptions.data.length == 0) {
              mixpanel.track("Blacklist is empty");
              $scope.busy = false;
              $scope.infiniteScrollDisabled = true;
              $scope.allResultsLoaded = true;
              $scope.emptyBlacklist = true;
            } else if ($scope.gridOptions.data.length - end > 0) {
              $scope.busy = false;
              $scope.loadingResults = false;
              $scope.infiniteScrollDisabled = false;
              $scope.loadMoreTrigger = true;
            } else if ($scope.gridOptions.data.length - end < 1) {
              $scope.loadMoreTrigger = false;
              $scope.busy = false;
              $scope.infiniteScrollDisabled = true;
              $scope.allResultsLoaded = true;
              $scope.resultsLength = $('ul.contacts-list').find('.item-wrap:visible').length;
            }

            start = start + loadNum;
            end = end + loadNum;

            $scope.contactList = controller.contactList;
        }
    }	
}]);