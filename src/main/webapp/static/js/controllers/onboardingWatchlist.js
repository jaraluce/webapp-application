angular.module('onboardingWatchlist', [])

.controller('onboardingWatchlist',['$scope', '$http', '$location',
function ($scope, $http, $location) {
    $scope.busy = true;
    $scope.linkedinEnabled = true;
    $scope.twitterEnabled = true;

    $scope.nameWatchlist = 'Onboarding watchlist';

    $http.get($scope.apiBaseUrl + '/watchlists?name=' + $scope.nameWatchlist).success(function(string_data) {
        $scope.watchlistId = string_data.watchlistId;

        $scope.infiniteScrollDisabled = false;
        var controller = this;
        controller.contactList = [];
        var contactList = [];
        $scope.contactList = [];

        $scope.loadMoreTrigger = false;
        $scope.loadingResults = true;
        if ($scope.infiniteScrollDisabled) return;
        $scope.infiniteScrollDisabled = true;

        $http({
            url: $scope.apiBaseUrl + '/watchlists/' + $scope.watchlistId + "/profiles",
            method: 'GET',
            params: {startingIndex: 0, endIndex: 12},
        }).then(function(response) {
            $scope.response = response.data;

            if ($scope.response.length > 0) {

                angular.forEach($scope.response, function(value, i){
                    controller.contactList.push(response.data[i]);

                    if (value.shares.length > 0) {
                        var currentShare = value.shares[0];

                        if (currentShare.like == true
                            || currentShare.retweet == true
                            || currentShare.fav == true) {
                            value.actionDoneLv1 = true;
                        }
                    }
                })

                $scope.busy = false;
                $scope.loadingResults = false;

                $scope.allResultsLoaded = true;

                $scope.contactList = controller.contactList;

            } else if ($scope.response.length == 0) {
                mixpanel.track("No results shown for this watchlist");
                $scope.busy = false;
                $scope.infiniteScrollDisabled = true;
                $scope.allResultsLoaded = true;
            }

            $scope.loadingResults = false;

        }).finally(function () {
            if ($scope.response.length == loadNum) {
                $scope.infiniteScrollDisabled = false;
                $scope.loadMoreTrigger = true;
            } else if ($scope.response.length < loadNum) {
                $scope.busy = false;
                $scope.infiniteScrollDisabled = true;
                $scope.allResultsLoaded = true;
                $scope.resultsLength = $('ul.contacts-list').find('.item-wrap:visible').length;
            }
        });
    });

    // Onboarding
    $scope.cardWatchlistRender = function() {
        if ($scope.isCurrentPath('/onboardingWatchlist/level0') == true) {
            startIntroLevel0();
        } else if ($scope.isCurrentPath('/onboardingWatchlist/level1') == true) {
            startIntroTap();
        } else if ($scope.isCurrentPath('/onboardingWatchlist/level2') == true) {
            startIntroTouch();
        } else if ($scope.isCurrentPath('/onboardingWatchlist/level3') == true) {
            startIntroConnect();
        } else if ($scope.isCurrentPath('/onboardingWatchlist/level4') == true) {
            startIntroContact();
        }
    }

    // Onboarding Level 0
    function startIntroLevel0() {
        var intro = introJs();
          intro.setOptions({
            'tooltipPosition': 'auto',
            'showStepNumbers': 'false',
            'showBullets': false,
            steps: [
              {
                element: '#step-watchlistLevels',
                intro: $scope.translation.OB_STEP_WATCHLISTLEVELS,
                position: 'bottom'
              }
            ]
          });

        intro.onchange(function(targetElement) {  
            switch (targetElement.id) 
                {
                case "step-watchlistLevels":
                    $('.discover, .engage').removeClass('hover');
                break;
                }
        });

        intro.setOption('doneLabel', $scope.translation.OB_GO_TO_TAP).start().oncomplete(function() {
            $location.path("/onboardingWatchlist/level1");
        });

        intro.onexit(function() {
            mixpanel.track("User closes onboarding");
            $scope.selWatchlist = $scope.initialWatchlist;
            $scope.newSelWatchlist($scope.initialWatchlist);
            $scope.deleteWatchlist('Onboarding watchlist');
            window.location.href = "/";
        });
    }

    // Onboarding Tap
    function startIntroTap() {
        var intro = introJs();
          intro.setOptions({
            'tooltipPosition': 'auto',
            'showStepNumbers': 'false',
            'showBullets': false,
            steps: [
              {
                element: '#step-tap',
                intro: $scope.translation.OB_STEP_WATCHLISTLEVEL1,
                position: 'bottom'
              }
            ]
          });

        intro.setOption('doneLabel', $scope.translation.OB_GO_TO_TOUCH).start().oncomplete(function() {
            $location.path("/onboardingWatchlist/level2");
        });

        intro.onexit(function() {
            mixpanel.track("User closes onboarding");
            $scope.selWatchlist = $scope.initialWatchlist;
            $scope.newSelWatchlist($scope.initialWatchlist);
            $scope.deleteWatchlist('Onboarding watchlist');
            window.location.href = "/";
        });
    }

    // Onboarding Touch
    function startIntroTouch() {
        var intro = introJs();
          intro.setOptions({
            'tooltipPosition': 'auto',
            'showStepNumbers': 'false',
            'showBullets': false,
            steps: [
              {
                element: '#step-touch',
                intro: $scope.translation.OB_STEP_WATCHLISTLEVEL2,
                position: 'bottom'
              }
            ]
          });

        intro.setOption('doneLabel', $scope.translation.OB_GO_TO_CONNECT).start().oncomplete(function() {
            $location.path("/onboardingWatchlist/level3");
        });

        intro.onexit(function() {
            mixpanel.track("User closes onboarding");
            $scope.selWatchlist = $scope.initialWatchlist;
            $scope.newSelWatchlist($scope.initialWatchlist);
            $scope.deleteWatchlist('Onboarding watchlist');
            window.location.href = "/";
        });
    }

    // Onboarding Connect
    function startIntroConnect() {
        var intro = introJs();
          intro.setOptions({
            'tooltipPosition': 'auto',
            'showStepNumbers': 'false',
            'showBullets': false,
            steps: [
              {
                element: '#step-connect',
                intro: $scope.translation.OB_STEP_WATCHLISTLEVEL3,
                position: 'bottom'
              }
            ]
          });

        intro.setOption('doneLabel', $scope.translation.OB_GO_TO_CONTACT).start().oncomplete(function() {
            $location.path("/onboardingWatchlist/level4");
        });

        intro.onexit(function() {
            mixpanel.track("User closes onboarding");
            $scope.selWatchlist = $scope.initialWatchlist;
            $scope.newSelWatchlist($scope.initialWatchlist);
            $scope.deleteWatchlist('Onboarding watchlist');
            window.location.href = "/";
        });
    }

    // Onboarding Contact
    function startIntroContact() {
        var intro = introJs();
          intro.setOptions({
            'tooltipPosition': 'auto',
            'showStepNumbers': 'false',
            'showBullets': false,
            steps: [
              {
                element: '#step-contact',
                intro: $scope.translation.OB_STEP_WATCHLISTLEVEL4,
                position: 'bottom'
              }
            ]
          });

        intro.setOption('nextLabel', $scope.translation.OB_NEXT)
        .setOption('prevLabel', $scope.translation.OB_BACK)
        .setOption('skipLabel', $scope.translation.OB_SKIP)
        .setOption('doneLabel', $scope.translation.OB_DONE).start();

        intro.oncomplete(function() {
            mixpanel.track("User completes onboarding");
            $scope.selWatchlist = $scope.initialWatchlist;
            $scope.newSelWatchlist($scope.initialWatchlist);
            $scope.deleteWatchlist('Onboarding watchlist');
            window.location.href = "/";
        });

        intro.onexit(function() {
            mixpanel.track("User closes onboarding");
            $scope.selWatchlist = $scope.initialWatchlist;
            $scope.newSelWatchlist($scope.initialWatchlist);
            $scope.deleteWatchlist('Onboarding watchlist');
            window.location.href = "/";
        });
    }
}]);