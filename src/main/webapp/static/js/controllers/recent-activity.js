angular.module('recent-activity', [])

.controller('recent-activity',['$scope', '$http', '$location',
function ($scope, $http, $location) {
    $scope.loadEnabled = false;
    $scope.cardsView = true;
	$scope.busy = true;
	$scope.linkedinEnabled = true;
	$scope.twitterEnabled = true;
    $scope.nameWatchlist = '';
	
	var controller = this;
	controller.contactList = [];

	//Get recent activity		
	$scope.contactList = [];

	var loadNum = 12;
	var start = 0;
	var end = loadNum;

    // Loading list view with full response
    $scope.infiniteScrollDisabled = false;

    $http({
        url: $scope.apiBaseUrl + '/my-universe/profiles',
        method: 'GET',
        params: {total: 1000}
    }).then(function(response) {
        mixpanel.track(response.data.length + " results shown");
        $scope.hideContextList();
        angular.forEach(response.data, function(value, i){

            // Order keywords
            if(value.keywords.length > 1) {
              value.keywords = value.keywords.sort(function (a, b) {
                  return a.toLowerCase().localeCompare(b.toLowerCase());
                });
            }

            // Order lists
            if(value.watchlists.length > 1) {
              value.watchlists = value.watchlists.sort(function (a, b) {
                  return a.toLowerCase().localeCompare(b.toLowerCase());
                });
            }

            // Order tags
            if(value.tags.length > 1) {
              value.tags = value.tags.sort(function (a, b) {
                  return a.toLowerCase().localeCompare(b.toLowerCase());
                });
            }

            // Setting current engage level
            if(value.level1Done) {
              value.currentEngageLevel = '2. REPLY';
            } else if(value.level2Done) {
              value.currentEngageLevel = '3. CONNECT';
            } else if(value.level3Done) {
              value.currentEngageLevel = '4. MESSAGE';
            } else if(value.level4Done) {
              value.currentEngageLevel = 'SALES READY';
            } else if (!value.level1Done
              && !value.level2Done
              && !value.level3Done
              && !value.level4Done) {
              value.currentEngageLevel = '1. LIKE';
            }

            if (value.name == null) {
              value.name = '';
            }

            if (value.location == null) {
              value.location = '';
            }

            // Setting network profile
            value.networks = [];
            if(value.networkProfiles.linkedin != undefined) {
                value.networks.push('linkedin');
              if (value.networkProfiles.linkedin.groups.length > 0) {
                value.groupsName = [];
                var groupsName = value.groupsName;
                angular.forEach(value.networkProfiles.linkedin.groups, function(value, i){
                  groupsName.push(value);
                });  
              }
            }
            if(value.networkProfiles.twitter != undefined) {
                value.networks.push('twitter');
            }
        });
        $scope.gridOptions.data = response.data;
        $scope.loadEnabled = true;
        $scope.loadMore();
        $scope.showCustomSearchButton = true;
    });


    $scope.loadMore = function() {
        if($scope.gridOptions.data != undefined && $scope.loadEnabled == true) {
            angular.forEach($scope.gridOptions.data, function(value, i){
                if( i >= start && i < end) {
                    value.activeProfile = 'relevante';
                    value.justAddedToWatchlist = false;
                    controller.contactList.push($scope.gridOptions.data[i]);
                }
            })

            $scope.hideNoResults = true;

            if($scope.gridOptions.data.length == 0) {
              mixpanel.track("No results shown at recent activity");
              $scope.busy = false;
              $scope.infiniteScrollDisabled = true;
              $scope.allResultsLoaded = true;
              $scope.emptyUniverseMsg = true;
              $scope.resultsLength = $('ul.contacts-list').find('.item-wrap:visible').length;
            } else if ($scope.gridOptions.data.length - end > 0) {
              $scope.busy = false;
              $scope.loadingResults = false;
              $scope.infiniteScrollDisabled = false;
              $scope.loadMoreTrigger = true;
            } else if ($scope.gridOptions.data.length - end < 1) {
              $scope.loadMoreTrigger = false;
              $scope.busy = false;
              $scope.infiniteScrollDisabled = true;
              $scope.allResultsLoaded = true;
              $scope.resultsLength = $('ul.contacts-list').find('.item-wrap:visible').length;
            }

            start = start + loadNum;
            end = end + loadNum;

            $scope.contactList = controller.contactList;
        }
    }

    $scope.startOnboarding = function () {
        mixpanel.track("User plays onboarding");
        startIntro();
    }

    function startIntro(){
    var intro = introJs();
      intro.setOptions({
        'tooltipPosition': 'auto',
        'showStepNumbers': 'false',
        'showBullets': false,
        steps: [
          {
            element: document.querySelector('#nav-title'),
            intro: $scope.translation.OB_WELCOME + $scope.userName + '!</h1>' + $scope.translation.OB_NAV_TITLE
          },
          {
            element: document.querySelectorAll('#step-search')[0],
            intro: $scope.translation.OB_STEP_SEARCH
          },
          {
            element: '#step-discover',
            intro: $scope.translation.OB_STEP_DISCOVER,
            position: 'right'
          },
          {
            element: '#item0',
            intro: $scope.translation.OB_STEP_CARD_WITH_SIGNALS,
            position: 'right'
          },
          {
            element: '#step-createWatchlist',
            intro: $scope.translation.OB_STEP_CREATEWATCHLIST,
            position: 'left'
          }
        ]
      });
        intro.onchange(function(targetElement) {  
            switch (targetElement.id) 
                {
                case "step-createWatchlist": 
                    $('.search-wrap input').removeClass('hover');
                    $('#new-watchlist-onboarding').show();
                    $('#new-watchlist-onboarding input').val('Onboarding watchlist');
                    setTimeout(function(){
                        $('li#0').find('.action.watchlist').trigger('click');
                    },500);
                    setTimeout(function(){
                        $('li#1').find('.action.watchlist').trigger('click');
                    },1000);
                    setTimeout(function(){
                        $('li#2').find('.action.watchlist').trigger('click');
                    },1500);
                    setTimeout(function(){
                        $('li#3').find('.action.watchlist').trigger('click');
                    },2000);
                break;
                case "step-discover": 
                    $('#new-watchlist-onboarding').hide();
                    $('.discover').addClass('hover');
                    setTimeout(function(){
                        $('input.create-filter').val('social selling');
                    },500);
                break;
                case "item0": 
                    $('.discover').removeClass('hover');
                break;
                }
        }).onbeforechange(function(targetElement) {  
            switch (targetElement.id) 
                {
                case "nav-title": 
                    $scope.isNewUser = false;
                    $('.search-wrap input').addClass('hover');
                break;
                case "step-search": 
                    $scope.saveWatchlist('Onboarding watchlist');
                    $scope.selWatchlist = 'Onboarding watchlist';
                break;
                }
        });

        intro.setOption('nextLabel', $scope.translation.OB_NEXT)
        .setOption('prevLabel', $scope.translation.OB_BACK)
        .setOption('skipLabel', $scope.translation.OB_SKIP)
        .setOption('doneLabel', $scope.translation.OB_GO_TO_WATCHLIST).start().oncomplete(function() {
            $location.path("/onboardingWatchlist/level0");
        });

        intro.onexit(function() {
            mixpanel.track("User closes onboarding");
            $scope.selWatchlist = $scope.initialWatchlist;
            $scope.newSelWatchlist($scope.initialWatchlist);
            $scope.deleteWatchlist('Onboarding watchlist');
            window.location.href = "/";
        });
    }
}]);