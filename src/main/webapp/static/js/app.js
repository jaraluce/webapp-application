var app = angular.module('relevante', [
	'config',
	'angularjs-crypto',
	'ngResource',
	'ngCookies',
	'angularValidator',
	'angular-jwt',
	'angular-oauth2',
	'tmh.dynamicLocale',
	'ngAnimate',
	'ui.bootstrap',
	'ui.bootstrap.tooltip',
	'ui.router',
	'ui.grid',
	'ui.grid.saveState',
	'ui.grid.infiniteScroll',
	'ui.grid.moveColumns',
	'ui.grid.selection',
	'ui.grid.resizeColumns',
	'ui.grid.autoResize',
	'ui.grid.grouping',
	'ui.grid.exporter',
	'ui.grid.importer',
	'LocalStorageModule',
	'infinite-scroll',
	'angular-click-outside',
	'ngDraggable',
	'list-settings',
	'custom-search',
	'recent-activity',
	'my-contacts',
	'build',
	'filter',
	'watchlist',
	'blacklist',
	'content-hub',
	'quicksearch',
	'onboardingFilter',
	'onboardingWatchlist',
	'cards'
]);

// JWT Interceptor
app.config(function Config($httpProvider, jwtInterceptorProvider) {

  	jwtInterceptorProvider.tokenGetter = function(jwtHelper, $http, $location, options) {

      	if (options.url.indexOf('/v1') === -1) {
          return null;
        }

	  	var jwtToken = localStorage.getItem('JWT');
	  	var userId = localStorage.getItem('UserID');

		var jwtDate;
		var jwtExpired;
		var jwtSubject;

	  	if (jwtToken != undefined) {
	  		jwtDate = jwtHelper.getTokenExpirationDate(jwtToken);
	    	jwtExpired = jwtHelper.isTokenExpired(jwtToken);
			jwtSubject = jwtHelper.decodeToken(jwtToken).sub;

			if (userId != undefined && jwtSubject != userId) {
				console.log('Unexpected subject, redirecting to /login!')
				$location.path("/login");
			}
		}

		if (jwtToken == undefined || jwtExpired == true) {
			console.log('Token does not exist or is expired!')
			$location.path("/login");
		}

	    return jwtToken;
	}

  	$httpProvider.interceptors.push('jwtInterceptor');
});

app.config(function(tmhDynamicLocaleProvider) {
	tmhDynamicLocaleProvider.localeLocationPattern('./static/bower_components/angular-i18n/angular-locale_{{locale}}.js');
});

app.run(function(authManager) {
    authManager.redirectWhenUnauthenticated();
    authManager.checkAuthOnRefresh();
});

app.run(function($rootScope, $state) {
    $rootScope.$state = $state;
});

// Cancelling $http requests when triggering a new one
app.factory('HttpRequestTimeoutInterceptor', function ($q, HttpPendingRequestsService) {
    return {
      request: function (config) {
        config = config || {};
        if (config.timeout === undefined && !config.noCancelOnRouteChange) {
          config.timeout = HttpPendingRequestsService.newTimeout();
        }
        return config;
      },

       responseError: function (response) {
		  if (response.config.timeout.isGloballyCancelled) {
		    return $q.defer().promise;
		  }
			return $q.reject(response);
		}
	};
  })

.service('HttpPendingRequestsService', function ($q, $location) {
    var cancelPromises = [];
 
    function newTimeout() {
      var cancelPromise = $q.defer();
      cancelPromises.push(cancelPromise);
      return cancelPromise.promise;
    }
 
    function cancelAll() {
      angular.forEach(cancelPromises, function (cancelPromise) {
        cancelPromise.promise.isGloballyCancelled = true;
        cancelPromise.resolve();
      });
      cancelPromises.length = 0;
    }
 
    return {
      newTimeout: newTimeout,
      cancelAll: cancelAll
    };
  })

.config(function($httpProvider) {
	$httpProvider.interceptors.push('HttpRequestTimeoutInterceptor');
})
 
.run(function ($rootScope, HttpPendingRequestsService) {
    $rootScope.$on('$locationChangeSuccess', function (event, newUrl, oldUrl) {
      if (newUrl !== oldUrl
      	&& newUrl.indexOf('login') === -1
      	&& newUrl.indexOf('signup') === -1
      	&& newUrl.indexOf('linkedinCallback') === -1
      	&& newUrl.indexOf('twitterCallback') === -1
      	&& newUrl.indexOf('pocketCallback') === -1
      	&& oldUrl.indexOf('login') === -1
      	&& oldUrl.indexOf('signup') === -1
      	&& oldUrl.indexOf('linkedinCallback') === -1
      	&& oldUrl.indexOf('twitterCallback') === -1
      	&& oldUrl.indexOf('pocketCallback') === -1) {
        HttpPendingRequestsService.cancelAll();
      }
    })
});



app.config(function(OAuthProvider, $stateProvider, $urlRouterProvider, $locationProvider) {

	OAuthProvider.configure({
      baseUrl: 'http://testhub.relevante.me',
      clientId: 'wfJmM0Ew6Bmu',
      clientSecret: 'alM6vXsHwxu9ZjUU2ti1gSgXNWP7kJjn0psA1eHl9RLLSrLh' // optional
    });

	$urlRouterProvider.otherwise('/');

	$stateProvider

	.state('enableUser', {
    	url: '/admin/enableUser',
    	templateUrl: '/static/templates/pages/enable_user.html'
    })

    .state('disableUser', {
    	url: '/admin/disableUser',
    	templateUrl: '/static/templates/pages/disable_user.html'
    })

    .state('login', {
    	url: '/login',
    	templateUrl: '/static/templates/pages/login.html',
    	controller: function($scope) {
    		$scope.checkLoggedUser();
    	}
    })

    .state('signUp', {
    	url: '/signup',
    	templateUrl: '/static/templates/pages/signup.html'
    })

    .state('linkedinCallback', {
    	url: '/linkedinCallback',
    	controller: function($scope, $http, $location) {
			var oAuthRequestSecret = localStorage.getItem("linkedinRequestSecret");
			var oAuthRequestToken = localStorage.getItem("linkedinRequestToken");
			var oAuthRequestOriginalToken = $location.search().oauth_token;
			var oAuthRequestVerifier = $location.search().oauth_verifier;
			var redirectUri = localStorage.getItem("linkedinCurrentUrl");

			$http({
		        url: $scope.apiBaseUrl + '/linkedin/connect/oAuth',
		        method: 'PUT',
		        contentType: "application/json",
		        data: {
		        	'oauthRequestSecret': oAuthRequestSecret,
		        	'oauthRequestToken': oAuthRequestToken,
		        	'oauthRequestOriginalToken': oAuthRequestOriginalToken,
		        	'oauthRequestVerifier': oAuthRequestVerifier
		        }
		    }).then(function successCallback(response) {
		    	mixpanel.track("Connects LinkedIn account");
		    	$('#main-menu').css('display', 'none')
		    	$scope.getUserAccount();
		    	$location.path(redirectUri);
		    	delete $location.search().oauth_token;
		    	delete $location.search().oauth_verifier;
		    	$location.$$compose();
		    	
		    	$scope.showUserAccount();

		    	localStorage.removeItem('linkedinRequestSecret');
		    	localStorage.removeItem('linkedinRequestToken');
		    	localStorage.removeItem('linkedinCurrentUrl');

		    }, function errorCallback(response) {
		    	// Error
		    });

		}
    })

    .state('twitterCallback', {
    	url: '/twitterCallback',
    	controller: function($scope, $http, $location) {
			var oAuthRequestSecret = localStorage.getItem("twitterRequestSecret");
			var oAuthRequestToken = localStorage.getItem("twitterRequestToken");
			var oAuthRequestOriginalToken = $location.search().oauth_token;
			var oAuthRequestVerifier = $location.search().oauth_verifier;
			var redirectUri = localStorage.getItem("twitterCurrentUrl");

			$http({
		        url: $scope.apiBaseUrl + '/twitter/connect/oAuth',
		        method: 'PUT',
		        contentType: "application/json",
		        data: {
		        	'oauthRequestSecret': oAuthRequestSecret,
		        	'oauthRequestToken': oAuthRequestToken,
		        	'oauthRequestOriginalToken': oAuthRequestOriginalToken,
		        	'oauthRequestVerifier': oAuthRequestVerifier
		        }
		    }).then(function successCallback(response) {
		    	mixpanel.track("Connects Twitter account");
		    	$('#main-menu').css('display', 'none')
		    	$scope.getUserAccount();
		    	$location.path(redirectUri);
		    	delete $location.search().oauth_token;
		    	delete $location.search().oauth_verifier;
		    	$location.$$compose();

		    	$scope.showUserAccount();

		    	localStorage.removeItem('twitterRequestSecret');
		    	localStorage.removeItem('twitterRequestToken');
		    	localStorage.removeItem('twitterCurrentUrl');

		    }, function errorCallback(response) {
		    	// Error
		    });

		}
    })

    .state('pocketCallback', {
    	url: '/pocketCallback',
    	controller: function($scope, $http, $location) {
			var oAuthRequestToken = localStorage.getItem("pocketRequestToken");
			var redirectUri = localStorage.getItem("pocketCurrentUrl");

			$http({
		        url: $scope.apiBaseUrl + '/pocket/connect/oAuth',
		        method: 'PUT',
		        contentType: "application/json",
		        data: {
		        	'oauthRequestToken': oAuthRequestToken,
		        }
		    }).then(function successCallback(response) {
		    	mixpanel.track("Connects Pocket account");
		    	$('#main-menu').css('display', 'none')
		    	$scope.getUserAccount();
		    	$location.path(redirectUri);
		    	delete $location.search().oauth_token;
		    	$location.$$compose();

		    	localStorage.removeItem('pocketRequestToken');
		    	localStorage.removeItem('pocketCurrentUrl');

		    }, function errorCallback(response) {
		    	// Error
		    });

		}
    })

	.state('home', {
    	url: '/',
    	templateUrl: '/static/templates/pages/home.html'
    })

    .state('myUniverse', {
    	url: '/myUniverse',
    	templateUrl: '/static/templates/pages/recent_activity.html',
    	controller: 'recent-activity'
    })

    .state('myContacts', {
    	url: '/myContacts',
    	templateUrl: '/static/templates/pages/my_contacts.html',
    	controller: 'my-contacts'
    })

    .state('import', {
		url: '/list/upload',
		templateUrl: '/static/templates/pages/import.html'
	})

    .state('createList', {
    	url: '/list/create-list',
		templateUrl: '/static/templates/pages/create_list.html',
		controller: 'listSettings'
	})

	.state('listSetup', {
    	url: '/list/:id/list-setup',
		templateUrl: '/static/templates/pages/list_setup.html',
		controller: 'listSettings'
	})

	.state('engageMode', {
    	url: '/list/:id/engage-mode',
		templateUrl: '/static/templates/pages/engage_mode.html',
		controller: 'listSettings'
	})

	.state('addPeople', {
    	url: '/list/:id/add-people',
		templateUrl: '/static/templates/pages/list-add-people.html',
		controller: 'listSettings'
	})

    .state('engage', {
    	url: '/list/:id/engage',
		templateUrl: '/static/templates/pages/engage.html',
		controller: 'listSettings'
	})

	.state('build', {
    	url: '/list/:id/build',
		templateUrl: '/static/templates/pages/build.html',
		controller: 'listSettings'
	})

    .state('search', {
    	url: '/list/:id/search/:searchType',
    	templateUrl: '/static/templates/pages/search.html',
    	controller: 'listSettings'
    })

    .state('upload', {
    	url: '/list/:id/upload',
		templateUrl: '/static/templates/pages/import.html',
		controller: 'listSettings'
	})

    .state('list', {
    	url: '/list/:id',
		templateUrl: '/static/templates/pages/watchlist_level0.html',
		controller: 'watchlist'
	})

	.state('watchlistLevel1', {
    	url: '/watchlist/level1/:id',
		templateUrl: '/static/templates/pages/watchlist_level1.html',
		controller: 'watchlist'
	})

	.state('watchlistLevel2', {
    	url: '/watchlist/level2/:id',
		templateUrl: '/static/templates/pages/watchlist_level2.html',
		controller: 'watchlist'
	})

	.state('watchlistLevel3', {
    	url: '/watchlist/level3/:id',
		templateUrl: '/static/templates/pages/watchlist_level3.html',
		controller: 'watchlist'
	})

	.state('watchlistLevel4', {
    	url: '/watchlist/level4/:id',
		templateUrl: '/static/templates/pages/watchlist_level4.html',
		controller: 'watchlist'
	})

	.state('blacklist', {
    	url: '/blacklist',
		templateUrl: '/static/templates/pages/blacklist.html',
		controller: 'blacklist'
	})

	.state('quickSearch', {
		url: '/search/:searchTerms',
		templateUrl: '/static/templates/pages/quicksearch.html',
		controller: 'quicksearch'
	})

	.state('account', {
		url: '/account',
		templateUrl: '/static/templates/pages/account.html'
	})

	.state('contentHUB', {
		url: '/contentHUB',
		templateUrl: '/static/templates/pages/content_hub.html'
	})

	.state('appFail', {
		url: '/appfail',
		templateUrl: '/static/templates/pages/appfail.html'
	})

	.state('onboardingFilter', {
		url: '/onboardingFilter',
		templateUrl: '/static/templates/pages/filter.html',
		controller: 'onboardingFilter'
	})

	.state('onboardingWatchlistLevel0', {
		url: '/onboardingWatchlist/level0',
		templateUrl: '/static/templates/pages/watchlist_level0.html',
		controller: 'onboardingWatchlist'
	})

	.state('onboardingWatchlistLevel1', {
		url: '/onboardingWatchlist/level1',
		templateUrl: '/static/templates/pages/watchlist_level1.html',
		controller: 'onboardingWatchlist'
	})

	.state('onboardingWatchlistLevel2', {
		url: '/onboardingWatchlist/level2',
		templateUrl: '/static/templates/pages/watchlist_level2.html',
		controller: 'onboardingWatchlist'
	})

	.state('onboardingWatchlistLevel3', {
		url: '/onboardingWatchlist/level3',
		templateUrl: '/static/templates/pages/watchlist_level3.html',
		controller: 'onboardingWatchlist'
	})

	.state('onboardingWatchlistLevel4', {
		url: '/onboardingWatchlist/level4',
		templateUrl: '/static/templates/pages/watchlist_level4.html',
		controller: 'onboardingWatchlist'
	})

	$locationProvider.html5Mode(true);
});

// Header template
app.directive('header', function(){

	return {
		restrict: 'E',
		templateUrl: '/static/templates/layout-elements/header.html',
	};

});

// Account template
app.directive('account', function(){

	return {
		restrict: 'E',
		templateUrl: '/static/templates/pages/account.html'
	};

});

// List settings panel
app.directive('listSettingsPanel', function(){

	return {
		restrict: 'E',
		templateUrl: '/static/templates/layout-elements/list_settings_panel.html'
	};

});

// List settings nav
app.directive('listSettingsNav', function(){

	return {
		restrict: 'E',
		templateUrl: '/static/templates/layout-elements/list_settings_nav.html'
	};

});

// Chrome extension advice template
app.directive('chromeExtensionAdvice', function(){

	return {
		restrict: 'E',
		templateUrl: '/static/templates/layout-elements/chrome_extension_advice.html'
	};

});


// Feedback messages template
app.directive('feedbackMessages', function(){

	return {
		restrict: 'E',
		templateUrl: '/static/templates/layout-elements/feedback_messages.html'
	};

});

// Subscription buttons
app.directive('subscriptionButtons', function(){

	return {
		restrict: 'E',
		templateUrl: '/static/templates/layout-elements/subscription_buttons.html'
	};

});

// Cards template for recent activity, quicksearch and blacklist
app.directive('cards', function(){

	return {
		restrict: 'E',
		templateUrl: '/static/templates/cards/cards.html',
	};

});

// Build mode create search
app.directive('buildCreateSearch', function(){

  return {
    restrict: 'E',
    templateUrl: '/static/templates/layout-elements/build_create_search.html',
  };

})

// Build mode create search
app.directive('engageSetup', function(){

  return {
    restrict: 'E',
    templateUrl: '/static/templates/layout-elements/engage_setup.html',
  };

})

// Cards template for build
app.directive('cardsBuild', function(){

	return {
		restrict: 'E',
		templateUrl: '/static/templates/cards/cards_build.html',
	};

});

// Cards template for filter
app.directive('cardsFilter', function(){

	return {
		restrict: 'E',
		templateUrl: '/static/templates/cards/cards_filter.html',
	};

});

// Cards template for watchlist level 0
app.directive('cardsWatchlistLevel0', function(){

	return {
		restrict: 'E',
		templateUrl: '/static/templates/cards/cards_watchlist_level0.html',
	};

});

// Cards template for watchlist level 1
app.directive('cardsWatchlistLevel1', function(){

	return {
		restrict: 'E',
		templateUrl: '/static/templates/cards/cards_watchlist_level1.html',
	};

});

// Cards template for watchlist level 2
app.directive('cardsWatchlistLevel2', function(){

	return {
		restrict: 'E',
		templateUrl: '/static/templates/cards/cards_watchlist_level2.html',
	};

});

// Cards template for watchlist level 3
app.directive('cardsWatchlistLevel3', function(){

	return {
		restrict: 'E',
		templateUrl: '/static/templates/cards/cards_watchlist_level3.html',
	};

});

// Cards template for watchlist level 4
app.directive('cardsWatchlistLevel4', function(){

	return {
		restrict: 'E',
		templateUrl: '/static/templates/cards/cards_watchlist_level4.html',
	};

});

// Social networks filter contacts
app.directive('socialNetworkFiltersContacts', function(){

	return {
		restrict: 'E',
		templateUrl: '/static/templates/layout-elements/social_networks_filter_contacts.html',
	};

});

// Switch cards/list views
app.directive('switchView', function(){

	return {
		restrict: 'E',
		templateUrl: '/static/templates/layout-elements/switch_view.html',
	};

});

//Bulk actions
app.directive('bulkActions', function(){

	return {
		restrict: 'E',
		templateUrl: '/static/templates/layout-elements/bulk_actions.html',
	};

});

// Social networks filter extended
app.directive('socialNetworkFiltersExtended', function(){

	return {
		restrict: 'E',
		templateUrl: '/static/templates/layout-elements/social_networks_filter_extended.html',
	};

});

// Results filters
app.directive('resultsFilters', function(){

	return {
		restrict: 'E',
		templateUrl: '/static/templates/layout-elements/results_filters.html',
	};

});

// Watchlist select
app.directive('watchlistSelect', function(){

	return {
		restrict: 'E',
		templateUrl: '/static/templates/layout-elements/watchlist_select.html',
	};

});

// Watchlist levels nav template
app.directive('watchlistTitle', function(){

	return {
		restrict: 'E',
		templateUrl: '/static/templates/layout-elements/watchlist_title.html',
	};

});

// SVG icons
app.directive('svgIcons', function(){

	return {
		restrict: 'E',
		templateUrl: '/static/templates/layout-elements/svg_icons.html',
	};

});

// Loading circle
app.directive('loadingCircle', function(){

	return {
		restrict: 'E',
		templateUrl: '/static/templates/layout-elements/loading_circle.html',
	};

});

// Results status messages
app.directive('resultsStatusMessages', function(){

	return {
		restrict: 'E',
		templateUrl: '/static/templates/layout-elements/results_status_messages.html',
	};

});

// Watchlist levels nav template
app.directive('navWatchlistLevels', function(){

	return {
		restrict: 'E',
		templateUrl: '/static/templates/layout-elements/nav_watchlist_levels.html',
	};

});

// Waiting for ng-repeat to retrieve all data
app.directive('onFinishRetrieve', function ($timeout) {
	return {
	    restrict: 'A',
	    link: function (scope, element, attr) {
	        if (scope.$last === true) {
                scope.$evalAsync(attr.onFinishRetrieve);
            }
	    }
	}
});

//Waiting for ng-repeat to render all data
app.directive('onFinishRender', function ($timeout) {
	return {
	    restrict: 'A',
	    link: function (scope, element, attr) {
	        if (scope.$last === true) {
                $timeout(function() { 
			      scope.$eval(attr.onFinishRender);
			   });
            }
	    }
	}
});

// Press enter key
app.directive('ngEnter', function () {
    return function (scope, element, attrs) {
        element.bind("keydown keypress", function (event) {
            if(event.which === 13) {
                scope.$apply(function (){
                    scope.$eval(attrs.ngEnter);
                });
 
                event.preventDefault();
            }
        });
    };
});

// Press control/command + enter key
app.directive('ngEnterCommand', function () {
    return function (scope, element, attrs) {
        element.bind("keydown keypress", function (event) {
            if(event.ctrlKey && event.which === 13
            	|| event.metaKey && event.which === 13) {
                scope.$apply(function (){
                    scope.$eval(attrs.ngEnterCommand);
                });
 
                event.preventDefault();
            }
        });
    };
});

// Press comma key
app.directive('ngComma', function () {
    return function (scope, element, attrs) {
        element.bind("keydown keypress", function (event) {
            if(event.which === 188) {
                scope.$apply(function (){
                    scope.$eval(attrs.ngComma);
                });
 
                event.preventDefault();
            }
        });
    };
});

// Prevent scroll
app.directive('preventScroll', function () {
    return {
	        restrict: 'A',
	        link: function (ng, elem, attrs) {
	              
	            elem.on('DOMMouseScroll mousewheel', function (event) {

	                var $this = $(this),
	                    scrollTop = this.scrollTop,
	                    scrollHeight = this.scrollHeight,
	                    height = $this.outerHeight(),
	                    delta = (event.type == 'DOMMouseScroll' ? event.originalEvent.detail * -40 : event.originalEvent.wheelDelta),
	                    up = delta > 0;

	                if (scrollHeight > height) {
	                    
	                    if (!up && -delta > scrollHeight - height - scrollTop) {
	                        // Down
	                        $this.scrollTop(scrollHeight);
	                        return prevent(event);
	                    }

	                    else if (up && delta > scrollTop) {
	                        // Up
	                        $this.scrollTop(0);
	                        return prevent(event);
	                    }
	                }

	            });

	            // Prevent Default
	            function prevent(event) {
	                event.stopPropagation();
	                event.preventDefault();
	                event.returnValue = false;
	                return false;
	            };
	        }
	    };
});

// Limit input length
app.directive("limitTo", [function() {
    return {
        restrict: "A",
        link: function(scope, elem, attrs) {
            var limit = parseInt(attrs.limitTo);
            angular.element(elem).on("keypress", function(e) {
                if (this.value.length == limit) e.preventDefault();
            });
        }
    }
}]);

// Focus input on demand
app.directive('showFocus', function($timeout) {
  return function(scope, element, attrs) {
    scope.$watch(attrs.showFocus, 
      function (newValue) { 
        $timeout(function() {
            newValue && element.focus();
        });
      },true);
  };    
});


// Unresolved profile picture’s url
app.directive('errSrc', function() {
  return {
    link: function(scope, element, attrs) {
      element.bind('error', function() {
        if (attrs.src != attrs.errSrc) {
          attrs.$set('src', attrs.errSrc);
        }
      });
    }
  }
});

 app.directive('fileReaderDirective', function() {
    return {
        restrict: "A",
        scope: {
            fileReaderDirective: "=",
        },
        link: function(scope, element, readFileData) {
            $(element).on('change', function(changeEvent) {
                var files = changeEvent.target.files;
                if (files.length) {
                    var r = new FileReader();
                    r.onload = function(e) {
                        var contents = e.target.result;
                        scope.$apply(function() {
                            scope.fileReaderDirective = contents;
                        });
                    };
                    
        			scope.fileData = files[0];
                    r.readAsText(files[0]);
      
                }
            });
        }
    };
});

app.factory('readFileData', function() {
    return {
        processData: function(csv_data) {
            var record = csv_data.split(/\r\n|\n/);
            var headers = record[0].split(',');
            var lines = [];
            var json = {};

            for (var i = 0; i < record.length; i++) {
                var data = record[i].split(',');
                if (data.length == headers.length) {
                    var tarr = [];
                    for (var j = 0; j < headers.length; j++) {
                        tarr.push(data[j]);
                    }
                    lines.push(tarr);
                }
            }
            
            for (var k = 0; k < lines.length; ++k){
              json[k] = lines[k];
            }
            return json;
        }
    };
});

app.controller('SiteCtrl',['$scope', '$attrs', '$rootScope', '$cookies', '$window', '$location', '$http', '$anchorScroll', 'translationService', 'tmhDynamicLocale', 'selWatchlistService', '$state', '$timeout', 'jwtHelper', '$uibModal', 'readFileData',
function ($scope, $attrs, $rootScope, $cookies, $window, $location, $http, $anchorScroll, translationService, tmhDynamicLocale, selWatchlistService, $state, $timeout, jwtHelper, $uibModal, readFileData) {
	$cookies.lang;
	$cookies.selWatchlist;

	$scope.loggedUser = false;

	// Run translation if selected language changes
	$scope.translate = function(selLang){
		if (selLang != undefined) {
			$scope.selectedLanguage = selLang;
		}
		translationService.getTranslation($scope, $scope.selectedLanguage);
		
		// Set dynamic locale
		tmhDynamicLocale.set($scope.selectedLanguage);
	};

	// Init
	$scope.selectedLanguage = 'en';
	if (localStorage.getItem("lang")) {
		$scope.selectedLanguage = localStorage.getItem("lang");
	}
	if ($location.search().lang) {
		$scope.selectedLanguage = $location.search().lang;
	}
	$scope.translate();

	// Loading full screen
	var loadingFullScreen;

	$scope.triggerLoadingFullScreen = function() {
		loadingFullScreen = $uibModal.open({
	      animation: false,
	      templateUrl: '/static/templates/cards-elements/loading_full_screen.html',
	      size: 'lg',
	       backdrop: 'static',
	      scope: $scope,
	      windowClass: 'loading-full-screen'
	    });
	}

	$scope.closeLoadingFullScreen = function () {
		if(loadingFullScreen != null){
	    	loadingFullScreen.close();
		}
	};

	// Get current path
	$scope.isCurrentPath = function (path) {
      return $location.path() == path;
    };

	// Relevante user info
	var controller = this;

	//$scope.triggerLoadingFullScreen();

	$scope.getUserAccount = function() {
		$http({
			url: $scope.apiBaseUrl + "/account",
			method: "GET"
		}).then(function (response) {

			$scope.closeLoadingFullScreen();

			$scope.loggedUser = true;
			var data = response.data
	        controller.user = data;
	        controller.userInfo = data;

	        data.extraEnrichmentPricePerUnit = data.extraEnrichmentPricePerUnit.slice(0,-1);

	        $scope.userImg = '/static/images/default_user_image.jpg';

			$http({
	        	url: $scope.apiBaseUrl + "/payments",
	        	method: 'GET'
	    	}).then(function successCallback(response) {

		    	var totalAmount = 0;
		    	var spentAmount = 0;

		    	angular.forEach(response.data, function (value, i) {
		    		value.amount = value.amount / 100;
		    		totalAmount = totalAmount + value.amount;
		    	});

		    	$scope.paymentHistory = response.data;
		    	$scope.totalCreditAmount = totalAmount;
		    	$scope.spentCreditAmount = spentAmount;
		    	$scope.currentCreditAmount = totalAmount - spentAmount;
		    	$scope.remainingEnrichmentQuantity = Math.floor($scope.currentCreditAmount / data.extraEnrichmentPricePerUnit)
			});
	        
	        $scope.isNewUser = data.isNewUser;
	        if ($scope.isNewUser == true) {
	        	$scope.firstTimeUser = true;
	            mixpanel.alias(data.id);
	            $scope.newUserOnboarding = true;
	        } else if ($scope.isNewUser == false) {
	        	$scope.firstTimeUser = false;
	        }
	        
			$scope.userName = data.name;
			$scope.email = data.email;
			$scope.id = data.id
			localStorage.setItem("UserID", data.id);

			// TruConversion sending unique id
			// _tip.push(['_trackIdentity', $scope.email]);

			var twitterScreenName;
			var twitterProfile;
			if (data.twitter == undefined) {
				$scope.twitterAccount = false;
				twitterScreenName = 'Twitter not connected.'
				twitterProfile = 'Twitter not connected.'
			} else {
				twitterScreenName = '@' + data.twitter.screenNameTwitter;
				twitterProfile = data.twitter.profile_url;
				$scope.twitterAccount = true;
				if (data.twitter.picture != null) {
					$scope.userImg = data.twitter.picture;
				}
			}

			var linkedinName;
			var linkedinProfile;
			var linkedinHeadline;
			if (data.linkedin == undefined) {
				$scope.linkedinAccount = false;
				linkedinName = 'LinkedIn not connected.'
				linkedinProfile = 'LinkedIn not connected.'
			} else {
				linkedinName = data.linkedin.name_linkedin;
				linkedinProfile = data.linkedin.profile_url;
				linkedinHeadline = data.linkedin.headline;
				$scope.linkedinAccount = true;
				if (data.linkedin.picture != null) {
					if(data.linkedin.picture.indexOf('/gif;') == -1) {
						$scope.userImg = data.linkedin.picture;
					}
				}
			}

			if (data.pocket == undefined) {
				$scope.pocketAccount = false;
			} else {
				$scope.pocketAccount = true;
				$scope.pocketUsername = data.pocket.username;
			}

			if (data.wordpress == undefined) {
				$scope.contentHubAccount = false;
			} else {
				$scope.wordpressData = data.wordpress;
				$scope.contentHubAccount = true;
				$scope.wordpressHubId = data.wordpress.hubId;
				$scope.hubEnabled = data.hubEnabled;
			}

			$scope.hubEnabled = data.hubEnabled;
			$scope.bulkEngageEnabled = data.bulkEngageEnabled;

			$scope.trialPeriodRemainingDays = data.trialPeriodRemainingDays;

			$scope.pricingPlan = data.pricingPlan;
			$scope.pricingPlanInterval = data.pricingPlanInterval;
			$scope.subscribed = data.subscribed;
			
			// Show trial period offer modal
			if (!data.subscribed
				&& data.trialPeriodRemainingDays <= 5
				&& data.trialPeriodRemainingDays >= 0) {
				$scope.subscriptionLevel2OfferEnabled = true;
				$scope.openTrialPeriodOfferModal();
			}

			if (!data.subscribed
				&& data.trialPeriodRemainingDays < 0) {
				$location.path("/");
				$scope.trialPeriodExpiredModal();
			}


			// Set coupon currency
			if (data.coupon) {
				var coupon = data.coupon
				if (coupon.currency) {
					var currency = coupon.currency;
					if (currency == 'eur') {
						data.coupon.currency = '€';
					}
				}
			}

			// Get saved lists
			$scope.getLists();

			// Get saved searches
			$scope.getSavedSearches();

			// Get tag list
			$scope.getTagList();

			// Intercom init with secure mode
	  		var intercomUserHash = data.intercomUserHash;

			window.Intercom('boot', {
				app_id: "rqjvzwfc",
				name: data.name,
				email: data.email,
				company: linkedinHeadline,
				user_id: data.id,
				user_hash: intercomUserHash,
				"has sequences.me app account": true
	        });

	        // LogRocket user identification
	        if(data.email != 'xavicn@me.com'
	        	&& data.email != 'xavi@relevante.me') {
	        	window.LogRocket && window.LogRocket.init('irowpc/relevanteme');
	        	LogRocket.identify(data.id, {
				  name: data.name,
				  email: data.email
				});
	        }

			mixpanel.init("dc2e431c6e52e50a035e106a78e663c2", {
		        loaded: function() {
		            distinct_id = mixpanel.get_distinct_id();
		        }
		    });


		    mixpanel.identify(data.id);
	        mixpanel.people.set({
	            "$name": data.name,
	            "$email": data.email,    // only special properties need the $
	            "App language": $scope.selectedLanguage,
	            "Linkedin name" : linkedinName,
	            "LinkedIn profile": linkedinProfile,
	            "Twitter screen name": twitterScreenName,
	            "Twitter profile": twitterProfile,
	            "Pricing plan" : data.pricingPlan,
	            "$last_login": new Date(),         // properties can be dates...
	        });
		   	mixpanel.people.set_once({
			    'Registration date': new Date(),
			    'Starting pricing plan' : data.pricingPlan
			});
		});
	}

	if (localStorage.getItem('JWT') != undefined) {
		$scope.getUserAccount();
	} else {
		if ($location.path().indexOf('/signup') === -1) {
			$location.path("/login");
		}
	}

	// Check relevante.me Chrome Extension
	$scope.checkGCE = function() {
		$scope.triggerLoadingFullScreen();
		document.addEventListener('relevanteExtensionCheckEvent', e => {
		    if (e.detail) {
		    	$scope.closeLoadingFullScreen();
		    	$scope.relevanteExtensionInstalled = true;
		    	// Send attribute to Intercom
				if (localStorage.getItem("Chrome extension enabled")) {
					Intercom('update', {"Chrome extension enabled": true});
				}
		    } else {
				// Manage notChrome extension state
				$location.path('/');
		    }
		})

		$timeout(function() {
			if(!$scope.relevanteExtensionInstalled) {
				$scope.closeLoadingFullScreen();
				$scope.relevanteExtensionNotInstalled = true;
			}
		}, 3000);
	}

	$scope.checkGCE();

	// Refresh on tab focus when no extension
	window.addEventListener('focus', function() {
		if(!$scope.relevanteExtensionInstalled
			&& $location.path().indexOf('login') === -1
      		&& $location.path().indexOf('signup') === -1) {
				$window.location.reload();
		}

		if($scope.checkBookmarkFromLinkedIn) {
			$http.get($scope.apiBaseUrl + '/lists/' + $scope.linkedinList.id).success(function(string_data) {
				if (string_data.num > 0) {
					$scope.checkBookmarkFromLinkedIn = false;
					$scope.goToList(string_data.id, string_data.name)
				}
			});
		}

		if($scope.checkNewResultsFromLinkedIn) {
			var sourceNum = $scope.currentLinkedinList.num;
			$http.get($scope.apiBaseUrl + '/lists/' + $scope.currentLinkedinList.id).success(function(string_data) {
				if (sourceNum != string_data.num) {
					$scope.checkNewResultsFromLinkedIn = false;
					$scope.triggerLoadingFullScreen();
					$window.location.reload();
				}
			});
		}
	});

	// sequences.me logout
	$scope.logoutRelevante = function() {
		// Remove JWT token
		localStorage.removeItem("JWT");

		// Remove user data
		localStorage.removeItem("UserID");
		localStorage.removeItem("RelevanteData1");
		localStorage.removeItem("RelevanteData2");
		
		// Redirect to sequences.me
		$window.location.href = "https://sequences.me";
	}

	// Quaderno
	var quadernoHandler = QuadernoCheckout.configure({
		key: $rootScope.quadernoApiKey,
		locale: $scope.selectedLanguage,
		color: '#58135a',
		callback: function(data) {

		  var details = jwtHelper.decodeToken(data.details);

		  $scope.triggerLoadingFullScreen();

		  	if ($scope.transactionType == 'subscription') {
			  	$http({
		        	url: $scope.apiBaseUrl + "/account/subscription",
		        	method: 'PUT',
		        	data: {"pricingPlanName" : $scope.pricingPlanName, "stripePricingPlanId" : $scope.stripePlanId, "stripeCustomerId" : details.customer, "stripeSubscriptionId" :  details.transaction, "couponId" : $scope.stripeCoupon}
		    	}).then(function successCallback(response) {
			    	mixpanel.track("Payment method successfully added, subscription complete!")
	            	Intercom('trackEvent', 'Successfully adds payment data!');
			    	
			    	$scope.getUserAccount();
			    	$scope.showUserAccount();

			    	// Closing stripe modal
			    	var modal = $scope.stripeModal;
					if(modal == 'trial_expired') {
						$scope.hideTrialPeriodExpiredModal();
						$scope.showUserAccount();
					} else if(modal == 'subscribe_now') {
						$scope.hideSubscribeNowModal();
					} else if(modal == 'upgrade_account') {
						$scope.hideUpgradeAccountModal();
					} else if(modal == 'trial_offer') {
						$scope.hideTrialPeriodOfferModal();
						$scope.showUserAccount();
					} else if(modal == 'reenable_subscription') {
						$scope.hideReenableAccountModal();
					}
		    	});
		    } else if ($scope.transactionType == 'charge') {
	    		$http({
		        	url: $scope.apiBaseUrl + "/payments",
		        	method: 'POST',
		        	data: {"paymentId" : details.transaction}
		    	}).then(function successCallback(response) {
			    	mixpanel.track("Payment method successfully added, subscription complete!")
			    	// Closing stripe modal
	    			var charge = jwtHelper.decodeToken(details.original_charge);

					$scope.hideEnrichmentBudgetModal();
					$scope.getUserAccount();
				});
	    	}
		}
	});

	$scope.submitPayment = function (transactionType, product, modal, coupon, e, amount, validAmount) {
		$scope.pricingPlanName = product;
		var price;
		var name;
		var description;
		var chargeData;
		var chargeDataJWT;
		$scope.transactionType = transactionType;
		$scope.stripeModal = modal;
		$scope.stripeCoupon = coupon;

		// Subscription plans settings
		if (transactionType == 'subscription') {
			if (product == 'level1') {
				var metadata = {
		          level: 'level1',
		          interval: 'monthly',
		          coupon: coupon
		        };
	            Intercom('trackEvent', 'Clicks subscription button', metadata);
				$scope.stripePlanId = 'plan_DUmWiUob1ZpT5d';
				price = 1200;
				description = $scope.translation.PLAN + ': ' + $scope.translation.PRICING_LEVEL1 + ' - '+ $scope.translation.MONTHLY_SUBSCRIPTION;
			} else if (product == 'level2') {
				var metadata = {
		          level: 'level2',
		          interval: 'monthly',
		          coupon: coupon
		        };
	            Intercom('trackEvent', 'Clicks subscription button', metadata);
				$scope.stripePlanId = 'plan_DUmlP2CD0vHAIy';
				if (coupon == null) { 
					price = 3900;
				} else if (coupon == 'YOURLUCKYDAY') {
					price = 4900
				}
				description = $scope.translation.PLAN + ': ' + $scope.translation.PRICING_LEVEL2 + ' - ' + $scope.translation.MONTHLY_SUBSCRIPTION;
			} else if (product == 'level3') {
				var metadata = {
		          level: 'level3',
		          interval: 'monthly',
		          coupon: coupon
		        };
	            Intercom('trackEvent', 'Clicks subscription button', metadata);
				$scope.stripePlanId = '';
				price = 18900;
				description = $scope.translation.PLAN + ': ' + $scope.translation.PRICING_GROWTH + ' - ' + $scope.translation.MONTHLY_SUBSCRIPTION;
			} else if (product == 'level1yearly') {
				var metadata = {
		          level: 'level1',
		          interval: 'yearly',
		          coupon: coupon
		        };
	            Intercom('trackEvent', 'Clicks subscription button', metadata);
				$scope.stripePlanId = 'plan_DUmVSR6qEyjUkx';
				price = 11400;
				description = $scope.translation.PLAN + ': ' + $scope.translation.PRICING_LEVEL1 + ' - ' + $scope.translation.YEARLY_SUBSCRIPTION;
			} else if (product == 'level2yearly') {
				var metadata = {
		          level: 'level2',
		          interval: 'yearly',
		          coupon: coupon
		        };
	            Intercom('trackEvent', 'Clicks subscription button', metadata);
				$scope.stripePlanId = 'plan_DUmkZDFWnSuisj';
				price = 35988;
				description = $scope.translation.PLAN + ': ' + $scope.translation.PRICING_LEVEL2; + ' - ' + $scope.translation.YEARLY_SUBSCRIPTION;
			} else if (product == 'level3yearly') {
				var metadata = {
		          level: 'level3',
		          interval: 'yearly',
		          coupon: coupon
		        };
	            Intercom('trackEvent', 'Clicks subscription button', metadata);
				$scope.stripePlanId = '';
				price = 168000;
				description = $scope.translation.PLAN + ': ' + $scope.translation.PRICING_GROWTH + ' - ' + $scope.translation.YEARLY_SUBSCRIPTION;
			} else if (product == 'level2offer') {
				$scope.stripePlanId = 'level2_8{v2tA9RMR2zGs+RX8dk2*2';
				price = 4900;
				description = $scope.translation.PLAN + ': ' + $scope.translation.PRICING_LEVEL2 + ' - ' + $scope.translation.MONTHLY_SUBSCRIPTION;
			}

			$scope.pricingPlanName = product;
			openQuadernoHandler();
		}

		if(transactionType == 'charge' && validAmount) {

			// Enrichment charge settings
			if (product == 'enrichmentBudget'){
				price = amount * 100;
				description = 'Extra profiles';
			}

			$http({
		        url: $scope.apiBaseUrl + '/quaderno/paymentData',
		        method: "POST",
		        data:  {"currency" : "EUR", "amount" : price}
		    }).then(function successCallback(response) {
		    	chargeDataJWT = response.data;
		    	openQuadernoHandler();
		   	});
		}

		// Open Quaderno Checkout with further options:
		function openQuadernoHandler() {
			quadernoHandler.open({
		      type: transactionType,
		      currency: 'eur',
		      amount: price,
		      charge: chargeDataJWT,
		      plan: $scope.stripePlanId,
		      description: description,
		      email: $scope.email,
		      first_name: $scope.userName,
		      last_name: '',
		      country: 'ES',
		      billing_address: true,
		      quantity: 1
		    });
		}

		e.preventDefault();
	}

	// Free subscription
	$scope.submitFreeSubscription = function(modal) {
		$http({
        	url: $scope.apiBaseUrl + "/account/subscription",
        	method: 'PUT',
        	data: {"pricingPlanName" : 'free'}
    	}).then(function successCallback(response) {
	    	mixpanel.track("Payment method successfully added, subscription complete!")
        	Intercom('trackEvent', 'Successfully adds payment data!');
	    	
	    	$scope.getUserAccount();
	    	$scope.showUserAccount();

	    	// Closing stripe modal
	    	var modal = $scope.stripeModal;
			if(modal == 'trial_expired') {
				$scope.hideTrialPeriodExpiredModal();
				$scope.showUserAccount();
			} else if(modal == 'subscribe_now') {
				$scope.hideSubscribeNowModal();
			} else if(modal == 'upgrade_account') {
				$scope.hideUpgradeAccountModal();
			} else if(modal == 'trial_offer') {
				$scope.hideTrialPeriodOfferModal();
				$scope.showUserAccount();
			} else if(modal == 'reenable_subscription') {
				$scope.hideReenableAccountModal();
			}
    	});
	}

	// Close Checkout on page navigation:
	window.addEventListener('popstate', function() {
	  //handler.close(); // Stripe
	  quadernoHandler.close(); // Quaderno
	});

	// LinkedIn sign in
	$scope.linkedinSignin = function() {
        var callbackUrl = $location.protocol() + '://' + location.host + '/linkedinCallback';

        $http({
	        url: $scope.apiBaseUrl + '/linkedin/connect/oAuthRequest',
	        method: 'POST',
	        contentType: "text/plain",
	        data: callbackUrl
	    }).then(function successCallback(response) {

	    	var currentUrl = $location.url();
	    	var requestSecret = response.data.requestSecret;
	    	var requestToken = response.data.requestToken;

	    	// Setting Local Storage
	    	localStorage.setItem("linkedinCurrentUrl", currentUrl);
	    	localStorage.setItem("linkedinRequestSecret", requestSecret);
	    	localStorage.setItem("linkedinRequestToken", requestToken);

	    	// Redirecting to Twitter oAuth
	    	$window.location.href = response.data.oauthUrl;

	    }, function errorCallback(response) {
	    	// Error
	    });
	};

	// LinkedIn logout
	$scope.linkedinLogout = function() {
		$http({
	        url: $scope.apiBaseUrl + '/linkedin/disconnect',
	        method: 'PUT'
	    }).then(function successCallback(response) {
			mixpanel.track("Disconnects LinkedIn account");
	    	$scope.getUserAccount();

	    }, function errorCallback(response) {
	    	// Error
	    });
	};

	// Twitter sign in
	$scope.twitterSignin = function() {
        var callbackUrl = $location.protocol() + '://' + location.host + '/twitterCallback';

        $http({
	        url: $scope.apiBaseUrl + '/twitter/connect/oAuthRequest',
	        method: 'POST',
	        contentType: "text/plain",
	        data: callbackUrl
	    }).then(function successCallback(response) {
	    	var currentUrl = $location.url();
	    	var requestSecret = response.data.requestSecret;
	    	var requestToken = response.data.requestToken;

	    	// Setting Local Storage
	    	localStorage.setItem("twitterCurrentUrl", currentUrl);
	    	localStorage.setItem("twitterRequestSecret", requestSecret);
	    	localStorage.setItem("twitterRequestToken", requestToken);

	    	// Redirecting to Twitter oAuth
	    	$window.location.href = response.data.oauthUrl;

	    }, function errorCallback(response) {
	    	// Error
	    });
	};

	// Twitter logout
	$scope.twitterLogout = function() {
		$http({
	        url: $scope.apiBaseUrl + '/twitter/disconnect',
	        method: 'PUT'
	    }).then(function successCallback(response) {
			mixpanel.track("Disconnects Twitter account");
	    	$scope.getUserAccount();

	    }, function errorCallback(response) {
	    	// Error
	    });
	};

	// Pocket sign in
	$scope.pocketSignin = function() {
        var callbackUrl = $location.protocol() + '://' + location.host + '/pocketCallback';

        $http({
	        url: $scope.apiBaseUrl + '/pocket/connect/oAuthRequest',
	        method: 'POST',
	        contentType: "text/plain",
	        data: callbackUrl
	    }).then(function successCallback(response) {
	    	var currentUrl = $location.url();
	    	var requestToken = response.data.requestToken;

	    	// Setting Local Storage
	    	localStorage.setItem("pocketCurrentUrl", currentUrl);
	    	localStorage.setItem("pocketRequestToken", requestToken);

	    	// Redirecting to Twitter oAuth
	    	$window.location.href = response.data.oauthUrl;

	    }, function errorCallback(response) {
	    	// Error
	    });
	};

	// Pocket logout
	$scope.pocketLogout = function() {
		$http({
	        url: $scope.apiBaseUrl + '/pocket/disconnect',
	        method: 'PUT'
	    }).then(function successCallback(response) {
			mixpanel.track("Disconnects Pocket account");
	    	$scope.getUserAccount();

	    }, function errorCallback(response) {
	    	// Error
	    });
	};

	// Pricing plans
	$scope.getPricingPlans = function() {
		$scope.pricingPlans = {
		    "type": "select", 
		    "name": "PricingPlan",
		    "selected": $scope.getDefaultPricingPlan(),
		    "values": [
		    	{value : "professional",
		    		label : $scope.translation.PRICING_LEVEL1 + ' (12€/' + $scope.translation.MONTH + ')',
		    		price : 12,
		    		description : $scope.translation.PRICING_LEVEL1_DESC},
		    	{value : "salesforce",
		    		label : $scope.translation.PRICING_LEVEL2 + ' (89€/' + $scope.translation.MONTH + ')',
		    		price : 89,
		    		description : $scope.translation.PRICING_LEVEL2_DESC},
		    	{value : "growth",
		    		label : $scope.translation.PRICING_GROWTH + ' (189€/' + $scope.translation.MONTH + ')',
		    		price : 189,
		    		description : $scope.translation.PRICING_GROWTH_DESC}
		    	]
		}
	};

	$scope.getDefaultPricingPlan = function () {
		var defaultPricingPlan = $location.search().pricing_plan;
		if (defaultPricingPlan == undefined) {
			return "professional";
		} else {
			return defaultPricingPlan;
		}
	}

	// Go to relevante.me Google Chrome extension
    $scope.goToChromeExtensionUrl = function () {
      mixpanel.track('Goes to Sequences.me Chrome extension url');

      // Sequences.me - Official Chrome extension
      var url = 'https://chrome.google.com/webstore/detail/sequencesme/gclhacdgcdpejkmakgfdjpgmllndhfic';

      // SALESFLOWS - Alternative Chrome extension
      // var url = 'https://chrome.google.com/webstore/detail/pfecaibfldjlikbnpmlliikdbnkjpjic';

      $window.open(url, '_blank');
    }

    // Dismiss Google Chrome advice
    $scope.dismissGCEADvice = function() {
    	localStorage.setItem("Dismiss Google Chrome extension advice", true);
    }

    $scope.getGCEADvicePref = function() {
    	return localStorage.getItem("Dismiss Google Chrome extension advice");
    }

	// Privacy policy modal
	$scope.showPrivacyPolicy = function(size) {
    	var modalInstance = $uibModal.open({
	      animation: false,
	      templateUrl: '/static/templates/pages/privacy_policy_' + $scope.selectedLanguage + '.html',
	      size: size,
	      scope: $scope,
	      windowClass: 'privacyPolicy'
	    });

	    // Hide policy modal
	    $scope.hidePrivacyPolicy = function() {
	    	modalInstance.close();
	    }
    }

    // Trial period expired modal
	$scope.trialPeriodExpiredModal = function(size) {
		//$scope.subscriptionLevel2OfferEnabled = false;
    	var modalInstance = $uibModal.open({
	      animation: false,
	      templateUrl: '/static/templates/layout-elements/trial_period_expired_modal.html',
	      size: 'lg',
	      backdrop: 'static',
	      scope: $scope,
	      windowClass: 'trialPeriodExpired'
	    });

	    // Hide trial period expired modal
	    $scope.hideTrialPeriodExpiredModal = function() {
	   		modalInstance.close();
	   	}
	}

    // Subscribe now modal
	$scope.openSubscribeNowModal = function() {
		//$scope.subscriptionLevel2OfferEnabled = false;
    	var modalInstance = $uibModal.open({
	      animation: false,
	      templateUrl: '/static/templates/layout-elements/subscribe_now_modal.html',
	      size: 'lg',
	      scope: $scope,
	      windowClass: 'subscribeNow'
	    });

	    // Hide subscribe now modal
	    $scope.hideSubscribeNowModal = function() {
	    	modalInstance.close();
	    }
    }

    // Subscribe now modal
	$scope.openReenableAccountModal = function() {
		$scope.alreadySubscribed = true;
		//$scope.subscriptionLevel2OfferEnabled = false;
    	var modalInstance = $uibModal.open({
	      animation: false,
	      templateUrl: '/static/templates/layout-elements/reenable_account_modal.html',
	      size: 'lg',
	      scope: $scope,
	      windowClass: 'subscribeNow'
	    });

	    // Hide subscribe now modal
	    $scope.hideReenableAccountModal = function() {
	    	$scope.alreadySubscribed = false;
	    	modalInstance.close();
	    }
    }

    // Upgrade account modal
	$scope.openUpgradeAccountModal = function() {
		//$scope.subscriptionLevel2OfferEnabled = false;
    	var modalInstance = $uibModal.open({
	      animation: false,
	      templateUrl: '/static/templates/layout-elements/upgrade_account_modal.html',
	      size: 'lg',
	      scope: $scope,
	      windowClass: 'upgradeAccount'
	    });

	    // Hide subscribe now modal
	    $scope.hideUpgradeAccountModal = function() {
	    	modalInstance.close();
	    }
    }

    // Trial period offer modal
	$scope.openTrialPeriodOfferModal = function() {
		//$scope.subscriptionLevel2OfferEnabled = true;
    	var modalInstance = $uibModal.open({
	      animation: false,
	      templateUrl: '/static/templates/layout-elements/trial_period_offer_modal.html',
	      size: 'lg',
	      scope: $scope,
	      windowClass: 'subscribeNow'
	    });

	    // Hide subscribe now modal
	    $scope.hideTrialPeriodOfferModal = function() {
	    	modalInstance.close();
	    }
    }

    // Enrichment budget modal
    $scope.openEnrichmentBudgetModal = function() {
        var modalInstance = $uibModal.open({
          animation: false,
          templateUrl: '/static/templates/layout-elements/enrichment_budget_modal.html',
          size: 'lg',
          scope: $scope,
          windowClass: 'enrichmentBudget'
        });

        // Hide subscribe now modal
        $scope.hideEnrichmentBudgetModal = function() {
            modalInstance.close();
        }
    }

	// Admin enable user
	$scope.adminEnableUser = function(data) {
		if(data.emails != undefined && data.emails.length > 0) {
			$scope.successfullyUserEnabled = false;
			$scope.errorUserEnabled = false;
			$scope.notValidEmailUserEnabled = false;
			$scope.downgradeMsg = false;

			var emailsArray = [];

			angular.forEach(data.emails, function (value, i) {
	    		emailsArray.push(value);
	    	});

			var hubUrl = data.hubUrl;
			var hubUser = data.hubUser;
			var hubPassword = data.hubPassword;
			var bulkEngageEnabled = data.bulkEngageEnabled;
			var pricingPlan = data.pricingPlan;

			$http({
		        url: $scope.apiBaseUrl + '/admin/enableUser',
		        method: 'POST',
		        contentType: "application/json",
		        data: {'emails': emailsArray, 'hubUrl': hubUrl, 'username': hubUser, 'password': hubPassword, "bulkEngageEnabled": bulkEngageEnabled, "pricingPlan" : pricingPlan}
		    }).then(function successCallback(response) {
		    	// Reset values
		    	$scope.resetEnableUserData(data);

		    	$scope.successfullyUserEnabled = true;

		    }, function errorCallback(response) {
		    	if (response.status === 500 ) {
		    		$scope.errorUserEnabled = true;
		    	} else if (response.status === 404 ) {
		    		$scope.notValidEmailUserEnabled = true;
		    		$scope.notValidEmailArray = emailsArray;
		    	} else if (response.status === 409 ) {
		    		$scope.downgradeMsg = true;
		    	}
		    });
		}
	};

	$scope.resetEnableUserData = function(data) {
		$scope.data.emails = [];
		$('#admin-enable-user').find('input.text').val('');
		data.bulkEngageEnabled = false;
		$scope.bulkEngageEnabled = false;
		data.pricingPlan = 'level1';
		$scope.pricingPlan = 'level1';
	}

	// Admin disable user
	$scope.adminDisableUser = function(data) {
		if(data.emails != undefined && data.emails.length > 0) {
			$scope.successfullyUserDisabled = false;
			$scope.errorUserEnabled = false;
			$scope.notValidEmailUserEnabled = false;

			var emailsArray = [];

			angular.forEach(data.emails, function (value, i) {
	    		emailsArray.push(value);
	    	});

			$http({
		        url: $scope.apiBaseUrl + '/admin/disableUser',
		        method: 'POST',
		        contentType: "application/json",
		        data: {'emails': emailsArray}
		    }).then(function successCallback(response) {
		    	// Reset values
		    	$scope.resetEnableUserData(data);

		    	$scope.successfullyUserDisabled = true;

		    }, function errorCallback(response) {
		    	if (response.status === 500 ) {
		    		$scope.errorUserEnabled = true;
		    	} else if (response.status === 404 ) {
		    		$scope.notValidEmailUserEnabled = true;
		    		$scope.notValidEmailArray = emailsArray;
		    	}
		    });
		}
	};

	// Show main menu
	$scope.showContextList = function() {
		$('#main-menu').toggle("slide", { direction: "left" }, 200);
	};

	// Hide main menu
	$scope.hideContextList = function() {
		$('#main-menu').hide("slide", { direction: "left" }, 200);
	};

	// Show user account
	$scope.showUserAccount = function() {
		$('.user-content-wrap').toggle('fast');
		$('#user-account').toggle("slide", { direction: "right" }, 200);
	};

	// Hide user account
	$scope.hideUserAccount = function() {
		$('.user-content-wrap').hide('fast');
		$('#user-account').hide("slide", { direction: "right" }, 200);
	};

	// Show list settings panel
	$scope.showListSettingsPanel = function() {
		$('.list-settings-wrap').toggle('fast')
		$('#list-settings-panel').toggle("slide", { direction: "right" }, 200);
	};

	// Hide list settings panel
	$scope.hideListSettingsPanel = function() {
		$('.list-settings-wrap').hide('fast')
		$('#list-settings-panel').hide("slide", { direction: "right" }, 200);
	};

	// Add terms to create list home page
	$scope.createListTerms = [];

	$scope.checkCreateListTerms = function(term) {
		$scope.createListTerms = [];
		$scope.createListTerms.push(term);
	}

	// Checking if watchlist exists when editing
	$scope.checkEditNameWatchlist = function(watchlistName) {
		$scope.watchlistEditNameExists = false;

		angular.forEach($scope.savedWatchlists, function (value, i) {
			if (value.name == watchlistName) {
				$scope.watchlistEditNameExists = true;
			}
		});
	}

	// Go to search
	$scope.goToSearch = function(listId, list, searchType) {
		mixpanel.track("Opens search", {"Search": list});
		var list;
		if (!localStorage.getItem("buildList")) {
			localStorage.setItem("buildList", $scope.initialList);
		}
    	$location.path("/list/" + listId + "/search/" + searchType);
	}

	// Go to build
	$scope.goToBuild = function(listId, list) {
		mixpanel.track("Opens search in app", {"Search": list});

    	if($location.path() == "/list/" + listId + "/build") {
    		$window.location.reload();
    	} else {
    		$location.path("/list/" + listId + "/build");
    	}
	}

	// Get saved searches
	$scope.getSavedSearches = function() {
		$http.get($scope.apiBaseUrl + '/searches').success(function(data) {
			angular.forEach(data, function (value, i) {
				value.negativeTerms = value.negativeTerms.join(', ')
			});
			controller.savedWatchlists = data;
			$scope.savedWatchlists = controller.savedWatchlists.sort($scope.sort_by('name', false, function(a){return a.toLowerCase()}));

			// Get initial watchlist
			if ($scope.savedWatchlists.length > 0) {
				$scope.initialWatchlist = $scope.savedWatchlists[0].name;
			}

			if (localStorage.getItem("selWatchlist") == 'Onboarding watchlist') {
				$scope.newUserOnboarding = true;
			}
		});
	}

	// Before render
	$scope.watchlistFunctions = function() {
		var localList = localStorage.getItem("selectedList")
		if (localList) {
			angular.forEach($scope.savedLists, function (value, i) {
		        if (localList === value.name) {
					$scope.selectedList = localList;
		        }
		    });
		}
		if (!localList
			|| $scope.selectedList == undefined) {
			$scope.selectedList = $scope.initialList;
			$scope.newSelWatchlist($scope.initialList);
		}
	}

	// Run new selected watchlist if selected watchlist changes
	$scope.newSelWatchlist = function(selWatchlist){
		selWatchlistService.getSelWatchlist($scope, selWatchlist);
	};

	// Create watchlist tooltip
	$scope.toggleWatchlistTooltip = function() {
		$('#new-watchlist-tooltip').slideToggle('fast');
	}

	// Advanced search
	$scope.advancedSearchFilters = ['location', 'company', 'industry'];
	$scope.advancedSearchItem = {'keyword': '', 'filters': $scope.advancedSearchFilters}
	$scope.advancedSearch = [];

	$scope.advancedSearchArray = [];

	$scope.pushAdvancedSearch = function(itemNum, advancedSearchFilter) {
		if ($scope.advancedSearchArray[itemNum] == undefined) {
			$scope.advancedSearchArray.push({'keyword': advancedSearchFilter.keyword, 'filter': advancedSearchFilter.filter});
		} else {
			$scope.advancedSearchArray[itemNum].keyword.push(advancedSearchFilter.keyword);
		}
	}

	$scope.addAdvancedSearchItem = function () {
		if ($scope.advancedSearch.length == 0) {
			$scope.advancedSearch.push($scope.advancedSearchItem);
		} else if ($scope.advancedSearch.length > 0) {
					$scope.advancedSearch.push($scope.advancedSearchItem);
			// angular.forEach($scope.advancedSearch, function(value, i){
			// 	if(value.keyword == ''){
			// 		return
			// 	} else {
			// 	}
			// });
		}
	}

	$scope.watchlistTermsUpdate = [];

	$scope.addWatchlistTermUpdate = function(term, watchlist) {
		if($('.update-list input.create-filter').val().length > 0) {
			$('.update-list input.create-filter').val('');
			$scope.updateListData = watchlist;
			$scope.updateListData.searchTerms.push(term);
			$scope.emptyWatchlistInput = true;
		} else if ($scope.updateListData.searchTerms.length > 0 && $('.update-list input.create-filter').val().length == 0) {
			$scope.updateWatchlist(watchlist, true);
		}
	}

	$scope.deleteWatchlistTermUpdate = function(term, watchlist) {
		$scope.updateListData = watchlist;
		$scope.updateListData.searchTerms.splice(term, 1);

		if ($scope.updateListData.searchTerms.length == 0) {
			$scope.emptyWatchlistInput = false;
		}
	}

	$scope.scrollTo = function(id) {
    	//$location.hash(id);
    	$anchorScroll(id);
   }

	$scope.updateWatchlist = function (watchlistData, runFilter){

		$scope.saveWatchlistValidatedFields = function() {
			var watchlistId = watchlistData.id;
			var name = watchlistData.name;
			var terms = watchlistData.searchTerms;
			var searchCondition = watchlistData.searchCondition;
			var negativeTerms = []

			if(watchlistData.negativeTerms != undefined
				&& watchlistData.negativeTerms != "") {
				negativeTerms = watchlistData.negativeTerms.split(', ');
			}

			var goal = watchlistData.searchGoal;
			var location;
			var industry;
			var company;

			if(watchlistData.location != '') {
				location = watchlistData.location;
				localStorage.setItem("defaultLocation", location);
			}
			if(watchlistData.industry != '') {
				industry = watchlistData.industry;
			}
			if(watchlistData.company != '') {
				company = watchlistData.company;
			}

	        if ($scope.watchlistNameUpdateExists == false) {
				$http({
			        url: $scope.apiBaseUrl + "/watchlists/" + watchlistId,
                    method: "PUT",
			        data: { name : name, searchTerms : terms, searchCondition: searchCondition, negativeTerms: negativeTerms, location : location, industry : industry, company : company, searchGoal: goal}
			    })
			    .then(function successCallback() {
			    	var data = watchlistData;
			    	mixpanel.track("Updates watchlist", {"Watchlist": data.name, "Terms" : data.searchTerms, "Search condition": searchCondition, "Negative terms": negativeTerms, "Location" : data.location, "Industry" : data.industry, "Company" : data.company});
		            $('#new-watchlist-tooltip').slideUp('fast');
					angular.forEach($scope.savedWatchlists, function (value, i) {
						if (value.watchlistId == watchlistId) {
							value.name = name;
							value.searchTerms = terms;
							value.searchGoal = goal;
							value.location = location;
							value.industry = industry;
						}
					});
					$scope.updateListActive = false;

		            if (runFilter == true) {
		            	if ($location.path() == "/search/" +  data.id) {
		            		$scope.hideContextList();
		            		$state.reload();
		            	} else {
		            		$scope.hideContextList();
		            		$location.path("/search/" +  data.id);
		            	}
		        	}
			    }, function errorCallback(response) {
			    	if (response.status === 409) {
		    			$scope.feedbackListLimit = true;
			    	}
			    });
			}
		}

		if (watchlistData.name == undefined
			|| watchlistData.name != undefined
			&& !watchlistData.name.length) {
			return;
		} else {
			$scope.saveWatchlistValidatedFields();
		}
	}

	// Sort array on key values
	$scope.sort_by = function(field, reverse, primer){

	   var key = primer ? 
	       function(x) {return primer(x[field])} : 
	       function(x) {return x[field]};

	   reverse = !reverse ? 1 : -1;

	   return function (a, b) {
	       return a = key(a), b = key(b), reverse * ((a > b) - (b > a));
	     } 
	}

	// GET PENDING REQUESTS
	$scope.getPendingRequests = function() {
		$http.get($scope.apiBaseUrl + '/requests/pending').success(function(data) {
			console.log(data)
		});
	}
	// $scope.getPendingRequests();

	// Get lists
	$scope.getLists = function(goToHome, setSelectedList, list) {
		$http.get($scope.apiBaseUrl + '/lists').success(function(data) {
			controller.savedLists = data;
			$scope.savedLists = controller.savedLists.sort($scope.sort_by('name', false,
				function(a){
					if(a) {
						return a.toLowerCase()
					}
				})
			);

			Intercom('update', {"Saved lists": $scope.savedLists.length});

			var totalPeopleInLists = 0
			var totalPeopleInMonitoredLists = 0
		    for ( var i = 0, _len = $scope.savedLists.length; i < _len; i++ ) {
		        totalPeopleInLists += $scope.savedLists[i].num;

		        if ($scope.savedLists[i].monitored) {
		        	totalPeopleInMonitoredLists += $scope.savedLists[i].num;;
		        }
		    }
		    Intercom('update', {"People in lists": totalPeopleInLists});
		    Intercom('update', {"People in monitored lists": totalPeopleInMonitoredLists});

			if(goToHome) {
				$location.path("/");
			}

			if (setSelectedList) {
				$scope.setSelectedList(list);
			}

			if ($scope.savedLists.length == 0) {
				localStorage.removeItem('selectedList');
				localStorage.removeItem('selWatchlist');
			}

			// Get initial watchlist
			if ($scope.savedLists.length > 0) {
				$scope.initialList = $scope.savedLists[0].name;

				if (!localStorage.getItem("selectedList")) {
					$scope.selectedList = $scope.initialList;
					$scope.setSelectedList($scope.selectedList)
				}
			}

			$scope.closeLoadingFullScreen();
		});
	}

	// Get selected List
	$scope.setSelectedList = function(list) {
		localStorage.setItem("selectedList", list);
	}

	// Get selected List
	$scope.getSelectedList = function() {
		if($scope.savedLists.length > 0) {
			if (!localStorage.getItem("selectedList")) {
				return $scope.savedLists[0].name;
			} else {
				return localStorage.getItem("selectedList");
			}
		}
	}

	// Get initial list
	$scope.getInitialList = function() {
		return $scope.savedLists[0].name;
	}

	// Create list from card
	$scope.createListModal = function() {
		var modalInstance = $uibModal.open({
	      animation: false,
	      templateUrl: '/static/templates/layout-elements/create_list_from_card.html',
	      size: 'lg',
	      scope: $scope,
	      windowClass: 'createListFromCardModal'
	    });

	    // Hide policy modal
	    $scope.hideCreateListModal = function() {
	    	modalInstance.close();
	    }
	}

	// Edit list home
    $scope.editListHome = function(list) {
      if(!$scope.listNameExists
        && list.name.length > 0) {
        $http({
              url: $scope.apiBaseUrl + "/lists/" + list.id,
              method: "PUT",
              data: { name : list.name, monitored : list.monitored }
          })
          .then(function(response) {
              mixpanel.track("Edits list", {"List": list.name});
              $scope.listName = list.name;
              $scope.getLists();
        });
      }
    }

	// Delete list
    $scope.deleteList = function(listId, listName) {
      $scope.triggerLoadingFullScreen()
      $http({
            url: $scope.apiBaseUrl + "/lists/" + listId,
            method: "DELETE",
            data: ""
        })
        .then(function(response) {
            mixpanel.track("Deletes list", {"List": listName});
            $scope.closeLoadingFullScreen();

            angular.forEach($scope.savedWatchlists, function (value, i) {
            	if(value.id == listId) {
		            $http({
				        url: $scope.apiBaseUrl + "/searches/" + listId,
				        method: "DELETE",
				        data: ""
				    })
				    .then(function() {
				    	mixpanel.track("Deletes search", {"Search": listName});
				    });
				}
			});
            $scope.getLists(true);

      });
    }

    // Check people in lists
    $scope.checkPeopleInLists = function() {
    	$http.get($scope.apiBaseUrl + '/my-universe/profiles').success(function(data) {
    		if(data.length == 0) {
    			$scope.peopleInLists = false;
    		} else if (data.length > 0) {
    			$scope.peopleInLists = true;
    		}
    	})
    }

	// Get default location from local storage
	$scope.getDefaultLocation = function() {
		return localStorage.getItem("defaultLocation");
	}

	// Delete watchlist
	$scope.deleteWatchlist = function(searchId, searchName, searchIndex) {
		$scope.triggerLoadingFullScreen();

		$http({
	        url: $scope.apiBaseUrl + "/searches/" + searchId,
	        method: "DELETE",
	        data: ""
	    })
	    .then(function() {
        	mixpanel.track("Deletes search", {"Search": searchName});
			$scope.savedWatchlists.splice(searchIndex, 1);
			$scope.closeLoadingFullScreen();

        	if(searchName == localStorage.getItem("selWatchlist")) {
        		$scope.selWatchlist = $scope.initialWatchlist;
        		$scope.newSelWatchlist($scope.initialWatchlist);
        		$location.path("/")
			}
		});
	}

	// Confirm delete watchlist
	$scope.confirmDeleteWatchlist = function(watchlistIndex) {
		$('.context-wrap.' + watchlistIndex).addClass('delete-active');
		$('.delete-button.' + watchlistIndex).show('fast');
	}

	// Hide delete watchlist
	$scope.hideDeleteWatchlist = function(watchlistIndex) {
		$('.context-wrap.' + watchlistIndex).removeClass('delete-active');
		$('.delete-button.' + watchlistIndex).hide();
	}

	// Update saved watchlists list
	$scope.filterSavedWatchlists = function(item) {
		var response = false;
		angular.forEach($scope.savedLists, function (value, i) {
			if(value.name == item) {
				response = true;
			}
        });
        return response;
    };

    // Go to home page
	$scope.goToHomePage = function() {
		mixpanel.track("Opens home page");
    	$location.path("/");
    	$scope.getLists();
	}

    // Go to create first list
	$scope.goToCreateFirstList = function() {
		mixpanel.track("Opens create first list");
    	$location.path("/list/create-list");
	}

	// Go to add people step
	$scope.goToAddPeopleStep = function(listId) {
		mixpanel.track("Goes to add people step");
    	$location.path("/list/" + listId + "/add-people");
	}

	// Go to my universe
	$scope.goToMyUniverse = function() {
		mixpanel.track("Opens my universe");
		$('#context-title').find('.arrow.up').hide();
		$('#context-title').find('.arrow.down').show()
		$('#context-list').slideUp('fast');
		$location.path("/myUniverse");
	}

	// Go to my contacts
	$scope.goToMyContacts = function() {
		mixpanel.track("Opens my contacts");
		$location.path("/myContacts");
	}	

	// Go to list
	$scope.goToList = function(listId, listName, listSetup) {
		mixpanel.track("Opens list", {"list": listName});
		$scope.setSelectedList(listName);

		localStorage.setItem("buildList", listName);

		if (listSetup == true) {
			$location.path("/list/" +  listId + "/list-setup");
		} else {
			$scope.triggerLoadingFullScreen();
			$http.get($scope.apiBaseUrl + '/lists/' + listId).success(function(string_data) {
			var currentList = string_data;
			var currentSearch = false;
	    		 $http.get($scope.apiBaseUrl + '/searches').success(function(data) {
			          angular.forEach(data, function (value, i) {
		    			if (listId == value.id) {
		    				currentSearch = true;
		    			}
			    		$scope.closeLoadingFullScreen();
		    		});			          
		    		if (!currentSearch) {
		    			$location.path("/list/" +  listId + "/list-setup");
		    		} else if (currentSearch) {
			    		if (currentList.num == 0) {
			    			$location.path("/list/" +  listId + "/list-setup");
			    		} else if (currentList.num > 0) {
							$location.path("/list/" +  listId + "/engage");
			    		}
			    	}
			     });
    		});
		}
	}

	// Open LinkedInTab
	$scope.openLinkedinTab = function(list, search) {
		$scope.checkNewResultsFromLinkedIn = true;
		$scope.currentLinkedinList = list;
		var searchConfig = search.searchTerms.toString().replace(/,/g , " ");
        $window.open('https://linkedin.com/search/results/index/?keywords=' + searchConfig + '&origin=GLOBAL_SEARCH_HEADER');
	}

	// Set build url
	$scope.setBuildUrl = function(listId) {
		if ($location.path().indexOf("/engage") != -1) {
			$location.path("/list/" +  listId + "/build");
		}
	}

	// Go to watchlist
	$scope.goToWatchlist = function(watchlistId, watchlistName) {
		mixpanel.track("Opens watchlist", {"Watchlist": watchlistName});
		
		// Close onboarding
		introJs().exit();

		$location.path("/watchlist/" +  watchlistId);

		$scope.getSelWatchlistList();
	}

	// Go to blacklist
	$scope.goToBlacklist = function() {
		mixpanel.track("Opens blacklist");
		$('#context-title').find('.arrow.down, .arrow.up').toggle();
		$('#context-list').slideUp('fast');
    	$location.path("/blacklist");
	}

	// Custom search modal
	var customSearchModal;

	$scope.showCustomSearch = function() {
		$scope.customSearchEnabled = true;
		customSearchModal = $uibModal.open({
	      animation: false,
	      backdrop: 'static',
	      templateUrl: '/static/templates/layout-elements/custom_search_modal.html',
	      size: 'lg',
	      scope: $scope,
	      windowClass: 'custom-search'
	    });

	}
	$scope.closeCustomSearch = function (reload) {
		$scope.customSearchEnabled = false;
	    customSearchModal.close();
	};


	// Get tag list
	$scope.getTagList = function() {
		$http.get($scope.apiBaseUrl + '/tags').success(function(data) {
			controller.savedTags = data;
			$scope.savedTags = controller.savedTags;
		});
	}

	// Enable search input
	$scope.enableSearch = function() {
        $('.search-wrap input').addClass('hover');
        $('.search-tags').show();
    }

    // Disable search input
	$scope.disableSearch = function() {
        $('.search-wrap input').removeClass('hover');
        $('.search-tags').hide();
    }

    // Search
    $scope.quickSearch = function(searchTerms) {

    	var normalize = (function() {
	      var from = "ÃÀÁÄÂÈÉËÊÌÍÏÎÒÓÖÔÙÚÜÛãàáäâèéëêìíïîòóöôùúüûÑñÇç", 
	          to   = "AAAAAEEEEIIIIOOOOUUUUaaaaaeeeeiiiioooouuuunncc",
	          mapping = {};
	     
	      for(var i = 0, j = from.length; i < j; i++ )
	          mapping[ from.charAt( i ) ] = to.charAt( i );
	     
	      return function( str ) {
	          var ret = [];
	          for( var i = 0, j = str.length; i < j; i++ ) {
	              var c = str.charAt( i );
	              if( mapping.hasOwnProperty( str.charAt( i ) ) )
	                  ret.push( mapping[ c ] );
	              else
	                  ret.push( c );
	          }      
	          return ret.join( '' );
	      }
	     
	    })();

	    search_input = normalize(searchTerms);

	    if (search_input.replace(" ", "") != "") {
	        $('input.search').val(''); // Empty input
	        $location.path("/search/"+ search_input);
	    }
	}

	// Set up the default filters
    $scope.filters = {
        location: "",
        position: "",
        company: ""
    };

    $scope.clearFilter = function() {
        $scope.filters.location = "";
        $scope.filters.position = "";
        $scope.filters.company = "";
    };

    // Grid options
    $scope.gridOptions = {
        columnDefs: [
          { field:'img', name: 'photo', cellTemplate: '<div class="list-img"><img ng-if="row.entity.imageUrl.length" ng-src="{{row.entity.imageUrl}}" err-src="/static/images/default_user_image.jpg" /><img ng-if="!row.entity.imageUrl.length" src="/static/images/default_user_image.jpg" /></div>', cellClass: 'image-cell', enableColumnMenu: false, enableFiltering: false, width: 80},
          { name: 'name', minWidth: 180, filter: {placeholder: 'Search name'}},
          { field: 'headline', name: 'headline', minWidth: 120, filter: {placeholder: 'Search headline'}},
          { field: 'email.join(", ")', name: 'email', minWidth: 120, filter: {placeholder: 'Search email'}},
          { field: 'currentEngageLevel', name: 'engage stage', minWidth: 120, filter: {placeholder: 'Search engage level'}},
          { field: 'networks.join(", ")', name: 'networks', minWidth: 120, filter: {placeholder: 'Search networks'}},
          { field: 'networkProfiles.linkedin.industry', name: 'industry', minWidth: 120, filter: {placeholder: 'Search industry'}},
          { field: 'networkProfiles.linkedin.company', name: 'company', minWidth: 120, filter: {placeholder: 'Search company'}},
          { field: 'networkProfiles.linkedin.title', name: 'title', minWidth: 120, filter: {placeholder: 'Search title'}},
          { name: 'location', minWidth: 120, filter: {placeholder: 'Search location'}},
          // { field: 'networkProfiles', name: 'Networks', cellClass: 'network', width: 120},
          { field: 'groupsName.join(", ")', name: 'groups', minWidth: 120, filter: {placeholder: 'Search groups'}},
          // { field: 'keywords.join(", ")', name: 'related to', minWidth: 120, filter: {placeholder: 'Search keywords'}},
          // { field: 'watchlists.join(", ")', name: 'watchlists', minWidth: 120},
          { field: 'tags.join(", ")', name: 'tags', minWidth: 120, filter: {placeholder: 'Search tags'}},
        ],
        rowTemplate: '<div ng-repeat="(colRenderIndex, col) in colContainer.renderedColumns track by col.colDef.name" class="ui-grid-cell" ng-class="{ \'ui-grid-row-header-cell\': col.isRowHeader, \'custom\': true, level1: row.entity.tapDone == true, level2: row.entity.touchDone == true, level3: row.entity.connectRequest == \'done\', level4: row.entity.message == \'pending\' || row.entity.message == \'done\'}" ng-click="grid.appScope.expand(grid.rows.indexOf(row), row.entity.id, \'lg\')" ng-hide="row.entity.hideRow" ui-grid-cell></div>',

        //virtualizationThreshold: 12,
        enableRowSelection: true,
        enableSelectAll: true,
        enableColumnResizing: true,
        enableColumnReordering: true,
        enableFiltering: true,
        enableGridMenu: true,
        fastWatch: true,
        rowHeight: 60,
        onRegisterApi: function(gridApi){
          $scope.gridApi = gridApi;

          	// Setup events so we're notified when grid state changes.
		    $scope.gridApi.colMovable.on.columnPositionChanged($scope, saveState);
		    $scope.gridApi.colResizable.on.columnSizeChanged($scope, saveState);
		    $scope.gridApi.core.on.columnVisibilityChanged($scope, saveState);
		    $scope.gridApi.core.on.filterChanged($scope, saveState);
		    $scope.gridApi.core.on.sortChanged($scope, saveState);

			// Restore previously saved state.
    		restoreState();
        },

        exporterCsvFilename: 'relevante.csv',
        exporterMenuPdf: false,
        exporterCsvLinkElement: angular.element(document.querySelectorAll(".custom-csv-link-location")),
        importerDataAddCallback: function ( grid, newObjects ) {
	      $scope.data = $scope.data.concat( newObjects );
	    }
    };

    function saveState() {
	  var state = $scope.gridApi.saveState.save();
	  state.selection = [];
	  localStorage.setItem('gridState', JSON.stringify(state));
	}

	function restoreState() {
	  $timeout(function() {
	    var state = JSON.parse(localStorage.getItem('gridState'));
	    if (state) {
	    	$scope.gridApi.saveState.restore($scope, state)
	    };
	  });
	}

	$scope.data = [];
    $scope.gridOptionsImport = {
	    enableGridMenu: true,
	    virtualizationThreshold: 12,
        enableRowSelection: true,
        enableSelectAll: true,
        enableFiltering: true,
        fastWatch: true,
	    data: 'data',
	    importerDataAddCallback: function ( grid, newObjects ) {
	      $scope.data = $scope.data.concat( newObjects );
	    },
	    onRegisterApi: function(gridApi){
	      $scope.gridApi = gridApi;
	    }
	  };

	$scope.removeFromBulkSelected = function(userId, index) {
		var userIndex;
		var selectedItems = $scope.gridApi.selection.getSelectedRows();
		angular.forEach($scope.gridOptions.data, function (value, i) {
			if (userId == value.id) {
				userIndex = i;
			}
		});

		$scope.gridApi.selection.toggleRowSelection($scope.gridOptions.data[userIndex]);
		selectedItems.splice(index, 1);

		$scope.selectedLinkedin = false;
    	$scope.selectedTwitter = false;

		angular.forEach(selectedItems, function (value, i) {
    		if (value.networkProfiles.linkedin != null) {
    			$scope.selectedLinkedin = true;
    		} else if (value.networkProfiles.twitter != null) {
    			$scope.selectedTwitter = true;
    		}
    	});
	}

	// Set preferred bulk network
	$scope.setBulkNetworkNoCheck = function(network) {
		var selectedItems = $scope.gridApi.selection.getSelectedRows();
		angular.forEach(selectedItems, function (value, i) {
    		if (value.networkProfiles.linkedin != null
    			&& value.networkProfiles.twitter != null) {
    			value.bulkNetwork = network;

    			if (value.bulkNetwork2  && value.bulkNetwork2 == network) {
    				value.bulkNetwork2 = network;
    			} else {
    				value.bulkNetwork2 = null;
    			}
    		}
    	});

    	return network;
	}

	// Set preferred bulk network with activity
	$scope.setBulkNetworkWithActivity = function(network) {
		var selectedItems = $scope.gridApi.selection.getSelectedRows();
		angular.forEach(selectedItems, function (value, i) {
    		if (value.networkProfiles.linkedin != null
    			&& value.networkProfiles.linkedin.lastPost != null
    			&& value.networkProfiles.twitter != null
    			&& value.networkProfiles.twitter.lastPost != null) {
    			value.bulkNetwork = network;

    			if (value.bulkNetwork2 
    				&& value.bulkNetwork2 == network) {
    				value.bulkNetwork2 = network;
    			} else {
    				value.bulkNetwork2 = null;
    			}
    		}
    	});

    	return network;
	}

	// Check if bulk LinkedIn results
	$scope.checkBulkLinkedin = function() {
		var selectedItems = $scope.gridApi.selection.getSelectedRows();
		var linkedinResult = false;
		angular.forEach(selectedItems, function (value, i) {
    		if (value.bulkNetwork == 'linkedin'
    			|| value.bulkNetwork2 == 'linkedin') {
    			linkedinResult = true;
    		}
    	});

    	return linkedinResult;
	}

	// Check if bulk Twitter results with activity
	$scope.checkBulkTwitter = function() {
		var selectedItems = $scope.gridApi.selection.getSelectedRows();
		var twitterResult = false;
		angular.forEach(selectedItems, function (value, i) {
    		if (value.bulkNetwork == 'twitter'
    			|| value.bulkNetwork2 == 'twitter') {
    			twitterResult = true;
    		}
    	});

    	return twitterResult;
	}

	// Bulk action tap
	$scope.bulkTapModal = function(size) {
    	var selectedItems = $scope.gridApi.selection.getSelectedRows();
    	$scope.selectedLinkedin = false;
    	$scope.selectedTwitter = false;

    	if (selectedItems.length > 0) {

	    	angular.forEach(selectedItems, function (value, i) {
	    		if (value.networkProfiles.linkedin != null) {
	    			$scope.selectedLinkedin = true;
	    		} else if (value.networkProfiles.twitter != null) {
	    			$scope.selectedTwitter = true;
	    		}
	    	});


	    	var modalInstance = $uibModal.open({
		      animation: false,
		      templateUrl: '/static/templates/layout-elements/bulk_like.html',
		      size: size,
		      scope: $scope,
		      windowClass: 'bulkTapModal'
		    });

		    // Close bulk action touch
		    $scope.cancelBulkTap = function() {
		    	modalInstance.close();
		    }
    	}
    }

    $scope.bulkTap = function(size) {

    	var bulkData = [];
    	var selectedItems = $scope.gridApi.selection.getSelectedRows();

    	if (selectedItems.length > 0) {
    		$scope.triggerLoadingFullScreen();

			angular.forEach(selectedItems, function (value, i) {
	    		var profile;
	    		if (value.bulkNetwork == 'linkedin') {
	    			profile = value.networkProfiles.linkedin;
	    		} else if (value.bulkNetwork == 'twitter') {
	    			profile = value.networkProfiles.twitter;
	    		}
				if(profile != null) {
					if(profile.lastPost != null) {
						var postId = profile.lastPost.id;
						var network = value.bulkNetwork;
						bulkData.push({'postId': postId, 'network': network});
					}
				}

				if (value.bulkNetwork2 
					&& value.bulkNetwork2 != null
					&& value.bulkNetwork2 != value.bulkNetwork) {
						if (value.bulkNetwork2 == 'linkedin') {
		    			profile = value.networkProfiles.linkedin;
		    		} else if (value.bulkNetwork2 == 'twitter') {
		    			profile = value.networkProfiles.twitter;
		    		}
					if(profile != null) {
						if(profile.lastPost != null) {
							var postId = profile.lastPost.id;
							var network = value.bulkNetwork2;
							bulkData.push({'postId': postId, 'network': network});
						}
					}
				}
			});

			$http({
		        url: $scope.apiBaseUrl + '/requests/level1',
		        method: 'POST',
		        data: JSON.stringify(bulkData)
		    }).then(function(response) {
		    	mixpanel.track("Bulk like to " + selectedItems.length + " persons.");

		    	var responseIndex;

		    	// Feedback
		    	var successResultsNum = 0;
    			var errorResultsNum = 0;
    			angular.forEach(response.data, function (value, i) {
    				responseIndex = i;
    				if (value != 'error') {
    					successResultsNum = successResultsNum + 1;
    				} else if (value == 'error') {
    					errorResultsNum = errorResultsNum + 1;
    				}
    			});
    			$scope.feedbackBulkLike = true;
				$scope.errorResultsNum = errorResultsNum;
				$scope.successResultsNum = successResultsNum;
				setTimeout(function(){
					$scope.feedbackBulkLike = false;
				}, 4000);

		    	// Close Modal
				$scope.cancelBulkTap();
				$scope.closeLoadingFullScreen();

		    	angular.forEach(selectedItems, function (value, i) {
		    		var selectedItemId = value.id
		    		value.level1Done = true;

			    	// Updating result's card
	    			angular.forEach(bulkData, function (value,i) {
	    				var bulkDataNetwork = value.network;
	    				var bulkDataPostId = value.postId;
						angular.forEach($scope.gridOptions.data, function (value,i) {
							if (value.id == selectedItemId) {
			    				if (bulkDataNetwork == 'linkedin') {
			    					if (value.networkProfiles.linkedin != null
			    						&& value.networkProfiles.linkedin.lastPost != null) {
				    					if (value.networkProfiles.linkedin.lastPost.id == bulkDataPostId) {
				    						value.networkProfiles.linkedin.lastPost.like = response.data[responseIndex];
				    					}
				    				}
			    				} else if (bulkDataNetwork == 'twitter') {
			    					if (value.networkProfiles.twitter != null
			    						&& value.networkProfiles.twitter.lastPost != null) {
				    					if (value.networkProfiles.twitter.lastPost.id == bulkDataPostId) {
				    						value.networkProfiles.twitter.lastPost.fav = response.data[responseIndex];
				    					}
				    				}
			    				}
			    			}
						});
					});
		    	});
		    	// Clear selected rows
		    	$scope.gridApi.selection.clearSelectedRows();
		    });
		}
    };

    // Bulk action touch
    $scope.bulkTouchModal = function(size) {
    	var selectedItems = $scope.gridApi.selection.getSelectedRows();
    	$scope.selectedLinkedin = false;
    	$scope.selectedTwitter = false;

    	if (selectedItems.length > 0) {

	    	angular.forEach(selectedItems, function (value, i) {
	    		if (value.type == 'linkedin') {
	    			$scope.selectedLinkedin = true;
	    		} else if (value.type =='twitter') {
	    			$scope.selectedTwitter = true;
	    		}
	    	});


	    	var modalInstance = $uibModal.open({
		      animation: false,
		      templateUrl: '/static/templates/layout-elements/bulk_reply.html',
		      size: size,
		      scope: $scope,
		      windowClass: 'bulkTouchModal'
		    });

		    // Close bulk action touch
		    $scope.cancelBulkTouch = function() {
		    	modalInstance.close();
		    }
    	}
    }

    $scope.bulkTouch = function(message, size) {
    	var bulkData = [];
    	var selectedItems = $scope.gridApi.selection.getSelectedRows();

    	if (message != undefined && message.length > 0) {
    		$scope.triggerLoadingFullScreen();

			angular.forEach(selectedItems, function (value, i) {
	    		var profile;
	    		if (value.bulkNetwork == 'linkedin') {
	    			profile = value.networkProfiles.linkedin;
	    		} else if (value.bulkNetwork == 'twitter') {
	    			profile = value.networkProfiles.twitter;
	    		}
				if(profile != null) {
					if(profile.lastPost != null) {
						var postId = profile.lastPost.id;
						var network = value.bulkNetwork;
						bulkData.push({'postId': postId, 'network': network, 'commentText': message});
					}
				}

				if (value.bulkNetwork2 
					&& value.bulkNetwork2 != null
					&& value.bulkNetwork2 != value.bulkNetwork) {
						if (value.bulkNetwork2 == 'linkedin') {
		    			profile = value.networkProfiles.linkedin;
		    		} else if (value.bulkNetwork2 == 'twitter') {
		    			profile = value.networkProfiles.twitter;
		    		}
					if(profile != null) {
						if(profile.lastPost != null) {
							var postId = profile.lastPost.id;
							var network = value.bulkNetwork2;
							bulkData.push({'postId': postId, 'network': network, 'commentText': message});
						}
					}
				}
			});

			$http({
		        url: $scope.apiBaseUrl + '/requests/level2',
		        method: 'POST',
		        data: JSON.stringify(bulkData)
		    }).then(function(response) {
		    	mixpanel.track("Bulk reply to " + selectedItems.length + " persons.");

		    	var responseIndex;

		    	// Feedback
		    	var successResultsNum = 0;
    			var errorResultsNum = 0;
    			angular.forEach(response.data, function (value, i) {
    				responseIndex = i;
    				if (value != 'error') {
    					successResultsNum = successResultsNum + 1;
    				} else if (value == 'error') {
    					errorResultsNum = errorResultsNum + 1;
    				}
    			});
    			$scope.feedbackBulkReply = true;
				$scope.errorResultsNum = errorResultsNum;
				$scope.successResultsNum = successResultsNum;
				setTimeout(function(){
					$scope.feedbackBulkReply = false;
				}, 4000);

		    	// Close Modal
				$scope.cancelBulkTouch();
				$scope.closeLoadingFullScreen();
		    	
		    	angular.forEach(selectedItems, function (value, i) {
		    		var selectedItemId = value.id
		    		value.level2Done = true;

		    		// Updating result's card
	    			angular.forEach(bulkData, function (value,i) {
	    				var bulkDataNetwork = value.network;
	    				var bulkDataPostId = value.postId;
						angular.forEach($scope.gridOptions.data, function (value,i) {
							if (value.id == selectedItemId) {
			    				if (bulkDataNetwork == 'linkedin') {
			    					if (value.networkProfiles.linkedin != null
			    						&& value.networkProfiles.linkedin.lastPost != null) {
				    					if (value.networkProfiles.linkedin.lastPost.id == bulkDataPostId) {
				    						value.networkProfiles.linkedin.lastPost.comment = response.data[responseIndex];
				    					}
				    				}
			    				} else if (bulkDataNetwork == 'twitter') {
			    					if (value.networkProfiles.twitter != null
			    						&& value.networkProfiles.twitter.lastPost != null) {
				    					if (value.networkProfiles.twitter.lastPost.id == bulkDataPostId) {
				    						value.networkProfiles.twitter.lastPost.reply = response.data[responseIndex];
				    					}
				    				}
			    				}
			    			}
						});
					});
		    	});
		    	// Clear selected rows
		    	$scope.gridApi.selection.clearSelectedRows();
		    	
		    });
		}
    }

    // Bulk action connect
	$scope.bulkConnectModal = function(size) {
    	var selectedItems = $scope.gridApi.selection.getSelectedRows();
    	$scope.selectedLinkedin = false;
    	$scope.selectedTwitter = false;

    	if (selectedItems.length > 0) {

	    	angular.forEach(selectedItems, function (value, i) {
	    		if (value.type == 'linkedin') {
	    			$scope.selectedLinkedin = true;
	    		} else if (value.type =='twitter') {
	    			$scope.selectedTwitter = true;
	    		}
	    	});


	    	var modalInstance = $uibModal.open({
		      animation: false,
		      templateUrl: '/static/templates/layout-elements/bulk_connect.html',
		      size: size,
		      scope: $scope,
		      windowClass: 'bulkConnectModal'
		    });

		    // Close bulk action touch
		    $scope.cancelBulkConnect = function() {
		    	modalInstance.close();
		    }
    	}
    }

    $scope.bulkConnect = function(message) {
    	var bulkData = [];
    	var selectedItems = $scope.gridApi.selection.getSelectedRows();

    	if (selectedItems.length > 0) {
    		$scope.triggerLoadingFullScreen();

			angular.forEach(selectedItems, function (value, i) {
				var profile;
				var connectMessage;
	    		if (value.bulkNetwork == 'linkedin') {
	    			profile = value.networkProfiles.linkedin;
	    			connectMessage = message;
	    		} else if (value.bulkNetwork == 'twitter') {
	    			profile = value.networkProfiles.twitter;
	    			connectMessage = null;
	    		}
				var userId = profile.id;
				var network = value.bulkNetwork;
				bulkData.push({'profileId': userId, 'network': network, 'message': connectMessage});

				if (value.bulkNetwork2 
					&& value.bulkNetwork2 != null
					&& value.bulkNetwork2 != value.bulkNetwork) {
						if (value.bulkNetwork2 == 'linkedin') {
		    			profile = value.networkProfiles.linkedin;
		    			connectMessage = message;
		    		} else if (value.bulkNetwork2 == 'twitter') {
		    			profile = value.networkProfiles.twitter;
		    			connectMessage = null;
		    		}

		    		var userId = profile.id;
					var network = value.bulkNetwork2;
					bulkData.push({'profileId': userId, 'network': network, 'message': connectMessage});
				}
			});

			$http({
		        url: $scope.apiBaseUrl + '/requests/level3',
		        method: 'POST',
		        data: JSON.stringify(bulkData)
		    }).then(function(response) {
		    	mixpanel.track("Bulk connect to " + selectedItems.length + " persons.");

		    	var responseIndex;

		    	// Feedback
		    	var successResultsNum = 0;
    			var errorResultsNum = 0;
    			angular.forEach(response.data, function (value, i) {
    				responseIndex = i;
    				if (value != 'error') {
    					successResultsNum = successResultsNum + 1;
    				} else if (value == 'error') {
    					errorResultsNum = errorResultsNum + 1;
    				}
    			});
    			$scope.feedbackBulkConnect = true;
				$scope.errorResultsNum = errorResultsNum;
				$scope.successResultsNum = successResultsNum;
				setTimeout(function(){
					$scope.feedbackBulkConnect = false;
				}, 4000);

		    	// Close Modal
				$scope.cancelBulkConnect();
				$scope.closeLoadingFullScreen();

		    	angular.forEach(selectedItems, function (value, i) {
		    		var selectedItemId = value.id;
		    		value.level3Done = true;

		    		// Updating result's card
		    		angular.forEach(bulkData, function (value,i) {
						var bulkDataNetwork = value.network;
						var bulkDataProfileId = value.profileId;
						angular.forEach($scope.gridOptions.data, function (value,i) {
							if (value.id == selectedItemId) {
			    				if (bulkDataNetwork == 'linkedin') {
			    					if (value.networkProfiles.linkedin != null
			    						&& value.networkProfiles.linkedin.id == bulkDataProfileId) {
				    					value.networkProfiles.linkedin.connectRequest = response.data[responseIndex];
				    				}
			    				} else if (bulkDataNetwork == 'twitter') {
			    					if (value.networkProfiles.twitter != null
			    						&& value.networkProfiles.twitter.id == bulkDataProfileId) {
				    					value.networkProfiles.twitter.follow = response.data[responseIndex];
				    				}
			    				}
			    			}
						});
					});
		    	});
		    	// Clear selected rows
		    	$scope.gridApi.selection.clearSelectedRows();
		    });
		}
    };

    // Bulk action contact
    $scope.bulkContactModal = function(size) {
    	var selectedItems = $scope.gridApi.selection.getSelectedRows();
    	$scope.selectedLinkedin = false;
    	$scope.selectedTwitter = false;

    	if (selectedItems.length > 0) {

	    	angular.forEach(selectedItems, function (value, i) {
	    		if (value.type == 'linkedin') {
	    			$scope.selectedLinkedin = true;
	    		} else if (value.type =='twitter') {
	    			$scope.selectedTwitter = true;
	    		}
	    	});


	    	var modalInstance = $uibModal.open({
		      animation: false,
		      templateUrl: '/static/templates/layout-elements/bulk_message.html',
		      size: size,
		      scope: $scope,
		      windowClass: 'bulkContactModal'
		    });

		    // Close bulk action touch
		    $scope.cancelBulkContact = function() {
		    	modalInstance.close();
		    }
    	}
    }

    $scope.bulkContact = function(subject, message, size) {
    	var bulkData = [];
    	var selectedItems = $scope.gridApi.selection.getSelectedRows();

    	if (message != undefined && message.length > 0) {
    		$scope.triggerLoadingFullScreen();

			angular.forEach(selectedItems, function (value, i) {
				var profile;
	    		if (value.bulkNetwork == 'linkedin') {
	    			profile = value.networkProfiles.linkedin;
	    		} else if (value.bulkNetwork == 'twitter') {
	    			profile = value.networkProfiles.twitter;
	    		}
				var userId = profile.id;
				var network = value.bulkNetwork;
				bulkData.push({'profileId': userId, 'network': network, 'subject': subject, 'message': message});

				if (value.bulkNetwork2 
					&& value.bulkNetwork2 != null
					&& value.bulkNetwork2 != value.bulkNetwork) {
						if (value.bulkNetwork2 == 'linkedin') {
		    			profile = value.networkProfiles.linkedin;
		    		} else if (value.bulkNetwork2 == 'twitter') {
		    			profile = value.networkProfiles.twitter;
		    		}
		    		var userId = profile.id;
					var network = value.bulkNetwork2;
					bulkData.push({'profileId': userId, 'network': network, 'subject': subject, 'message': message});
				}
			});

			$http({
		        url: $scope.apiBaseUrl + '/requests/level4',
		        method: 'POST',
		        data: JSON.stringify(bulkData)
		    }).then(function(response) {
		    	mixpanel.track("Bulk message to " + selectedItems.length + " persons.");

		    	var responseIndex;

		    	// Feedback
		    	var successResultsNum = 0;
    			var errorResultsNum = 0;

    			angular.forEach(response.data, function (value, i) {
    				responseIndex = i;
    				if (value != 'error') {
    					successResultsNum = successResultsNum + 1;
    				} else if (value == 'error') {
    					errorResultsNum = errorResultsNum + 1;
    				}
    			});
    			$scope.feedbackBulkMessage = true;
				$scope.errorResultsNum = errorResultsNum;
				$scope.successResultsNum = successResultsNum;
				setTimeout(function(){
					$scope.feedbackBulkMessage = false;
				}, 4000);

		    	// Close Modal
				$scope.cancelBulkContact();
				$scope.closeLoadingFullScreen();
		    	
		    	angular.forEach(selectedItems, function (value, i) {
		    		var selectedItemId = value.id;
		    		value.level4Done = true;

		    		// Updating result's card
		    		angular.forEach(bulkData, function (value,i) {
						var bulkDataNetwork = value.network;
						var bulkDataProfileId = value.profileId;
						angular.forEach($scope.gridOptions.data, function (value,i) {
							if (value.id == selectedItemId) {
			    				if (bulkDataNetwork == 'linkedin') {
			    					if (value.networkProfiles.linkedin != null
			    						&& value.networkProfiles.linkedin.id == bulkDataProfileId) {
				    					value.networkProfiles.linkedin.message = response.data[responseIndex];
				    				}
			    				} else if (bulkDataNetwork == 'twitter') {
			    					if (value.networkProfiles.twitter != null
			    						&& value.networkProfiles.twitter.id == bulkDataProfileId) {
				    					value.networkProfiles.twitter.directMessage = response.data[responseIndex];
				    				}
			    				}
			    			}
						});
					});
		    	});
		    	// Clear selected rows
		    	$scope.gridApi.selection.clearSelectedRows();
		    	
		    });
		}
    }

    // Bulk action assign tag
    $scope.bulkTagModal = function(size) {
    	var selectedItems = $scope.gridApi.selection.getSelectedRows();
    	$scope.selectedLinkedin = false;
    	$scope.selectedTwitter = false;

    	if (selectedItems.length > 0) {

	    	var modalInstance = $uibModal.open({
		      animation: false,
		      templateUrl: '/static/templates/layout-elements/bulk_tag.html',
		      size: size,
		      scope: $scope,
		      windowClass: 'bulkTagModal'
		    });

		    // Close bulk action touch
		    $scope.cancelBulkTag = function() {
		    	modalInstance.close();
		    }
    	}
    }

    $scope.bulkTag = function(tag, size) {
    	var bulkData = [];
    	var selectedItems = $scope.gridApi.selection.getSelectedRows();
    	var successResultsNum = 0;
    	var alreadyResultsNum = 0;

    	if (tag != undefined && tag != '') {
    		$scope.triggerLoadingFullScreen();

			angular.forEach(selectedItems, function (value, i) {
				var userId = value.id;
				bulkData.push(userId);
			});

			var bulkTagData = {'tag': tag, 'profileIds' : bulkData};

			$http({
		        url: $scope.apiBaseUrl + "/tags/profiles/",
		        method: 'PUT',
		        data: JSON.stringify(bulkTagData)
		    }).then(function(response) {
		    	mixpanel.track("Bulk assigned tag " + tag + " to " + selectedItems.length + " persons.");

		    	// Close Modal
				$scope.cancelBulkTag();
				$scope.closeLoadingFullScreen();

				angular.forEach(selectedItems, function (value, i) {
					var tagRepeated = false;
					
					angular.forEach(value.tags, function (value, i) {
						if (value == tag) {
							alreadyResultsNum = alreadyResultsNum + 1;
							tagRepeated = true;
						}
		    		});

		    		if (tagRepeated == false) {
		    			value.tags.push(tag);
						successResultsNum = successResultsNum + 1;
		    		}
		    	});
		    	
				$scope.feedbackBulkTag = true;
				$scope.alreadyResultsNum = alreadyResultsNum;
				$scope.successResultsNum = successResultsNum;
				setTimeout(function(){
					$scope.feedbackBulkTag = false;
				}, 4000);
		    });
		}
    }

    // Bulk action assign watchlist
    $scope.bulkWatchlistModal = function(size) {
    	var selectedItems = $scope.gridApi.selection.getSelectedRows();
    	$scope.selectedLinkedin = false;
    	$scope.selectedTwitter = false;

    	if (selectedItems.length > 0) {

	    	var modalInstance = $uibModal.open({
		      animation: false,
		      templateUrl: '/static/templates/layout-elements/bulk_watchlist.html',
		      size: size,
		      scope: $scope,
		      windowClass: 'bulkWatchlistModal'
		    });

		    // Close bulk action touch
		    $scope.cancelBulkWatchlist = function() {
		    	modalInstance.close();
		    }
    	}
    }

    $scope.bulkList = function(list, size) {
    	var bulkData = [];
    	var selectedItems = $scope.gridApi.selection.getSelectedRows();
    	var successResultsNum = 0;
    	var alreadyResultsNum = 0;

    	if (list != undefined) {
    		var allResultsAssigned = true;
			angular.forEach(selectedItems, function (value, i) {
				var userId = value.id;
				var pushList = true;

				angular.forEach(value.watchlists, function(value, i) {
					if (value == list) {
						pushList = false;
						alreadyResultsNum = alreadyResultsNum + 1;
						return;
					}
				});
				if (pushList == true) {
					bulkData.push(userId);
					successResultsNum = successResultsNum + 1
					allResultsAssigned = false;
				}
			});

			if (!allResultsAssigned) {
				$scope.triggerLoadingFullScreen();

				angular.forEach($scope.savedLists, function (value, i) {
					var listId;

					if (value.name === list) {
						listName = value.name;
						listId = value.id;

						$http({
					        url: $scope.apiBaseUrl + "/lists/" + listId + "/profiles/",
					        method: 'PUT',
					        data: JSON.stringify(bulkData)
					    }).then(function successCallback(response) {
					    	mixpanel.track("Bulk assigned list " + list + " to " + selectedItems.length + " persons.");

					    	// Close Modal
							$scope.cancelBulkWatchlist();
							$scope.closeLoadingFullScreen();

							angular.forEach(selectedItems, function (value, i) {
								var watchlistRepeated = false;
								var userId = value.id
						
								angular.forEach(value.watchlists, function (value, i) {
									if (value == list) {
										watchlistRepeated = true;
									}
					    		});

					    		if (watchlistRepeated == false) {
					    			value.watchlists.push(list);
					    		}
					    	});

					    	$scope.getLists();
					    	
					    }, function errorCallback(response) {
					    	if (response.status === 409) {
				    			$scope.feedbackPeopleListLimit = true;
					    	}
				    	});
					}
				});
			} else if (allResultsAssigned == true) {
				// Close Modal
				$scope.cancelBulkWatchlist();
			}

			$scope.feedbackBulkWatchlist = true;
			$scope.alreadyResultsNum = alreadyResultsNum;
			$scope.successResultsNum = successResultsNum;
			setTimeout(function(){
				$scope.feedbackBulkWatchlist = false;
			}, 3000);
		}
    }

    // Form validator
	$scope.myCustomValidator = function(text){		
		return true;
	};


	$scope.anotherCustomValidator = function(text){
		if(text === "rainbow"){
			return true;
		}
		else return "type in 'rainbow'";
	};

	$scope.passwordValidator = function(password) {

		if(!password){return;}
		
		if (password.length < 6) {
			return $scope.translation.PASSWORD_VALIDATOR_CHAR_NUM;
		}

		// if (!password.match(/[A-Z]/)) {
		// 	 return "Password must have at least one capital letter";
		// }

		// if (!password.match(/[0-9]/)) {
		// 	 return "Password must have at least one number";
		// }

		return true;
	};

	// Register user
	$scope.registerUser = function(form){
		var name;

		$scope.loginForm;

		if (!form.name) {
			name = form.email.substr(0, form.email.indexOf('@'));
		} else {
			name = form.name
		}

		var pricingPlan = $location.search().pricing_plan;

		if (pricingPlan == undefined) {
			pricingPlan = null;
		} else {
			if (pricingPlan == "professional") {
				pricingPlan = "level1";
			} else if (pricingPlan == "salesforce") {
				pricingPlan = "level2";
			} else if (pricingPlan == "growth") {
				pricingPlan = "level3";
			}
		}

    	$http({
	        url: $scope.apiBaseUrl + "/account",
	        method: 'POST',
	        data: {"name" : name, "email" : form.email, "password" : form.password, "intendedPricingPlan" : pricingPlan},
	        skipAuthorization: true
	    }).then(function successCallback(response) {
	    	if (response.status === 201 ) {
    			// Remove JWT token and user data
				localStorage.removeItem("JWT");
				localStorage.removeItem("UserID");
				localStorage.removeItem("RelevanteData1");
				localStorage.removeItem("RelevanteData2");

				// Set JWT token
		    	localStorage.setItem("JWT", response.data);
		    	$location.path("/");
		    	$scope.getUserAccount();
	    	}

	    }, function errorCallback(response) {
	    	if (response.status === 304 ) {
	    		$scope.inputChange(false);
    			$scope.alreadyRegisteredUser = true;
	    	} else if (response.status === 402 ) {

	    		var data = response.data;

	    		if (data == 'level1') {
	    			$scope.pricingPlan = 'level1';
	    			$scope.pricingPlanInterval = 'month';
	    		} else if (data == 'level1yearly') {
	    			$scope.pricingPlan = 'level1';
	    			$scope.pricingPlanInterval = 'month';
	    		} else if (data == 'level2') {
	    			$scope.pricingPlan = 'level2';
	    			$scope.pricingPlanInterval = 'month';
	    		} else if (data == 'level2yearly') {
	    			$scope.pricingPlan = 'level2';
	    			$scope.pricingPlanInterval = 'month';
	    		} else if (data == 'level3') {
	    			$scope.pricingPlan = 'level3';
	    			$scope.pricingPlanInterval = 'month';
	    		} else if (data == 'level3yearly') {
	    			$scope.pricingPlan = 'level3';
	    			$scope.pricingPlanInterval = 'month';
	    		}

	    		$scope.subscribed = true;
	    		$scope.alreadySubscribed = true;

	    		$scope.loginForm = form;
	    		$scope.email = form.email;
	    		$scope.trialPeriodExpiredModal();
	    	}
	    });
	};

	// Register user bg
	$scope.checkSignupBg = function (form) {
		var formCompleteBg;
		if( form.acceptPolicy && form.email.length && form.password.length) {
			$('body.signUp').addClass('formComplete');
		} else {
			$('body.signUp').removeClass('formComplete');
		}
	}

	// Login user
	$scope.loginUser = function(form){
		$scope.triggerLoadingFullScreen();
    	$scope.loginForm = form;

    	var authUrl;

    	if (form.rememberMe) {
    		authUrl = $scope.apiBaseUrl + "/auth?rememberSession=true";
    	} else {
    		authUrl = $scope.apiBaseUrl + "/auth"
    	}

		$http({
        url: authUrl,
        method: 'POST',
        data: {"email" : form.email, "password" : form.password},
    	skipAuthorization: true
    	}).then(function successCallback(response) {
    		if (response.status === 200 ) {
    			mixpanel.track("User login");

    			$scope.alreadySubscribed = false;

    			// Remove JWT token and user data
				localStorage.removeItem("JWT");
				localStorage.removeItem("UserID");

				// Set JWT token
		    	localStorage.setItem("JWT", response.data.jwtToken);

		    	// Remember user sesion
				if (form.rememberMe) {
		    		// Do stuff here
		    	}

		    	$location.path("/");
		    	$scope.getUserAccount();

		    	// Close loading screen if present
		    	if (loadingFullScreen != null) {
		    		$scope.closeLoadingFullScreen();
		    	}	
	    	}
    	}, function errorCallback(response) {
    		if (response.status === 401 ) {
    			$scope.closeLoadingFullScreen();
    			$scope.inputChange(false);
    			$scope.incorrectLogin = true;
	    	} else if (response.status === 402 ) {
	    		$scope.closeLoadingFullScreen();
    			var data = response.data
	    		$scope.email = form.email;

	    		// Remove JWT token and user data
				localStorage.removeItem("JWT");
				localStorage.removeItem("UserID");

				// Set JWT token
		    	localStorage.setItem("JWT", data.jwtToken);
		    	$location.path("/");

	    		// Enable Salesforce offer after 15 days from trial expiration
				if (data.trialPeriodRemainingDays <= 0
					&& data.trialPeriodRemainingDays >= -15) {
					$scope.trialPeriodRemainingDays = data.trialPeriodRemainingDays;
					$scope.subscriptionLevel2OfferEnabled = true;
				}

				$scope.trialPeriodExpiredModal();
	    	}
    	});
	};

	// Hiding tips when input change
	$scope.inputChange = function(data) {
		$scope.hideTip = data;
	}

	// Update user data
	$scope.updateUserData = function(form){
		$http({
        url: $scope.apiBaseUrl + "/account/details",
        method: 'PUT',
        data: {"name" : form.name, "email" : form.email, "password" : form.password}
    	}).then(function successCallback(response) {
    		if (response.status === 200 ) {
    			mixpanel.track("User updates his data");

		  		$scope.editNameEnabled = false;
		  		$scope.editEmailEnabled = false;
		  		$scope.editPasswordEnabled = false;

		  		$('.user-details').find('input').val('');
		    	$scope.getUserAccount();
	    	}
    	}, function errorCallback(response) {
    		if (response.status === 409 ) {
    			$scope.alreadyRegisteredEmail = true;
	    	}
    	});
	};

	// Delete relevante.me account
	$scope.cancelRelevanteSubscription = function () {
    	$scope.triggerLoadingFullScreen();
		$http({
	        url: $scope.apiBaseUrl + "/account",
	        method: 'DELETE'
	    }).then(function successCallback(response) {
    		mixpanel.track("User cancels subscription. :(");

    		$scope.getUserAccount();
    	});
	}

	// Set the default value of inputType
	$scope.inputType = 'password';
  
	// Hide & show password function
	$scope.hideShowPassword = function(){
		if ($scope.inputType == 'password') {
			$scope.inputType = 'text';
		}
		else {
			$scope.inputType = 'password';
		}
	};

	// Password recovery modal
	$scope.passwordRecoveryModal = function(size) {
    	var modalInstance = $uibModal.open({
	      animation: false,
	      templateUrl: '/static/templates/layout-elements/password_recovery.html',
	      size: size,
	      scope: $scope,
	      windowClass: 'passwordRecovery'
	    });
    }
	    
    // Close passwordRecovery
    $scope.closePasswordRecoveryModal = function() {
    	modalInstance.close();
    }

    // Password recovery
    $scope.passwordRecovery = function(email) {

    	subject = $scope.translation.PASSWORD_RECOVERY_SUBJECT;
    	message = $scope.translation.PASSWORD_RECOVERY_MESSAGE;

    	$http({
	        url: $scope.apiBaseUrl + "/forgot-password",
	        method: 'POST',
	        data: {"email" : email, "subject" : subject, "message" : message},
	        skipAuthorization: true
    	}).then(function successCallback(response) {
    		if (response.status === 200 ) {
    			mixpanel.track("Password recovery", {"User's e-mail": email});
    			$scope.passwordRecoverySuccess = true;
	    	}
    	}, function errorCallback(response) {
    		if (response.status === 404 ||
    		 	response.status === 406) {
    			mixpanel.track("Password recovery attempt from unknown e-mail", {"User's e-mail": email});
    			$scope.emailNotFound = true;
	    	}
    	});
    }

    $scope.resetEmailNotFound = function() {
    	$scope.emailNotFound = false;
    }

	// Redirecto to homepage if loggedUser
	$scope.checkLoggedUser = function() {
		var jwtToken = localStorage.getItem('JWT');
		var jwtExpired;

		if (jwtToken != undefined) {
			jwtExpired = jwtHelper.isTokenExpired(jwtToken);
			if (!jwtExpired) {
				$location.path("/");
			}
		}
    }
}]);