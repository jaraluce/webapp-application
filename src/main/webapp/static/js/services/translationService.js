app.service('translationService', function($resource, localStorageService) {

    this.getTranslation = function($scope, language) {

    	$scope.$watch('lang',function(value) {
			localStorage.setItem("lang", language);
			localStorage.getItem("lang");
		});

        var languageFilePath = './static/lang/translation_' + language + '.json';
        $resource(languageFilePath).get(function (data) {
            $scope.translation = data;
        });
    };
});